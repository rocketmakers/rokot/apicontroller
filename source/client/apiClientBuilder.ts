import * as ts from "typescript";
import * as _ from "underscore";
import { ApiReflector } from "./apiReflector";
import * as Logger from "bunyan";
import { IApi, IMiddlewareKey } from "../core";

export interface IRouteType {
  typeString: string
  validationSpec: any
}

export interface IApiClientRoute {
  uniqueName: string
  typeArgs: string[] | undefined
  name: string
  route: string
  verbs: string[]
  contentType: string
  middleware: IMiddlewareKey[] | undefined
  bodyType: IRouteType
  queryType: IRouteType
  paramsType: IRouteType
  responseType: string
}

export interface IApiClientController {
  typeName: string
  routes: IApiClientRoute[]
}

export interface IApiClient {
  refs: string[]
  controllers: IApiClientController[]
}

function makeRef(source: string) {
  const src = source.trim()
  let idx = src.indexOf("export ")
  if (idx > -1) {
    return src
  }

  idx = src.indexOf("declare ")
  if (idx > -1) {
    return src.substring(0, idx) + 'export ' + src.substring(idx + 8)
  }

  return "export " + src;
}

function getTypeArgument(node: ts.TypeReferenceNode, index: number) {
  if (!node || !node.typeArguments) {
    return "";
  }
  return node.typeArguments[index].getText();
}

function findRoute(api: IApi, typeName: string, memberName: string) {
  const found = _.find(api.controllers, c => c.name === typeName)
  if (!found) {
    return;
  }
  return _.find(found.routes, r => r.memberName === memberName)
}

function getSource(node: ts.Node) {
  return node.getFullText()
}

export interface IRoutePathPostProcessor {
  process(route: string): string
}

function excludeNodesBySourcePath(paths: string[], nodes: ts.Node[]) {
  return nodes.filter(node => _.every(paths, exclude => node.getSourceFile().fileName.indexOf(exclude) === -1))
}
/** Builds api clients using type information obtained by reflecting the ApiController source code*/
export class ApiClientBuilder {
  constructor(private logger: Logger, private routePathPostProcessor?: IRoutePathPostProcessor) { }

  build(sourcePaths: string[], api: IApi, options: ts.CompilerOptions = {}, excludeReferencesIfIncludes: string[] = [], excludeSourcePathsIfIncludes: string[] = []): IApiClient {
    const reflector = new ApiReflector(this.logger, sourcePaths, options, excludeSourcePathsIfIncludes);
    const result = reflector.reflect();
    const uniqueNodes: ts.Node[] = _.unique(_.flatten(result.dependencies.types.map(t => t.allNodes)))
    excludeReferencesIfIncludes.push("node_modules/typescript")
    const allRefs = excludeNodesBySourcePath(excludeReferencesIfIncludes, uniqueNodes).map(node => makeRef(getSource(node)))
    const apiClient: IApiClient = { refs: _.flatten(allRefs), controllers: [] }
    if (result.dependencies.variables.length) {
      apiClient.refs = [...excludeNodesBySourcePath(excludeReferencesIfIncludes, result.dependencies.variables).map(v => makeRef(getSource(v))), ...apiClient.refs]
    }

    result.api.forEach(cls => {
      if (!cls.name) {
        this.logger.warn(`No class name found`)
        return;
      }

      const controller: IApiClientController = { typeName: cls.name.text, routes: [] };
      const routes = cls.members.filter(m => {
        const items = ApiReflector.getApiAttr(m.decorators, "route");
        return items && items.length > 0
      }) as ts.MethodDeclaration[]
      routes.forEach(r => {
        const rt = r.parameters[0].type as ts.TypeReferenceNode;
        const typeArgs = r.typeParameters && r.typeParameters.map(ta => getSource(ta))
        const typeArgCount = rt.typeArguments ? rt.typeArguments.length : 0
        const name = (r.name as ts.Identifier).text;
        let body = "void"
        let indexStart = 0;
        if (typeArgCount === 4) {
          body = getTypeArgument(rt, 0)
          indexStart = 1;
        } else if (typeArgCount !== 3) {
          this.logger.error(`Unable to extract types from controller type '${controller.typeName}' member: '${name}'`)
          return
        }

        const response = getTypeArgument(rt, indexStart)
        const params = getTypeArgument(rt, indexStart + 1)
        const query = getTypeArgument(rt, indexStart + 2)
        const route = findRoute(api, controller.typeName, name)
        if (!route) {
          this.logger.error(`Unable to find api route/verbs for controller type '${controller.typeName}' member: '${name}'`)
          return
        }
        route.bodyValidationSpec
        controller.routes.push({
          uniqueName: route.name,
          typeArgs,
          name,
          contentType: route.contentType,
          middleware: route.middlewares,
          bodyType: {
            typeString: body,
            validationSpec: route.bodyValidationSpec
          },
          queryType: {
            typeString: query,
            validationSpec: route.queryValidationSpec
          },
          paramsType: {
            typeString: params,
            validationSpec: route.paramsValidationSpec
          },
          responseType: response,
          verbs: route.verbs,
          route: this.routePathPostProcessor ? this.routePathPostProcessor.process(route.route) : route.route
        })
      })

      if (controller.routes.length) {
        apiClient.controllers.push(controller)
      }
    })
    return apiClient;
  }
}
