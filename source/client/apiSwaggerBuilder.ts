// import * as ts from "typescript";
// import * as _ from "underscore";
// import { ApiReflector } from "./apiReflector";
// import * as Logger from "bunyan";
// import { IApi, IMiddlewareKey } from "../core";
// import * as Swagger from "swagger-schema-official";

// export interface IApiClientRoute {
//   name: string
//   route: string
//   verbs: string[]
//   contentType: string
//   middleware: IMiddlewareKey[] | undefined
//   bodyType: string
//   queryType: string
//   paramsType: string
//   responseType: string
// }

// export interface IApiClientController {
//   typeName: string
//   routes: IApiClientRoute[]
// }


// function getTypeArgument(node: ts.TypeReferenceNode, index: number) {
//   const tn = getTypeArgumentNode(node, index)
//   if (!tn) {
//     return "";
//   }
//   return tn.getText();
// }

// function getTypeArgumentNode(node: ts.TypeReferenceNode, index: number) {
//   if (!node || !node.typeArguments) {
//     return;
//   }
//   return node.typeArguments[index];
// }

// function findRoute(api: IApi, typeName: string, memberName: string) {
//   const found = _.find(api.controllers, c => c.name === typeName)
//   if (!found) {
//     return;
//   }
//   return _.find(found.routes, r => r.memberName === memberName)
// }

// function getOperationRoute(route: string): string {
//   return route.split("/").map(p => {
//     if (p && p.substr(0, 1) === ":") {
//       return `{${p.substr(1).replace(/\?/g, "")}}`
//     }
//     return p
//   }).join("/")
// }

// function getOperationPathParameters(route: string, bodyNode: ts.TypeNode | undefined, checker: ts.TypeChecker): [Swagger.Parameter] | undefined {
//   const params: Swagger.Parameter[] = []
//   route.split("/").map(p => {
//     if (p && p.substr(0, 1) === ":") {
//       params.push({
//         name: p.substr(1).replace(/\?/g, ""),
//         in: "path",
//         required: p.indexOf("?") === -1,
//         type: "string"
//       } as Swagger.PathParameter)
//     }
//   })
//   if (bodyNode) {
//     const body = {
//       "name": "body",
//       "in": "body",
//       "required": true,
//       "schema": getTypeDefintion(bodyNode, checker)
//     } as Swagger.BodyParameter
//     params.push(body)
//   }
//   return params.length ? params as any : undefined;
// }

// function integerEnum(name: string, members: { name: string, value: number }[]): Swagger.Schema {
//   const schema: Swagger.Schema = SystemTypes.integer()
//   schema.enum = members as any
//   schema.readOnly = true
//   return schema
// }
// function stringEnum(name: string, members: { name: string, value: string }[]): Swagger.Schema {
//   const schema: Swagger.Schema = SystemTypes.string()
//   schema.enum = members as any
//   schema.readOnly = true
//   // schema["enumType"] = name
//   // schema["enumName"] = name
//   // schema["enumMembers"] = members
//   return schema
// }

// function hasFlag(flags: ts.TypeFlags, flag: ts.TypeFlags) {
//   return (flags & flag) === flag
// }

// class SystemTypes {
//   static integer() {
//     return {
//       type: "integer",
//       format: "int32"
//     }
//   }
//   static long() {
//     return {
//       type: "integer",
//       format: "int64"
//     }
//   }
//   static float() {
//     return {
//       type: "number",
//       format: "float"
//     }
//   }
//   static double() {
//     return {
//       type: "number",
//       format: "double"
//     }
//   }
//   static string() {
//     return {
//       type: "string"
//     }
//   }
//   static byte() {
//     return {
//       type: "string",
//       format: "byte"
//     }
//   }
//   static binary() {
//     return {
//       type: "string",
//       format: "binary"
//     }
//   }
//   static dateTime() {
//     return {
//       type: "string",
//       format: "date-time"
//     }
//   }
//   static date() {
//     return {
//       type: "string",
//       format: "date"
//     }
//   }
//   static boolean() {
//     return {
//       type: "boolean"
//     }
//   }
//   static password() {
//     return {
//       type: "string",
//       format: "password"
//     }
//   }
//   static object(properties?: { [key: string]: Swagger.Schema }): Swagger.Schema {
//     return {
//       type: "object",
//       properties
//     }
//   }
// }

// function getTypeDefintion(type: ts.TypeNode, checker: ts.TypeChecker): Swagger.Schema | Swagger.Schema[] {
//   switch (type.kind) {
//     case ts.SyntaxKind.ArrayType:
//       const at = type as ts.ArrayTypeNode
//       return {
//         type: "array",
//         items: getTypeDefintion(at.elementType, checker)
//       }
//     case ts.SyntaxKind.NumberKeyword:
//       return SystemTypes.integer()
//     case ts.SyntaxKind.AnyKeyword:
//       return SystemTypes.object()
//     case ts.SyntaxKind.BooleanKeyword:
//       return SystemTypes.boolean()
//     case ts.SyntaxKind.StringKeyword:
//       return SystemTypes.string()
//     case ts.SyntaxKind.IntersectionType:
//       const it = type as ts.IntersectionTypeNode
//       return { allOf: it.types.map(t => getTypeDefintion(t, checker) as Swagger.Schema) as any }
//     case ts.SyntaxKind.UnionType:
//       const ut = type as ts.UnionTypeNode
//       return _.flatten(ut.types.map(t => getTypeDefintion(t, checker)))
//     case ts.SyntaxKind.TypeReference:
//       const tr = type as ts.TypeReferenceNode
//       const t = checker.getTypeAtLocation(tr)

//       if (hasFlag(t.flags, ts.TypeFlags.Object)) {
//         return {
//           "$ref": `#/definitions/${tr.typeName.getText()}`
//         }
//       } else if (hasFlag(t.flags, ts.TypeFlags.Enum)) {
//         const ed = t.symbol && t.symbol.declarations && t.symbol.declarations[0] as ts.EnumDeclaration
//         if (ed) {
//           let last = 0
//           return integerEnum(ed.name.text, ed.members.map((m) => {
//             if (m.initializer) {
//               last = parseInt(m.initializer.getText())
//             } else {
//               last += 1;
//             }
//             return { name: m.name.getText(), value: last }
//           }))
//         }
//       }
//       else if (hasFlag(t.flags, ts.TypeFlags.Union) || hasFlag(t.flags, ts.TypeFlags.Intersection)) {
//         const ta = t.aliasSymbol && t.aliasSymbol.declarations && t.aliasSymbol.declarations[0] as ts.TypeAliasDeclaration
//         if (ta) {
//           const utn = ta.type as ts.UnionOrIntersectionTypeNode
//           if (utn.types.find(t => t.kind === ts.SyntaxKind.LastTypeNode)) {
//             return stringEnum(tr.typeName.getText(), utn.types.map((m, i) => {
//               const typName = getTypeName(m, checker) || "n/a"
//               return { name: typName, value: typName }
//             }))
//           }
//           return getTypeDefintion(utn, checker)
//         }
//       }
//       console.log("UNKNOWN PROP: ", t.pattern, `#/definitions/${tr.typeName.getText()} ### ${ts.TypeFlags[t.flags]} - ${t.flags}`)
//       return {
//         "$ref": `#/definitions/${tr.typeName.getText()} ### ${ts.TypeFlags[t.flags]} - ${t.flags}`
//       }
//     // case "Date":
//     //   return {
//     //     format: "date-time",
//     //     type: "string"
//     //   }
//   }
//   console.log("UNKNOWN PROP TYPE: ", ts.SyntaxKind[type.kind])
//   return { type: `${type.getText()} ### ${ts.SyntaxKind[type.kind]}` }
// }

// function getTypeName(node: ts.Node, checker: ts.TypeChecker) {
//   switch (node.kind) {
//     case ts.SyntaxKind.TypeAliasDeclaration:
//       const ta = node as ts.TypeAliasDeclaration
//       return //ta.name.text
//     case ts.SyntaxKind.InterfaceDeclaration:
//       const id = node as ts.InterfaceDeclaration
//       return id.name.text
//     case ts.SyntaxKind.InterfaceDeclaration:
//       const ed = node as ts.EnumDeclaration
//       return ed.name.text
//     case ts.SyntaxKind.EnumDeclaration:
//       return
//     case ts.SyntaxKind.LastTypeNode:
//       const ll = node as ts.LiteralTypeNode
//       return ll && ll.literal && ll.literal.getText().replace(/\"/g, "")
//   }

//   console.log("UNKNOWN TYPE NAME: ", `${ts.SyntaxKind[node.kind]}`)
//   return node.getText() + " ### " + ts.SyntaxKind[node.kind]
// }
// /** Builds api clients using type information obtained by reflecting the ApiController source code*/
// export class ApiSwaggerBuilder {
//   constructor(private logger: Logger) { }

//   build(sourcePaths: string[], api: IApi, options: ts.CompilerOptions = {}): Swagger.Spec {

//     const defs: { [key: string]: Swagger.Schema } = {}
//     const spec: Swagger.Spec = {
//       swagger: "2.0",
//       info: { title: "my app", version: "v1" },
//       paths: {},
//       definitions: defs
//     }

//     const reflector = new ApiReflector(this.logger, sourcePaths, options);
//     const result = reflector.reflect();

//     result.dependencies.types.forEach(d => {
//       const props: { [key: string]: Swagger.Schema } = {}
//       const typeName = getTypeName(d.node, reflector.checker)
//       if (!typeName) {
//         return
//       }
//       d.type.symbol && d.type.symbol.name
//       d.type.getProperties().forEach(p => {
//         if (!p.declarations || !p.declarations[0] || p.declarations[0].kind !== ts.SyntaxKind.PropertySignature) {
//           return
//         }

//         const ps: ts.PropertySignature = p.declarations[0] as ts.PropertySignature
//         if (ps.type) {
//           props[p.name] = getTypeDefintion(ps.type, reflector.checker) as any
//         }
//       })

//       defs[typeName] = SystemTypes.object(props)
//     })

//     result.api.forEach(cls => {
//       const routes = cls.members.filter(m => {
//         const items = ApiReflector.getApiAttr(m.decorators, "route");
//         return items && items.length > 0
//       }) as ts.MethodDeclaration[]
//       routes.forEach(r => {
//         const rt = r.parameters[0].type as ts.TypeReferenceNode;
//         const typeArgCount = rt.typeArguments ? rt.typeArguments.length : 0
//         const name = (r.name as ts.Identifier).text;
//         let body = "void"
//         let bodyNode: ts.TypeNode | undefined;
//         let indexStart = 0;
//         if (typeArgCount === 4) {
//           body = getTypeArgument(rt, 0)
//           bodyNode = getTypeArgumentNode(rt, 0)
//           indexStart = 1;
//         } else if (typeArgCount !== 3) {
//           this.logger.error(`Unable to extract types from controller type '${cls.name}' member: '${name}'`)
//           return
//         }

//         const response = getTypeArgument(rt, indexStart)
//         const responseNode = getTypeArgumentNode(rt, indexStart)
//         const params = getTypeArgument(rt, indexStart + 1)
//         const query = getTypeArgument(rt, indexStart + 2)
//         const route = findRoute(api, cls.name.text, name)
//         if (!route) {
//           this.logger.error(`Unable to find api route/verbs for controller type '${cls.name}' member: '${name}'`)
//           return
//         }
//         const routePath = getOperationRoute(route.route);
//         const pth = spec.paths[routePath] || {}
//         route.verbs.forEach(v => {
//           const op: Swagger.Operation = {
//             responses: {
//               200: {
//                 description: "OK",
//                 schema: responseNode && getTypeDefintion(responseNode, reflector.checker)
//               } as any
//             },
//             operationId: route.name,
//             tags: [cls.name.text],
//             parameters: getOperationPathParameters(route.route, bodyNode, reflector.checker),
//             produces: response && response !== "void" ? [route.contentType] : undefined,
//             consumes: body && body !== "void" ? ["application/json"] : undefined,
//           }
//           pth[v] = op
//         })
//         spec.paths[routePath] = pth
//       })
//     })
//     return spec;
//   }
// }
