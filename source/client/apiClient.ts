import * as ts from "typescript";
import * as Logger from "bunyan";
import { ApiClientBuilder, IApiClient, IRoutePathPostProcessor } from "./apiClientBuilder";
import { createApi, createDefaultApi } from "../server/api";
import { IApi, IApiController, MiddlewareFunctionDictionary } from "../core";

/** Create the default Api Client instance using the supplied source paths and all @api decorated controllers and middleware functions*/
export function createDefaultApiClient(logger: Logger, sourcePaths: string[], options: ts.CompilerOptions = {}, excludeReferencesIfIncludes: string[] = [], routePathPostProcessor?: IRoutePathPostProcessor, excludeSourcePathIfIncludes: string[] = []): IApiClient {
  return createApiClient(logger, sourcePaths, createDefaultApi(logger), options, excludeReferencesIfIncludes, routePathPostProcessor, excludeSourcePathIfIncludes)
}

/** Create an Api Client instance using the supplied source paths, controllers and middleware functions*/
export function createCustomApiClient(logger: Logger, sourcePaths: string[], apiControllers: IApiController[], middlewareFunctions: MiddlewareFunctionDictionary, options: ts.CompilerOptions = {}, excludeReferencesIfIncludes: string[] = [], routePathPostProcessor?: IRoutePathPostProcessor, excludeSourcePathIfIncludes: string[] = []): IApiClient {
  return createApiClient(logger, sourcePaths, createApi(logger, apiControllers, middlewareFunctions), options, excludeReferencesIfIncludes, routePathPostProcessor, excludeSourcePathIfIncludes)
}

/** Create an Api Client instance using the supplied source paths and api*/
export function createApiClient(logger: Logger, sourcePaths: string[], api: IApi, options: ts.CompilerOptions = {}, excludeReferencesIfIncludes: string[] = [], routePathPostProcessor?: IRoutePathPostProcessor, excludeSourcePathIfIncludes: string[] = []): IApiClient {
  const apiClientBuilder = new ApiClientBuilder(logger, routePathPostProcessor)
  return apiClientBuilder.build(sourcePaths, api, options, excludeReferencesIfIncludes, excludeSourcePathIfIncludes)
}
