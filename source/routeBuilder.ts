import { Shared } from "./shared";
import { ApiExplorer } from "./server/apiExplorer";
import * as Logger from "bunyan";
import { MiddlewareFunctionDictionary, IMiddlewareFunction, IMiddlewareKey, IApiControllerRoute, INewableConstructor, IApiController, IApiRequest, IApiRequestHandler, IRuntimeApi } from "./core";

export abstract class RouteBuilder {
  //private provider: MiddlewareProvider;
  constructor(protected logger: Logger, private controllerConstructor?: INewableConstructor<any>, protected specValidator?: (spec: any, item: any, part: string) => Promise<any>) {
    //this.provider = new MiddlewareProvider(logger,middlewares);
  }

  protected abstract createRequestHandler(route: IApiControllerRoute, routeHandler: IApiRequestHandler<any, any, any, any, any>): Function;
  protected abstract setupRoute(route: IApiControllerRoute, verb: string, requestHandlers: Function[]): void;
  protected abstract createValidatorMiddleware(route: IApiControllerRoute): Function | undefined;

  private async validateRequestInput(req: {}, part: string, validate?: any) {
    if (!validate) {
      return;
    }
    if (!this.specValidator) {
      return;
    }
    req[part] = await this.specValidator(validate, req[part], part)
  }

  protected async enforceRequestInputValidation(req: {}, route: IApiControllerRoute, body: string, params: string, query: string) {
    await this.validateRequestInput(req, body, route.bodyValidationSpec)
    await this.validateRequestInput(req, params, route.paramsValidationSpec)
    await this.validateRequestInput(req, query, route.queryValidationSpec)
  }

  build(runtimeApi: IRuntimeApi) {
    if (runtimeApi.errors && runtimeApi.errors.length) {
      return false;
    }

    //const mws = this.provider.get(api.controllers);
    ApiExplorer.forEachControllerRoute(runtimeApi.controllers, (c, r) => this.processControllerRoute(c, r, runtimeApi.middlewareFunctions))
    return true;
  }

  private getMiddleware(mw: IMiddlewareKey, mwf: IMiddlewareFunction): Function {
    if (!mw.params || mw.params.length === 0) {
      return mwf.func;
    } else {
      return mwf.func(...mw.params)
    }
  }

  private createRouteMiddlewares(route: IApiControllerRoute, middlewareFunctions: MiddlewareFunctionDictionary) {
    const routeMiddleware: Function[] = []
    route.middlewares && route.middlewares.forEach(mw => {
      const found = middlewareFunctions[mw.key]
      if (!found) {
        this.logger.error(`Unable to find middleware for route ${route.name} key '${mw.key}'`)
        return
      }
      routeMiddleware.push(this.getMiddleware(mw, found))
    })
    return routeMiddleware
  }

  private processControllerRoute(c: IApiController, r: IApiControllerRoute, middlewareFunctions: MiddlewareFunctionDictionary) {
    const routeHandler = (handler: IApiRequest<any, any, any, any, any>) => {
      const instance = Shared.construct(c.controllerClass, c.name, this.controllerConstructor);
      this.invokeRouteFunction(r.func, instance, handler)
    }

    const routeMiddleware = this.createRouteMiddlewares(r, middlewareFunctions);
    const vmw = this.createValidatorMiddleware(r)
    if (vmw) {
      routeMiddleware.push(vmw)
    }

    routeMiddleware.push(this.createRequestHandler(r, routeHandler))
    r.verbs.forEach(rv => {
      this.setupRoute(r, rv, routeMiddleware);
    })
  }

  protected invokeRouteFunction(func: Function, instance: any, handler: IApiRequest<any, any, any, any, any>) {
    return new Promise(res => res(func.apply(instance, [handler])))
  }
}
