
export const enum ConstEnum {
  Fred = 1,
  Dave,
  Andrew
}
export enum NonConstEnum {
  Freddy = 1,
  Davey,
  Andy
}

export abstract class SimpleClassBase {
  simpleString!: string
  simpleNumber!: number
  simpleBoolean!: boolean
  simpleAny: any
  constEnum!: ConstEnum
  nonConstEnum!: NonConstEnum
}

export class SimpleClass extends SimpleClassBase {

  simpleStringOptional?: string
  simpleNumberOptional?: number
  simpleBooleanOptional?: boolean
  simpleAnyOptional?: any
  constEnumOptional?: ConstEnum
  nonConstEnumOptional?: NonConstEnum
}

export class ArrayClass {
  simpleStringArrayOptional?: string[]

  simpleStringArray!: string[]
  simpleNumberArray!: number[]
  simpleBooleanArray!: boolean[]
  simpleAnyArray!: any[]

  complexArray!: (SimpleClass | string)[]
  complexAnonArray!: { simple: SimpleClass, name?: string }[]
}

export interface IUnionIntersectionAnonInterface {
  simpleString: string | number
  simpleNumber: ArrayClass & SimpleClass
  simpleBoolean: { name: boolean, arrays: ArrayClass[], simple: SimpleClass }
  simpleAny: any
}
