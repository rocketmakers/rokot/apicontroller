import { api } from "../../decorators";
import { IExpressApiRequest } from "../../express/routeBuilder";

export type IRequest<TBody, TResponse, TParams, TQuery> = IExpressApiRequest<TBody, TResponse, TParams, TQuery>
export type IGetRequest<TResponse, TParams, TQuery> = IExpressApiRequest<void, TResponse, TParams, TQuery>

const Help = "XHELP"
const Me = "XME"

type Helps = typeof Help | typeof Me


@api.controller("SimpleController", "/simple")
class SimpleController {


  @api.route(":id")
  @api.verbs("put")
  @api.routeMetadata({ name: "keith" })
  hidden(req: IGetRequest<Helps, { id: string }, { optionalThing?: string }>) {
    req.send(200, null)
  }

  @api.route(":id")
  @api.verbs("post")
  @api.routeMetadata({ name: "keith" })
  hiddenPost(req: IRequest<{ boom: boolean }, Helps, { id: string }, void>) {
    req.send(200, null)
  }
}

export interface IData { }

export class DataAttributes {
  //private static dict: {[key: string] : any} = []
  static dataItem(key: string): ClassDecorator {
    return (f) => {
      f["___dataKey__"] = key
    };
  }

  static getKeyFor<T extends IData>(cls: new () => T): string {
    return cls["___dataKey__"]
  }
}

@DataAttributes.dataItem("hello")
export class INotificationContent implements IData {
  content!: string

  id?: string
  updatedOn?: string
  deletedOn?: string
  dataTypeKey!: string
  dataKey!: string
  languageKey!: string
}

interface IApiGenIncludePropertyTypes {
  props: DataAttributes
  content: INotificationContent
}

