import { expect } from "rokot-test";
import * as _ from "underscore";
import { ConsoleLogger } from "rokot-log";
import "chai-as-promised";
const logger = ConsoleLogger.create("api-client-test", { level: "trace" });
import * as path from "path"
import { createDefaultApiClient } from "../../client/apiClient";
import "./typesControllers"
import { expressClientWriter } from "../../express/clientWriter";
describe("Api Reflector", () => {
  //   it("should resolve all types", () => {
  //     //debugger
  //     const source = path.resolve("./source/test/reflector/typesControllers.ts")
  //     const tsConfigPath = path.resolve("./source/tsconfig.json")

  //     const reflector = new ApiReflector(logger, [source], require(tsConfigPath))
  //     const data = reflector.reflect()
  // //    console.log(data.dependencies.map(d => d.node.getFullText()));

  //     expect(true).to.eq(true)
  //   })
  it("should generate apiclient", function () {
    this.timeout(0)
    //debugger
    const source = path.resolve("./source/test/reflector/typesControllers.ts")
    const tsConfigPath = path.resolve("./source/tsconfig.json")
    const api = createDefaultApiClient(logger, [source], require(tsConfigPath))
    // const reflector = new ApiReflector(logger, [source], require(tsConfigPath))
    // const data = reflector.reflect()
    console.log(JSON.stringify(api, null, 1));
    expressClientWriter("out/client.ts", api, { mode: "All" })
    expect(true).to.eq(true)
  })
})
