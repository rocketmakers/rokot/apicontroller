import { IStringDictionary, HttpVerb, IApiController, IApiControllerRoute, INewable, IMiddlewareKey, MiddlewareFunctionDictionary, IRouteValidationSpec } from "./core";
import 'reflect-metadata';
import { DecoratorStore, getGlobalCache, IPropertyMetadata } from "./decoration";
import { Shared } from "./shared";

interface IApiRoute {
  path: string
  func: Function
}

interface IApiRouteMiddleware {
  keys: IMiddlewareKey[]
}

interface IApiValidatorSpec<T> {
  spec: T
}

interface IApiRouteVerb {
  verbs: HttpVerb[]
}
interface IApiRouteContentType {
  contentType: string
}
interface IApiRouteMetadata<T> {
  metadata: T
}

interface IApiItemDecoration extends IPropertyMetadata {
  route: IApiRoute
  middleware?: IApiRouteMiddleware
  verb?: IApiRouteVerb
  contentType?: IApiRouteContentType
  metadata?: IApiRouteMetadata<any>
  bodyValidationSpec?: any
  queryValidationSpec?: any
  paramsValidationSpec?: any
}

interface IApiDecoration {
  name: string
  routePrefix?: string
  middlewares?: IMiddlewareKey[]
  controllerClass: INewable<IApiController>;
  items?: IApiItemDecoration[]
}

const apiDecorators = new DecoratorStore<IApiDecoration>("controller")
export const validationSpecDictionary: IStringDictionary<IRouteValidationSpec> = {}

/** the complete set of Api Controllers that have been loaded (indicated via @api.controller) */
export const apiControllers: IApiController[] = getGlobalCache<IApiController[]>('apiControllers', [])

/** the complete set of Middleware functions that have been loaded (indicated via @api.middlewareFunction or @api.middlewareProviderFunction) */
export const middlewareFunctions: MiddlewareFunctionDictionary = getGlobalCache<MiddlewareFunctionDictionary>('middlewareFunctions', {})

/** Register a named middleware function */
export function registerMiddlewareFunction(key: string, func: Function) {
  middlewareFunctions[key] = { key, func }
}

/** Register a function that can be called with a specified min/max number of parameters that will return a middleware function */
export function registerMiddlewareProvider(key: string, func: Function, paramMin: number, paramMax = paramMin) {
  middlewareFunctions[key] = { key, func, paramMin, paramMax }
}

function makeApiController(decorator: IApiDecoration): IApiController {
  const c = {
    name: decorator.name,
    routes: !decorator.items ? [] : decorator.items.map(i => buildApiControllerRoute(i, decorator)),
    controllerClass: decorator.controllerClass
  }

  apiControllers.push(c)
  return c
}

function buildApiControllerRoute(apiItem: IApiItemDecoration, api: IApiDecoration): IApiControllerRoute {
  const memberRoute = apiItem.route ? apiItem.route.path : Shared.defaultRoute()
  const middlewareKeys = apiItem.middleware ? apiItem.middleware.keys : Shared.defaultMiddleware()
  const contentType = apiItem.contentType ? apiItem.contentType.contentType : Shared.defaultContentType()
  const acceptVerbs = apiItem.verb ? apiItem.verb.verbs : Shared.defaultVerbs(apiItem.propertyName)
  const metadata = apiItem.metadata ? apiItem.metadata.metadata : undefined
  const name = Shared.makeRouteName(api.name, apiItem.propertyName);
  const route = Shared.makeRoute(api.routePrefix, memberRoute);
  const verbs = acceptVerbs;
  let middlewares: IMiddlewareKey[] | undefined;
  if (api.middlewares) {
    if (middlewareKeys) {
      middlewares = [...api.middlewares, ...middlewareKeys];
    } else {
      middlewares = api.middlewares;
    }
  } else {
    middlewares = middlewareKeys;
  }

  const spec = createRouteValidationSpec(apiItem)
  if (spec) {
    validationSpecDictionary[name] = spec
  }

  return {
    name,
    memberName: apiItem.propertyName,
    route,
    verbs,
    metadata,
    contentType,
    middlewares,
    func: apiItem.route.func,
    bodyValidationSpec: apiItem.bodyValidationSpec && apiItem.bodyValidationSpec.spec,
    paramsValidationSpec: apiItem.paramsValidationSpec && apiItem.paramsValidationSpec.spec,
    queryValidationSpec: apiItem.queryValidationSpec && apiItem.queryValidationSpec.spec,
  };
}

function createRouteValidationSpec(route: IApiItemDecoration) {
  if (route.bodyValidationSpec || route.paramsValidationSpec || route.queryValidationSpec) {
    const spec: IRouteValidationSpec = {}
    if (route.bodyValidationSpec) {
      spec.body = route.bodyValidationSpec
    }
    if (route.paramsValidationSpec) {
      spec.params = route.paramsValidationSpec
    }
    if (route.queryValidationSpec) {
      spec.query = route.queryValidationSpec
    }
    return spec
  }
}

function buildMiddlewareKey(key: string, params: any[]): IMiddlewareKey {
  if (!params || !params.length) {
    return { key }
  }
  return { key, params }
}

export interface IMiddlewareKeyBuilder {
  add(key: string, ...params: any[]): IMiddlewareKeyBuilder
}

class MiddlewareKeyBuilder implements IMiddlewareKeyBuilder {
  keys: IMiddlewareKey[] = []
  add(key: string, ...params: any[]): IMiddlewareKeyBuilder {
    this.keys.push(buildMiddlewareKey(key, params))
    return this;
  }
}

export class Api {
  /** Indicate a named middleware function */
  middlewareFunction(key: string) {
    return function (target: any, methodName: string, descriptor?: PropertyDescriptor) {
      registerMiddlewareFunction(key, target[methodName]);
    }
  }

  /** Indicate that this function can be called with a specified min/max number of parameters, and will return a middleware function */
  middlewareProviderFunction(key: string, paramMin: number, paramMax = paramMin) {
    return function (target: any, methodName: string, descriptor?: PropertyDescriptor) {
      registerMiddlewareProvider(key, target[methodName], paramMin, paramMax)
    }
  }

  /** Indicates a class (the Api Controller) whose members provide REST route methods */
  controller<T>(name: keyof T, routePrefix?: string, middlewareBuilder?: (b: IMiddlewareKeyBuilder) => void): ClassDecorator;
  controller(name: string, routePrefix?: string, middlewareBuilder?: (b: IMiddlewareKeyBuilder) => void): ClassDecorator;
  controller(name: string, routePrefix?: string, middlewareBuilder?: (b: IMiddlewareKeyBuilder) => void): ClassDecorator {
    return apiDecorators.fromClass(name, (t) => {
      let middlewares: IMiddlewareKey[] | undefined;
      if (middlewareBuilder) {
        const mwb = new MiddlewareKeyBuilder()
        middlewareBuilder(mwb)
        middlewares = mwb.keys
      } else {
        middlewares = undefined
      }
      return { name, middlewares, routePrefix, controllerClass: t as any }
    }, makeApiController, ["route", "middleware", "verb", "bodyValidationSpec", "queryValidationSpec", "paramsValidationSpec", "contentType", "metadata"])
  }

  /** Indicates an Api Controller route (expressing the route path as something your engines routing understands) (can define ONLY ONCE per route - last one wins!) */
  route(path?: string) {
    return apiDecorators.memberCollect<IApiRoute>("route", (t, propertyName, type) => ({ propertyName, path: path || "", func: t[propertyName] }))
  }

  /** Associate ONE middleware with the route (can define multiple per route)*/
  middleware(key: string, ...params: any[]) {
    return apiDecorators.memberCollect<IApiRouteMiddleware>("middleware", (t, propertyName, type) => ({ propertyName, keys: [buildMiddlewareKey(key, params)] }))
  }

  /** Implicitly define the REST verb(s) to be used by the route (can define ONLY ONCE per route - last one wins!)*/
  verbs(...verbs: HttpVerb[]) {
    return apiDecorators.memberCollect<IApiRouteVerb>("verb", (t, propertyName, type) => ({ propertyName, verbs }))
  }

  /** Specify the routes accepted content type */
  contentType(contentType: string) {
    return apiDecorators.memberCollect<IApiRouteContentType>("contentType", (t, propertyName, type) => ({ propertyName, contentType }))
  }

  /** Define arbitrary metadata attached to the route */
  routeMetadata<T>(metadata: T) {
    return apiDecorators.memberCollect<IApiRouteMetadata<T>>("metadata", (t, propertyName, type) => ({ propertyName, metadata }))
  }

  /** Define a validation spec for the request body (can define ONLY ONCE per route - last one wins!)*/
  bodyValidationSpec<T>(spec: T) {
    return this.validator<T>(spec, "bodyValidationSpec")
  }

  /** Define a validation spec for the request query string (can define ONLY ONCE per route - last one wins!)*/
  queryValidationSpec<T>(spec: T) {
    return this.validator<T>(spec, "queryValidationSpec")
  }

  /** Define a validation spec for the expected url parameters (can define ONLY ONCE per route - last one wins!)*/
  paramsValidationSpec<T>(spec: T) {
    return this.validator<T>(spec, "paramsValidationSpec")
  }

  private validator<T>(spec: T, itemType: string) {
    return apiDecorators.memberCollect<IApiValidatorSpec<T>>(itemType, (t, propertyName, type) => ({ propertyName, spec }))
  }
}

/** The decorators required to define Api Controllers */
export const api = new Api()
