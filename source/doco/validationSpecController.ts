import { api, validationSpecDictionary, IStringDictionary, IRouteValidationSpec } from "../index";
import { IGetRequest } from "./customRequest"; // from file above

@api.controller("ValidationSpecController", "validationSpec")
class ValidationSpecController {
  @api.route()
  get(req: IGetRequest<IStringDictionary<IRouteValidationSpec>, void, void>) {
    req.send(200, validationSpecDictionary)
  }
}
