import { api } from "../index";
import { IRequest, IGetRequest, IUser } from "./customRequest"; // from file above


interface IGroup {
  id: string;
  name: string;
  members: IUser[];
}

/*
Register the MiddlewareController
: all route paths are prefixed with "/middleware"
: all routes use the middleware function "one"
  then the resolved middleware via provider "logger"
  (using "MiddlewareController" as the required param)
*/
@api.controller("MiddlewareController", "/middleware", b => b.add("one").add("logger", "MiddlewareController"))
class MiddlewareController {

  @api.route(":id")
  @api.verbs("get", "options")
  @api.middleware("two")
  @api.middleware("three")
  get(req: IGetRequest<IGroup, { id: string }, void>) {
    req.sendOk({ id: req.params.id, name: "group", members: [{ id: "1", userName: "User 1" }] });
  }

  @api.route()
  @api.verbs("get", "options")
  getAll(req: IGetRequest<IGroup[], void, void>) {
    req.sendOk([
      { id: "1", name: "group", members: [{ id: "1", userName: "User 1" }] }
    ]);
  }

  @api.route()
  @api.contentType("application/x-www-form-urlencoded")
  post(req: IRequest<IGroup, IGroup, void, void>) {
    req.sendCreated(req.body);
  }

  @api.route(":id")
  delete(req: IGetRequest<void, { id: string }, void>) {
    var id = req.params.id;
    req.sendNoContent()
  }
}
