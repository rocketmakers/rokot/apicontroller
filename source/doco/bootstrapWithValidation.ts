import { CustomExpressRouteBuilder } from "./customRequest"; // from file above
import { ApiBuilder, apiControllers, middlewareFunctions } from "../index";
import { ConsoleLogger } from "rokot-log";
import * as express from 'express';
import { Validation, ClientConstraintSpec } from "rokot-validate";

/* part will be "body" | "params" | "query" */
function validate<T>(spec: ClientConstraintSpec<T>, item: any, part: string) {
  return Validation.executeClient<T>(item, spec)
}

export function boot(port: number) {
  const app = express();
  const logger = ConsoleLogger.create("Api Routes", { level: "trace" });

  const apiBuilder = new ApiBuilder(logger)
  const runtimeApi = apiBuilder.buildRuntime(apiControllers, middlewareFunctions)
  if (runtimeApi.errors && runtimeApi.errors.length) {
    console.log("Unable to build api model - Service stopping!")
    return;
  }

  const builder = new CustomExpressRouteBuilder(logger, app, undefined, validate);
  const ok = builder.build(runtimeApi);
  if (!ok) {
    console.log("Unable to build express routes - Service stopping!")
    return;
  }

  app.listen(port, () => {
    console.log(`Server listening on port ${port}!`);
  });
}
