import { registerMiddlewareFunction } from "../index";

registerMiddlewareFunction("four", (req: Express.Request, res: Express.Response, next: () => void) => {
  console.log("four")
  next();
})
