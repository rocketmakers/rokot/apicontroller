import { api } from "../index";
import { IRequest, IUser } from "./customRequest"; // from file above
import { createClientConstraintSpec } from "rokot-validate";

interface IRequireValidation {
  id: string;
  name: string;
  members: IUser[];
}

const bodySpec = createClientConstraintSpec<IRequireValidation>(b => {
  return {
    id: { absence: true },
    name: b.stringMandatory(),
    members: b.arrayValidator<IUser>({ id: b.stringMandatory(), userName: b.stringMandatory() })
  }
})

@api.controller("ValidatedController", "/validated")
class ValidatedController {

  @api.route()
  @api.bodyValidationSpec(bodySpec)
  post(req: IRequest<IRequireValidation, IRequireValidation, void, void>) {
    req.sendCreated(req.body);
  }
}
