import { IRoutePathPostProcessor } from "../client/apiClientBuilder";

export class Express4SplatRoutePathPostProcessor implements IRoutePathPostProcessor {
    process(route: string): string {
        const index = route ? route.indexOf("(") : -1
        if (index > -1) {
            return route.substr(0, index)
        }
        return route
    }
}