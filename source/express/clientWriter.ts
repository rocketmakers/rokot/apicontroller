import * as _ from "underscore";
import { IApiClient, IApiClientRoute } from "../client/apiClientBuilder";
import * as pathToRegexp from 'path-to-regexp'
import { FileStreamWriter } from "../fileStreamWriter";

/** Writes the api client to a single file, you need to override Fetcher.request to implement fetch */
export interface IExpressClientWriterOptions {
  genericResponseWrapper?: (type: string) => string
  mode?: "All" | "Definitions" | "Controllers"
  /** use this feature to use a single parameter on each controller endpoint instead of one for each body, query, param, header */
  monoParam?: boolean
}

function makeRouteName(route: IApiClientRoute, verb: string, index: number) {
  return `${route.name}${index > 0 ? "_" + verb : ""}`
}

export function expressClientWriter(outFile: string, apiClient: IApiClient, options?: IExpressClientWriterOptions, ...imports: string[]) {
  return FileStreamWriter.write(outFile, stream => {
    const mode = options && options.mode || "All"
    if (mode !== "Definitions") {
      if (imports) {
        imports.forEach(i => {
          stream.write(`${i}\n`);
        })
      }
    }

    if (mode !== "Controllers") {
      apiClient.refs.forEach(r => {
        if (mode === "Definitions") {
          stream.write(`${r.indexOf("enum ") === 0 ? "declare " : ""}${r}\n`);
          return
        }
        stream.write(`${r}\n`);
      })
    }

    if (mode !== "Definitions") {

      const responseType = options && options.genericResponseWrapper ? options.genericResponseWrapper("T") : "T"
      stream.write(`
export interface HeaderMap {
  [k: string]: string
}

export interface IFetcher {
  request<T>(url: string, verb: string, contentType: string, body?: any, headers?: HeaderMap): Promise<${responseType}>
  buildQueryString(query?: any): string
  requestValidate?(routeUniqueName: string, paramValues: {body?:any, params?: any, query?: any}) : Promise<void>
}

export class Qs {
  private static buildQueryStringKeyValueItem(key: string, value: any): string {
    return \`\${encodeURIComponent(key)}=\${encodeURIComponent(value)}\`;
  }

  static buildQueryStringKeyValue(key: string, value: any): string {
    if (Array.isArray(value)) {
      return value.map(v => Qs.buildQueryStringKeyValueItem(key, v)).join('&');
    }
    return Qs.buildQueryStringKeyValueItem(key, value);
  }

  static buildQueryString(query?: any, buildQueryStringValue?: (key: string, value: any) => string): string {
    if (!query) {
      return '';
    }
    const fn = buildQueryStringValue || Qs.buildQueryStringKeyValue;
    const keys = Object.keys(query).filter(k => !!query[k]);
    const qs = keys.map(k => fn(k, query[k])).join('&');
    if (qs) {
      return \`?\${qs}\`;
    }
    return '';
  }
}

export function optionalParam(parameter: any, key: string) {
  return parameter && parameter[key] ? \`/\${parameter[key]}\` : ""
}

`);
      apiClient.controllers.forEach(controller => {
        stream.write(`
export class ${controller.typeName} {
  constructor(private fetcher: IFetcher){}
`);
        stream.write(controller.routes.map(route => {
          const verbs = route.verbs
          const hasValidation = !!route.bodyType.validationSpec || !!route.queryType.validationSpec || !!route.paramsType.validationSpec
          let routePath = resolveRoutePath(route, hasValidation, options.monoParam)
          if (!isVoid(route.queryType.typeString)) {
            routePath += "${this.fetcher.buildQueryString(" + (hasValidation ? "paramValues." : options.monoParam ? "request." : "") + "query)}"
          }
          const responseType = options && options.genericResponseWrapper ? options.genericResponseWrapper(route.responseType) : route.responseType
          const params = _.filter([formatTypeParam("body", route.bodyType.typeString), formatTypeParam("params", route.paramsType.typeString), formatTypeParam("query?", route.queryType.typeString)], q => !!q).join(",")
          const paramValues = _.filter([formatTypeParamVariable("body", route.bodyType.typeString, options.monoParam), formatTypeParamVariable("params", route.paramsType.typeString, options.monoParam), formatTypeParamVariable("query?", route.queryType.typeString, options.monoParam)], q => !!q).join(",")
          let validate = ''
          if (hasValidation) {
            validate = `const paramValues = {${paramValues}}
    if (this.fetcher.requestValidate) {
      await this.fetcher.requestValidate("${route.uniqueName}", paramValues)
    }
    `
          }
          const bodyParam = isVoid(route.bodyType.typeString) ? "undefined" : `${hasValidation ? "paramValues." : options.monoParam ? "request.": ""}body`
          return verbs.map((verb, index) => `
  ${makeRouteName(route, verb, index)} = async ${route.typeArgs ? `<${route.typeArgs.join(",")}>` : ""}(${options.monoParam ? "request: { " : ""}${params}${params ? ", " : ""}headers?: HeaderMap${options.monoParam ? " }": ""}): Promise<${responseType}> => {
    ${validate}return await this.fetcher.request<${route.responseType}>(\`${routePath}\`, "${verb}", "${route.contentType}", ${bodyParam}, ${options.monoParam ? "request.": ""}headers)
  }`).join("\n")
        }).join("\n"));
        stream.write(`
}`);
      })

      stream.write(`
export class ApiClient {
  constructor(private fetcher: IFetcher){}
`);

      apiClient.controllers.forEach(controller => {
        stream.write(`
  ${makeInstanceName(controller.typeName)} = new ${controller.typeName}(this.fetcher)
  `);
      })

      stream.write(`
}
`);
    }
  })

}

function makeInstanceName(name: string) {
  const idx = name.indexOf("Controller")
  if (idx > -1) {
    name = name.substr(0, idx)
  }
  return name.substr(0, 1).toLowerCase() + name.substr(1)
}

function resolveRoutePath(r: IApiClientRoute, hasValidation: boolean, monoParam: boolean) {
  let route = r.route;
  const keys: { name: string, optional: boolean }[] = []
  pathToRegexp(route, keys as any)
  keys.forEach(k => {
    if (k.optional) {
      route = route.replace(`/:${k.name}?`, `\${optionalParam(${hasValidation ? "paramValues." : monoParam ? "request." : ""}params, "${k.name}")}`)
      return
    }
    route = route.replace(":" + k.name, `\${${hasValidation ? "paramValues." : monoParam ? "request." : ""}params.${k.name}}`)
  })
  return route
}

function isVoid(type: string) {
  return !type || type === "void"
}

function formatTypeParam(name: string, type: string) {
  if (isVoid(type)) {
    return;
  }
  return `${name}: ${type}`
}

function formatTypeParamVariable(name: string, type: string, monoParam: boolean) {
  if (isVoid(type)) {
    return;
  }

  const key = `${name.replace(/\?/g, "")}`;

  return key + (monoParam ? `: request.${key}` : "")
}
