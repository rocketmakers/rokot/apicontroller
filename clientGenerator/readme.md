# Rokot Api Controller Client Generator

This tool is used to find all controller entry points which are decorated using `rokot-apicontroller` within a given source, and then generate representations of those endpoints in various formats.

The tool will process the entry points into a language agnostic representation and then forward them to configured `interpreters`.

Currently the tool has an interpreter that will output content in the following formats
1. OpenApi
2. Typescript

## How To Use

There is a single entry point, `generate`, which takes a `@rocketmakers/logger` instance and a configuration object.

You can also run the `index.js` using node, which will start a CLI for the tool.

### Configuration

| Argument | Description |
|-|-|
| entryPoint | The path to the ts entry point for the controller definitions |
| exclusionPaths | The collection of strings that if contained in a discovered source from the provided entry point will be ignored. |
| tsConfigPath | The path to the ts config to describe how the entry point should be processed |
| overridesPath | The path to a file that contains overrides on how to describe discovered types |
| interpreters | This is a collection of tuples, that contains the format to output, and where to output the generated file. |
| metadata | Metadata about the service that will be included in certain outputs |

## How to extend

There are two main parts for the generator

### Node Procesor

This takes the discovered endpoints and processes the types into a language agnostic representation. This will need expanding when `TypeScript` introduces new features that we'll want to support and know how to process.

### Interpreters

Interpreters are how we take the language agnositic representation and convert it into a language specific representation. This is where you'll come if we want to output in new languages. 

Once a new interpreter is built, it will need to be registered with the `generate` function.

## Design Decisions

You can find explainations for certain design decisions in its [dedicated document](./designDecisions.md).

## Release

A new version of the client will be published to `npm` once merged into `master`. Therefore, you'll need to remember to update the version in [package.json](./package.json)