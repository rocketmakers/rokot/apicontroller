import { Args } from '@rocketmakers/shell-commands/lib/args';
import { createLogger, setDefaultLoggerLevel } from '@rocketmakers/shell-commands/lib/logger';
import { Prerequisites } from '@rocketmakers/shell-commands/lib/prerequisites';
import { InterpreterKind, IInterpreterConfig, generate } from './generate';
import { IClientMetadata } from './interpreters';
import { assertNever } from './utils';

async function run() {
  const args = await Args.match({
    log: Args.single({
      description: 'The log level',
      shortName: 'l',
      defaultValue: 'info',
      validValues: ['trace', 'debug', 'info', 'warn', 'error', 'fatal'],
    }),
    entryPoint: Args.single({
      description: 'The entry point to generate the client from',
      shortName: 'i',
      mandatory: true,
    }),
    tsConfig: Args.single({
      description: 'The path to the ts config to use',
      shortName: 'tsc',
    }),
    interpreters: Args.multi({
      description: 'The interpreter to use',
      shortName: 'int',
      mandatory: true,
      validValues: ['openapi', 'typescript'],
    }),
    outputs: Args.multi({
      description: 'The path the generated client should be saved to',
      shortName: 'o',
      mandatory: true,
    }),
    exclusionPaths: Args.multi({
      shortName: 'e',
      description: 'Values to ignore',
    }),
    overridesPath: Args.single({
      shortName: 'op',
      description: 'The path to the json that contains typing overrides',
    }),
    server: Args.multi({
      shortName: 's',
      description: 'A optional server that the client can be used against',
    }),
    name: Args.single({
      shortName: 'n',
      description: 'The name of the set of services',
      defaultValue: 'RocketmakersClient',
    }),
    version: Args.single({
      shortName: 'v',
      description: 'The version of the client',
      defaultValue: '1.0.0',
    }),
    monoParam: Args.switched({
      shortName: 'mp',
      description: 'Determines if typescript client should include mono params',
    }),
  });

  if (!args) {
    return;
  }

  const { log } = args;
  setDefaultLoggerLevel(log as any);
  const logger = createLogger('api-client-builder');

  try {
    await Prerequisites.check();

    if (args.interpreters.length !== args.outputs.length) {
      throw new Error('The same number of interpreters and outputs must exist');
    }

    const metadata: IClientMetadata = {
      name: args.name,
      version: args.version,
      servers: args.server,
    };

    // Build up our interpreters config
    const interpreters: IInterpreterConfig[] = [];
    for (let i = 0; i < args.interpreters.length; i++) {
      const kind: InterpreterKind = args.interpreters[i] as InterpreterKind;
      const outputPath = args.outputs[i];
      switch (kind) {
        case 'openapi':
          interpreters.push({ kind, outputPath });
          break;
        case 'typescript':
          interpreters.push({ kind, outputPath, isMonoParams: args.monoParam });
          break;
        default:
          assertNever(kind, 'Unexpected interpreter kind');
          break;
      }
    }

    await generate(logger, {
      entryPoint: args.entryPoint,
      exclusionPaths: args.exclusionPaths,
      interpreters,
      metadata,
      overridesPath: args.overridesPath,
      tsConfigPath: args.tsConfig,
    });
  } catch (e) {
    logger.error(e.message);
    process.exit(-1);
  }
}

// eslint-disable-next-line @typescript-eslint/no-floating-promises
run();
