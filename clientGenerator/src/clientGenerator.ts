import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { IReflectionResult, ApiReflector } from './apiReflector';
import { BaseClientInterpreter, InterpreterData } from './interpreters';
import { IControllerData, TypeData, ITypeParameterData, IControllerRouteMethod } from './types/metadata';
import { NodeProcessor } from './nodeProcessor';
import { IBuilderContext } from './types/types';
import { buildNameFromContext } from './utils';
import { ITypeOverride } from './types/typeOverride';

export class ClientGenerator {
  private uniqueTypes: InterpreterData[] = [];

  private typeProcessor: NodeProcessor;

  constructor(
    private logger: Logger,
    checker: ts.TypeChecker,
    private interpreters: BaseClientInterpreter[],
    typeOverrides: ITypeOverride[] = []
  ) {
    this.typeProcessor = new NodeProcessor(this.logger, checker, typeOverrides);
  }

  private async writeToInterpreters(data: InterpreterData): Promise<void> {
    // Only write new types
    if (this.uniqueTypes.findIndex(x => x.kind === data.kind && x.name === data.name) >= 0) {
      return;
    }

    for (const interpreter of this.interpreters) {
      const copy = { ...data };
      if (copy.kind === 'controller') {
        copy.routes = copy.routes.map(r => {
          return { ...r, methods: [...r.methods] };
        });
      }

      await interpreter.write(copy);
    }

    this.uniqueTypes.push(data);
  }

  private reportError(message: string) {
    throw new Error(message);
  }

  private async processArgument(context: IBuilderContext, argument: ts.TypeNode): Promise<TypeData | undefined> {
    this.logger.info(
      `Processing argument for '${context.name}' of '${buildNameFromContext(context, { separator: '.' })}'`
    );

    const discoveredTypes = this.typeProcessor.process(context, argument);
    if (discoveredTypes.dependencies) {
      for (const dependencyType of discoveredTypes.dependencies) {
        if (dependencyType.kind === 'array') {
          continue;
        }

        // Only write new types
        await this.writeToInterpreters(dependencyType);
      }
    }

    if (discoveredTypes.main.kind !== 'array') {
      await this.writeToInterpreters(discoveredTypes.main);
    }

    return discoveredTypes.main.kind === 'literal' && discoveredTypes.main.name === 'void'
      ? undefined
      : discoveredTypes.main;
  }

  private getApiDecorator(
    decorators: ts.NodeArray<ts.Decorator> | undefined,
    decoratorName: string
  ): ts.Decorator | undefined {
    return decorators?.find(d => {
      if (ts.isCallExpression(d.expression)) {
        if (ts.isPropertyAccessExpression(d.expression.expression)) {
          return d.expression.expression.getText() === `api.${decoratorName}`;
        }
      }

      return false;
    });
  }

  private getRouteDecorator(
    decorators: ts.NodeArray<ts.Decorator> | undefined,
    decoratorName: string
  ): string | undefined {
    const targetDecorator = this.getApiDecorator(decorators, decoratorName);
    if (targetDecorator && ts.isCallExpression(targetDecorator.expression)) {
      if (targetDecorator.expression.arguments.length > 1) {
        throw new Error(`Decorator 'api.${decoratorName}' had unexpected number of parameters`);
      }

      // Return our text removing any quotes
      return targetDecorator.expression.arguments && targetDecorator.expression.arguments.length > 0
        ? targetDecorator.expression.arguments[0]
            .getText()
            .replace(/'/g, '')
            .replace(/"/g, '')
        : '';
    }

    return undefined;
  }

  private getControllerDecorator(
    decorators: ts.NodeArray<ts.Decorator> | undefined,
    decoratorName: string
  ): { root: string } | undefined {
    const targetDecorator = this.getApiDecorator(decorators, decoratorName);

    if (targetDecorator && ts.isCallExpression(targetDecorator.expression)) {
      if (targetDecorator.expression.arguments.length > 2 || targetDecorator.expression.arguments.length < 1) {
        throw new Error(`Decorator 'api.${decoratorName}' had unexpected number of parameters`);
      }

      // Return our text removing any quotes
      return {
        root:
          targetDecorator.expression.arguments.length === 2
            ? targetDecorator.expression.arguments[1]
                .getText()
                .replace(/'/g, '')
                .replace(/"/g, '')
            : '',
      };
    }

    return undefined;
  }

  private async processApiControllerRouteMethod(
    context: IBuilderContext,
    route: ts.MethodDeclaration
  ): Promise<IControllerRouteMethod & { path: string }> {
    if (route.parameters.length !== 1) {
      this.reportError(`Expected a single parameter in route '${route.name}' of controller '${context.name}'`);
    }

    const routeInputType = route.parameters[0].type as ts.TypeReferenceNode;
    const routeInputTypeArgumentCount = (routeInputType.typeArguments || []).length;

    const metadata: IControllerRouteMethod & { path: string } = {
      name: route.name.getText(),
      method: this.getRouteDecorator(route.decorators, 'verbs') || '',
      path: this.getRouteDecorator(route.decorators, 'route') || '',
      contentType: 'application/json',
    };

    metadata.path = metadata.path.startsWith('/') ? metadata.path.substring(1) : metadata.path;

    // If we have type parameters (e.g. generics), then try to resolve them to concrete types
    const typeParameters: ITypeParameterData[] = [];
    const defTypeParameters = route.typeParameters || [];
    if (defTypeParameters.length > 0) {
      for (let i = 0; i < defTypeParameters.length; i++) {
        const param = defTypeParameters[i];
        try {
          const type = this.typeProcessor.process(context, param);
          typeParameters.push({
            kind: 'type-parameter',
            name: param.name.text,
            type: type.main,
          });
        } catch {
          // If we can't resolve our generic, then treat as any input
          this.logger.trace(`Unable to resolve type parameter '${param.name.text}'. Treating as any type.`);
          typeParameters.push({
            kind: 'type-parameter',
            name: param.name.text,
            type: {
              kind: 'literal',
              name: 'any',
            },
          });
        }
      }
    }

    const routeContext: IBuilderContext = { parent: context, name: metadata.name, kind: 'controller' };
    if (routeInputTypeArgumentCount === 4) {
      const args = routeInputType.typeArguments as ts.NodeArray<ts.TypeNode>;

      metadata.body = await this.processArgument(
        { parent: routeContext, name: 'Body', typeParameters, kind: 'unknown' },
        args[0]
      );
      metadata.response = await this.processArgument(
        { parent: routeContext, name: 'Response', typeParameters, kind: 'unknown' },
        args[1]
      );
      metadata.params = await this.processArgument(
        { parent: routeContext, name: 'Params', typeParameters, kind: 'unknown' },
        args[2]
      );
      metadata.query = await this.processArgument(
        { parent: routeContext, name: 'Query', typeParameters, kind: 'unknown' },
        args[3]
      );
    } else if (routeInputTypeArgumentCount === 3) {
      const args = routeInputType.typeArguments as ts.NodeArray<ts.TypeNode>;

      metadata.response = await this.processArgument(
        { parent: routeContext, name: 'Response', typeParameters, kind: 'unknown' },
        args[0]
      );
      metadata.params = await this.processArgument(
        { parent: routeContext, name: 'Params', typeParameters, kind: 'unknown' },
        args[1]
      );
      metadata.query = await this.processArgument(
        { parent: routeContext, name: 'Query', typeParameters, kind: 'unknown' },
        args[2]
      );
    } else {
      this.reportError(`Unable to extract request/response types from controller route '${route.name}'`);
    }

    return metadata;
  }

  private async processApiController(controllerClass: ts.ClassDeclaration): Promise<void> {
    if (!controllerClass.name) {
      this.reportError('Api controller found with no name');
      return;
    }

    const controllerName = controllerClass.name.text.replace('Controller', '');
    const decorator = this.getControllerDecorator(controllerClass.decorators, 'controller');

    const controller: IControllerData = {
      kind: 'controller',
      name: controllerName,
      root: (decorator?.root.startsWith('/') ? decorator?.root.substring(1) : decorator?.root) || '',
      routes: [],
    };

    const routes = controllerClass.members.filter(m => {
      const items = ApiReflector.getApiAttr(m.decorators, 'route');
      return items && items.length > 0;
    }) as ts.MethodDeclaration[];

    for (const route of routes) {
      const routeMethod = await this.processApiControllerRouteMethod(
        { parent: undefined, name: controller.name, kind: 'controller' },
        route
      );
      const existingRoute = controller.routes.find(x => x.path === routeMethod.path);
      if (existingRoute) {
        existingRoute.methods.push(routeMethod);
      } else {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { path, ...routeWithoutPath } = routeMethod;
        controller.routes.push({
          path: routeMethod.path,
          methods: [routeWithoutPath],
        });
      }
    }

    await this.writeToInterpreters(controller);
  }

  public async process(result: IReflectionResult): Promise<void> {
    // Make sure we're processing our controllers in a deterministic way
    const sortedControllers = result.api.sort((controllerOne, controllerTwo) =>
      (controllerOne.name?.text || '').localeCompare(controllerTwo.name?.text || '')
    );

    for (const api of sortedControllers) {
      await this.processApiController(api);
    }

    // Finalise our interpreters
    for (const interpreter of this.interpreters) {
      await interpreter.finalise();
    }
  }
}
