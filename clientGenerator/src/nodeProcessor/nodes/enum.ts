import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { IEnumData } from '../../types/metadata';
import { getNodeNameSafely } from '../utils';

export class EnumDeclarationProcessor extends BaseNodeProcessor<ts.EnumDeclaration> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.EnumDeclaration);
  }

  public process(context: IBuilderContext, node: ts.EnumDeclaration): NodeProcessorResult {
    this.logger.trace(`Processing enum '${node.name.text}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    const enumData: IEnumData = {
      kind: 'enum',
      name: node.name.text,
      values: [],
    };

    for (const member of node.members) {
      if (ts.isEnumMember(member) === false) {
        throw new Error(`Unexpected member '${member.kind}' in enum`);
      } else if (!member.initializer) {
        throw new Error(`Member expected to have initilizer`);
      }

      enumData.values.push({
        key: getNodeNameSafely(member.name),
        value: getNodeNameSafely(member.initializer)
          .replace(/"/g, '')
          .replace(/'/g, ''),
      });
    }

    return {
      main: enumData,
      dependencies: [],
    };
  }
}
