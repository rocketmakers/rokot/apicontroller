import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { getNodeNameSafely } from '../utils';

/*
 * Type aliases are types that reference the definitions of other types (or even just rereference)
 */
export class IndexedAccessTypeNodeProcessor extends BaseNodeProcessor<ts.IndexedAccessTypeNode> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.IndexedAccessType);
  }

  public process(context: IBuilderContext, node: ts.IndexedAccessTypeNode): NodeProcessorResult {
    this.logger.trace(`Processing index access node '${getNodeNameSafely(node)}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    const type = this.processor.checker.getTypeAtLocation(node);
    return this.processType(context, type);
  }
}
