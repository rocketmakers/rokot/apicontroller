import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { getAllProperties, getNodeNameSafely } from '../utils';
import { assertNever } from '../../utils';

export class TypeOperatorProcessor extends BaseNodeProcessor<ts.TypeOperatorNode> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.TypeOperator);
  }

  public process(context: IBuilderContext, node: ts.TypeOperatorNode): NodeProcessorResult {
    this.logger.trace(`Processing type operator '${getNodeNameSafely(node)}'`);

    switch (node.operator) {
      case ts.SyntaxKind.ReadonlyKeyword:
      case ts.SyntaxKind.UniqueKeyword:
        throw new Error(`Type operator '${node.operator}' is not currently supported`);
      case ts.SyntaxKind.KeyOfKeyword:
        const typeDef = this.processor.process(context, node.type);

        // Get all of our known properties and convert them into an enum
        const allProperties = getAllProperties(typeDef.main, [
          typeDef.main,
          ...typeDef.dependencies,
          ...this.processor.discoveredTypes,
        ]);
        if ('name' in typeDef.main && allProperties.length > 0) {
          return {
            dependencies: [],
            main: {
              kind: 'enum',
              name: `${typeDef.main.name}PropertyKeysEnum`,
              values: allProperties.map(x => {
                return {
                  key: x.name,
                  value: x.name,
                };
              }),
            },
          };
        }

        // Otherwise treat it just like a normal string, as that's what it ultimately boils down to
        return {
          dependencies: [],
          main: {
            kind: 'literal',
            name: 'string',
          },
        };
      default:
        assertNever(node.operator, 'Unexpected type operator');
    }

    throw new Error(`Unable to resolve type operator`);
  }
}
