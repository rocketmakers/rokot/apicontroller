import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { getNodeNameSafely } from '../utils';

export class ExpressionWithTypeArgumentsNodeProcessor extends BaseNodeProcessor<ts.ExpressionWithTypeArguments> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.ExpressionWithTypeArguments);
  }

  public process(context: IBuilderContext, node: ts.ExpressionWithTypeArguments): NodeProcessorResult {
    this.logger.trace(`Processing expression with type arguments '${getNodeNameSafely(node)}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    // Because our expression might be referencing an inbuilt typescript utility, attempt to process it as one first
    const typescriptResult = this.tryProcessTypescriptUtilities(context, node.expression, node.typeArguments);
    if (typescriptResult) {
      return typescriptResult;
    }

    const { typeParameters, dependencies } = this.resolveTypeArguments(context, node.typeArguments);

    const typeExpressionResult = this.processor.process({ ...context, typeParameters }, node.expression);
    dependencies.push(...typeExpressionResult.dependencies);

    return {
      main: typeExpressionResult.main,
      dependencies,
    };
  }
}
