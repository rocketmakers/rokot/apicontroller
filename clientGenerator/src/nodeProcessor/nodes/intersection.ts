import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { IIntersectionData, TypeData } from '../../types/metadata';
import { buildNameFromContext } from '../../utils';
import { getNodeNameSafely } from '../utils';

export class IntersectionNodeProcessor extends BaseNodeProcessor<ts.IntersectionTypeNode> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.IntersectionType);
  }

  public process(context: IBuilderContext, node: ts.IntersectionTypeNode): NodeProcessorResult {
    this.logger.trace(`Processing intersection '${getNodeNameSafely(node)}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    const intersectionData: IIntersectionData = {
      kind: 'intersection',
      name: buildNameFromContext(context, {}),
      options: [],
    };

    const dependencies: TypeData[] = [];

    for (let i = 0; i < node.types.length; i++) {
      const intersectionNode = node.types[i];
      this.logger.trace(`Processing intersection type '${getNodeNameSafely(intersectionNode)}'`);

      const typeResult = this.processor.process(
        { parent: context, name: i.toString(), kind: 'intersection' },
        intersectionNode
      );
      intersectionData.options.push(typeResult.main);
      dependencies.push(...typeResult.dependencies, typeResult.main);
    }

    return {
      main: intersectionData,
      dependencies,
    };
  }
}
