import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { IInterfaceData } from '../../types/metadata';
import { resolveTypeParameters, convertToReferencableData, getNodeNameSafely } from '../utils';
import { NodeWithPropertiesProcessor } from './property';

export class InterfaceDeclarationProcessor extends NodeWithPropertiesProcessor<ts.InterfaceDeclaration> {
  private discoveredTypes: NodeProcessorResult[] = [];

  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.InterfaceDeclaration);
  }

  public process(context: IBuilderContext, node: ts.InterfaceDeclaration): NodeProcessorResult {
    this.logger.trace(`Processing interface '${node.name.text}'`);
    const name = node.name.text;

    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    const interfaceData: IInterfaceData = {
      kind: 'interface',
      name,
      properties: [],
      baseTypes: [],
      supportsAdditionalProperties: false,
    };

    // Check to see if we've seen this interface before. This is to handle self referencing types/circular dependencies which are supported
    // in typescript, but will cause an infinite loop if we don't.
    const foundType = this.discoveredTypes.find(
      x => x.main.kind === interfaceData.kind && x.main.name === interfaceData.name
    );
    if (foundType) {
      this.logger.trace(
        'Type has already been processed (or is in the process of being processed), so returning cached definition'
      );
      return foundType;
    }

    const result: NodeProcessorResult = {
      main: interfaceData,
      dependencies: [],
    };

    this.discoveredTypes.push(result);

    // Process our type parameters (e.g. generics) in case we have generics that we know the types for based on our context
    const { typeParameters, dependencies } = resolveTypeParameters(
      this.logger,
      this.processor,
      context,
      node.typeParameters
    );
    result.dependencies.push(...dependencies);

    if (typeParameters.length > 0) {
      interfaceData.name = `${interfaceData.name}_${this.buildNameFromTypeParameters(typeParameters)}`;
    }

    for (const member of node.members) {
      if (ts.isIndexSignatureDeclaration(member)) {
        interfaceData.supportsAdditionalProperties = true;
        continue;
      }

      const { property, dependencies: propertyDependencies } = this.processProperty(
        { parent: context, name, typeParameters, kind: 'interface' },
        member
      );
      interfaceData.properties.push(property);
      result.dependencies.push(...propertyDependencies);
    }

    // Process any inheritance we may have
    if (node.heritageClauses) {
      for (const clause of node.heritageClauses) {
        for (const type of clause.types) {
          this.logger.trace(`Processing inherited type '${getNodeNameSafely(type)}'`);
          const inheritanceType = this.processor.process({ ...context, typeParameters }, type);
          result.dependencies.push(...inheritanceType.dependencies, inheritanceType.main);
          interfaceData.baseTypes.push(convertToReferencableData(inheritanceType.main));
        }
      }
    }

    return result;
  }
}
