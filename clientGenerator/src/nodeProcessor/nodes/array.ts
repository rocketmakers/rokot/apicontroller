import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { getNodeNameSafely } from '../utils';

export class ArrayNodeProcessor extends BaseNodeProcessor<ts.ArrayTypeNode> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.ArrayType);
  }

  public process(context: IBuilderContext, node: ts.ArrayTypeNode): NodeProcessorResult {
    this.logger.trace(`Processing array type '${getNodeNameSafely(node)}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    const result = this.processor.process(context, node.elementType);
    return {
      main: {
        kind: 'array',
        type: result.main,
      },
      dependencies: [...result.dependencies, result.main],
    };
  }
}
