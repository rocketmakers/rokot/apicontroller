import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';

// We can get into this situation when enums with a single value are referenced. For some reason typescript compiles it down
export class EnumMemberProcessor extends BaseNodeProcessor<ts.EnumMember> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.EnumMember);
  }

  public process(context: IBuilderContext, node: ts.EnumMember): NodeProcessorResult {
    this.logger.trace(`Processing enum member '${node.name}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    if (node.parent.members.length !== 1) {
      throw new Error('Should never process an enum member of an enum with mutliple members');
    }

    return this.processor.process(context, node.parent);
  }
}
