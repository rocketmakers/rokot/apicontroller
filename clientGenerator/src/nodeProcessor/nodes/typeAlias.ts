import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { assertNever } from '../../utils';
import { getName, resolveTypeParameters } from '../utils';
import { IOpaqueTypeDefinitionData, IOpaqueTypeImplementationData } from '../../types/metadata';

/*
 * Type aliases are types that reference the definitions of other types (or even just rereference)
 */
export class TypeAliasDeclarationProcessor extends BaseNodeProcessor<ts.TypeAliasDeclaration> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.TypeAliasDeclaration);
  }

  // We are needing to capture and treat opaque types differently because their underlying objects and how they're
  // used don't play nicely with other formats
  private tryConvertToOpaqueType(typeAliasName: string, data: NodeProcessorResult): NodeProcessorResult {
    // Bit of a hack, but we're interpreting opaque types based on their alias name starting with "Opaque"
    // and they're of a certain shape
    if (data.main.kind === 'intersection' && typeAliasName.toLowerCase().startsWith('opaque')) {
      const intersection = data.main;
      if (intersection.options.length === 2 && intersection.options[0].kind === 'literal') {
        const opaqueTypeType = intersection.options[0];
        if (intersection.options[1].kind === 'interface' || intersection.options[1].kind === 'class') {
          const opaqueDetails = intersection.options[1];
          if (
            opaqueDetails.properties.length === 1 &&
            opaqueDetails.properties[0].name === '__opaque' &&
            opaqueDetails.properties[0].type.kind === 'qualified-type'
          ) {
            // We need to add a record for both the implementation of the opaque type and the definition
            // of the underlying opaque type.
            const opaqueKey = opaqueDetails.properties[0].type;

            const opaqueTypeDef: IOpaqueTypeDefinitionData = {
              kind: 'opaque-type-def',
              name: typeAliasName,
              type: opaqueTypeType,
            };

            const opaqueTypeImpl: IOpaqueTypeImplementationData = {
              kind: 'opaque-type-impl',
              name: typeAliasName,
              type: opaqueTypeDef,
              key: opaqueKey,
            };

            // Check to see if we want to apply an override to our implementation, based on key name no doubt - barf!
            const insensitveKey = opaqueKey.name.toLowerCase();
            if (insensitveKey.endsWith('datetime')) {
              opaqueTypeImpl.restriction = {
                kind: 'literal',
                name: 'datetime',
              };
            } else if (insensitveKey.endsWith('date')) {
              opaqueTypeImpl.restriction = {
                kind: 'literal',
                name: 'date',
              };
            }

            return {
              main: opaqueTypeImpl,
              dependencies: [opaqueTypeDef, opaqueTypeType, opaqueKey],
            };
          }
        }
      }
    }

    return data;
  }

  public process(context: IBuilderContext, declaration: ts.TypeAliasDeclaration): NodeProcessorResult {
    this.logger.trace(`Processing type alias '${declaration.name.text}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    // extract our type parameters and try to resolve them
    const { typeParameters, dependencies } = resolveTypeParameters(
      this.logger,
      this.processor,
      context,
      declaration.typeParameters
    );

    let result = this.processor.process({ ...context, name: declaration.name.text, typeParameters }, declaration.type);

    // See if our underlying type is a special "OpaqueType", and if so convert
    result = this.tryConvertToOpaqueType(declaration.name.text, result);

    result.dependencies.push(...dependencies);

    // Update the name of our definition to be the name of our type alias, as this is what will be referenced
    switch (result.main.kind) {
      case 'class':
      case 'interface':
      case 'enum':
      case 'union':
      case 'intersection':
      case 'qualified-type':
      case 'opaque-type-impl':
        // If our alias has type parameters defined, then it means our underling type is a generic implementation.
        // Therefore we need to update the name to include the used generic types to avoid clashes in implementations.
        const nameSuffix =
          declaration.typeParameters && declaration.typeParameters.length > 0
            ? `_${typeParameters.map(x => (x.type && getName(x.type)) || x.name).join('_')}`
            : '';

        result = {
          main: {
            ...result.main,
            name: `${declaration.name.text}${nameSuffix}`,
          },
          dependencies: result.dependencies,
        };
        break;
      case 'opaque-type-def':
        // Opaque type definitions already have the right names, so we'll ignore
        break;
      case 'array':
        throw new Error('Array is not currently supported for type alias');
      case 'literal':
        // Ignore literal types
        break;
      default:
        assertNever(result.main, 'Unexpected kind');
        break;
    }

    return result;
  }
}
