import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { NodeProcessor } from '..';
import { BaseNodeProcessor } from '.';
import { IBuilderContext } from '../../types/types';
import { IPropertyData, TypeData } from '../../types/metadata';
import { convertToReferencableData, getNodeNameSafely } from '../utils';

export abstract class NodeWithPropertiesProcessor<TNode extends ts.Node> extends BaseNodeProcessor<TNode> {
  constructor(processor: NodeProcessor, logger: Logger, kind: ts.SyntaxKind) {
    super(processor, logger, kind);
  }

  protected processProperty(
    context: IBuilderContext,
    member: ts.ClassElement | ts.TypeElement
  ): {
    property: IPropertyData;
    dependencies: TypeData[];
  } {
    this.logger.trace(`Processing property '${member.name && getNodeNameSafely(member.name)}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    if (ts.isPropertySignature(member) || ts.isPropertyDeclaration(member)) {
      if (!member.type) {
        throw new Error(`Failed to determine type on member '${member.name}'`);
      }

      const propertyName = getNodeNameSafely(member.name)
        .replace(/"/g, '')
        .replace(/'/g, '');

      let property: IPropertyData;
      const dependencies: TypeData[] = [];

      // Check that our property type isn't referencing a type parameter of our parent, as this may occur in certain
      // scenarios (e.g. generics)
      const typeParameter = context.typeParameters?.find(
        x => member.type && x.name === getNodeNameSafely(member.type) && x.type
      );
      if (typeParameter?.type) {
        this.logger.trace('Using type parameter from context');
        property = {
          name: propertyName,
          type: convertToReferencableData(typeParameter.type),
          isNullable: !!member.questionToken,
        };
      } else {
        this.logger.trace(`Processing type of property '${propertyName}'`);
        const propertyResult = this.processor.process(
          { parent: context, name: propertyName, kind: 'unknown' },
          member.type
        );

        property = {
          name: propertyName,
          type: convertToReferencableData(propertyResult.main),
          isNullable: !!member.questionToken,
        };

        dependencies.push(...propertyResult.dependencies, propertyResult.main);
      }

      // Respect the nullable-ness of our underlying type.
      if ('isNullable' in property.type && property.type.isNullable) {
        property.isNullable = property.type.isNullable;
      }

      return {
        property,
        dependencies,
      };
    }

    throw new Error(`Unexpected member declaration type (${member.kind}) on '${context.parent?.name}'`);
  }
}
