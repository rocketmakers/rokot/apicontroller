import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { ITypeParameterData, IUnionData, TypeData } from '../../types/metadata';
import { assertNever, buildNameFromContext } from '../../utils';
import { toPascalCase, findCommonProperties, getAllProperties, convertUnionToEnum, getUniqueItems } from '../utils';

export interface INodeProcessor {
  canProcess(node: ts.Node): boolean;
  process(context: IBuilderContext, node: ts.Node): NodeProcessorResult;
}

export abstract class BaseNodeProcessor<TNode extends ts.Node> implements INodeProcessor {
  // eslint-disable-next-line no-empty-function
  constructor(protected processor: NodeProcessor, protected logger: Logger, private kind: ts.SyntaxKind) {}

  public canProcess(node: ts.Node): boolean {
    return node.kind === this.kind;
  }

  public abstract process(context: IBuilderContext, node: TNode): NodeProcessorResult;

  protected buildNameFromTypeParameters(params: ITypeParameterData[]): string {
    return (
      params
        .map(p => {
          if (!p.type) {
            return undefined;
          }

          switch (p.type.kind) {
            case 'class':
            case 'interface':
            case 'enum':
            case 'union':
            case 'literal':
            case 'intersection':
            case 'qualified-type':
            case 'opaque-type-def':
            case 'opaque-type-impl':
              return p.type.name;
            case 'array':
              return undefined;
            default:
              assertNever(p.type, 'Unexpected kind');
              return undefined;
          }
        })
        .filter(x => !!x)
        .join('_')
        // Can't really have non alphanumeric characters in our name
        .replace(/[^a-zA-Z0-9]/, '_')
    );
  }

  /*
   * Cycle through our type parameters in our context and see if we have any resolved type parameters
   * matching our target name. This is useful for generics that have concrete types further up the tree.
   */
  protected getTypeParameter(context: IBuilderContext, targetName: string): NodeProcessorResult | undefined {
    if (context.typeParameters) {
      const target = context.typeParameters.find(x => x.name === targetName && x.type);
      if (target && target.type) {
        return {
          main: target.type,
          dependencies: [],
        };
      }
    }

    if (context.parent) {
      return this.getTypeParameter(context.parent, targetName);
    }

    return undefined;
  }

  // Process our type parameters as we are probably dealing with a generic and we might have resolved some of our generic types
  // further up the tree
  protected resolveTypeArguments(
    context: IBuilderContext,
    typeArguments: ts.NodeArray<ts.Node> | undefined
  ): { typeParameters: ITypeParameterData[]; dependencies: TypeData[] } {
    const dependencies: TypeData[] = [];
    const typeParameters: ITypeParameterData[] = [];
    const defTypeArguments = typeArguments || [];

    if (defTypeArguments.length > 0) {
      for (let i = 0; i < defTypeArguments.length; i++) {
        const arg = defTypeArguments[i];
        const paramName = arg.getText();

        // Attempt to resolve the argument from our context as it may have been resolved further up the tree
        const resolvedParameter =
          context.typeParameters && context.typeParameters.find(x => x.name === paramName && x.type);
        if (resolvedParameter) {
          typeParameters.push({ ...resolvedParameter, name: paramName });
        } else {
          // Attempt to process the argument as it may be an explicit type (e.g. generic inheritance)
          this.logger.trace(`Processing type argument '${paramName}'`);
          const type = this.processor.process(context, arg);
          typeParameters.push({
            kind: 'type-parameter',
            name: paramName,
            type: type.main,
          });

          dependencies.push(...type.dependencies, type.main);
        }
      }
    }

    return { typeParameters, dependencies };
  }

  protected tryProcessTypescriptUtilities(
    context: IBuilderContext,
    node: ts.Node,
    args: ts.NodeArray<ts.Node> | undefined
  ): NodeProcessorResult | undefined {
    const nodeSymbol = this.processor.checker.getSymbolAtLocation(node);
    if (!nodeSymbol) {
      return undefined;
    }

    const source = nodeSymbol.declarations[0].getSourceFile();
    if (source.fileName.indexOf('node_modules/typescript') >= 0) {
      const { name } = nodeSymbol;
      switch (name.toLowerCase()) {
        // Omits also resolve to picks
        case 'pick':
        case 'omit':
          this.logger.trace(`Processing as '${name}'`);
          this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

          if (!args || args.length !== 2) {
            throw new Error('Unexpected number of arguments');
          }

          const classType = this.processor.process(context, args[0]);
          const utilityPropertyTypes = this.processor.process(context, args[1]);

          if (classType.main.kind !== 'class' && classType.main.kind !== 'interface') {
            throw new Error(`${name} type was expected to be of type class or interface`);
          }

          // Gather our property names from our utility argument
          const specifiedProperties: string[] = [];
          if (utilityPropertyTypes.main.kind === 'union') {
            for (const item of utilityPropertyTypes.main.options) {
              if (item.kind !== 'qualified-type') {
                throw new Error('Expected union to resolve to a qualified type value');
              } else if (item.type.kind !== 'literal' || item.type.name !== 'string') {
                throw new Error('Expected union to resolve to a literal string value');
              }

              specifiedProperties.push(item.value);
            }
          } else if (utilityPropertyTypes.main.kind === 'qualified-type') {
            specifiedProperties.push(utilityPropertyTypes.main.name);
          } else if (utilityPropertyTypes.main.kind === 'enum') {
            for (const item of utilityPropertyTypes.main.values) {
              specifiedProperties.push(item.value);
            }
          } else {
            throw new Error(`${name} type was expected to have second type of union`);
          }

          // Dependent on our utility, we either want to include our defined properties, or exclude them
          const includeDefinedProperties = name.toLowerCase() === 'pick';

          // Our omit/pick type will be it's own interface as we might be omitting properties that we're inheriting
          const allProperties = getAllProperties(classType.main, [
            classType.main,
            ...classType.dependencies,
            ...this.processor.discoveredTypes,
          ]);

          return {
            main: {
              kind: 'interface',
              baseTypes: [],
              name: `${classType.main.name}${name}${specifiedProperties.map(p => toPascalCase(p)).join('')}`,
              properties: allProperties.filter(x => specifiedProperties.includes(x.name) === includeDefinedProperties),
              supportsAdditionalProperties: false,
            },
            dependencies: classType.dependencies,
          };
        case 'partial':
          this.logger.trace(`Processing node as Partial`);
          this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

          if (!args || args.length !== 1) {
            throw new Error('Unexpected number of arguments');
          }

          const utilityType = this.processor.process(context, args[0]);
          if (utilityType.main.kind !== 'class' && utilityType.main.kind !== 'interface') {
            throw new Error(`Omit type was expected to be of type class or interface`);
          }

          const allPartialProperties = getAllProperties(utilityType.main, [
            utilityType.main,
            ...utilityType.dependencies,
            ...this.processor.discoveredTypes,
          ]);

          // Our partial type will be it's own interface as we might be needing to make properties that we're inheriting optional.
          return {
            main: {
              kind: 'interface',
              baseTypes: [],
              name: `${utilityType.main.name}${name}`,
              properties: allPartialProperties.map(x => {
                return { ...x, isNullable: true };
              }),
              supportsAdditionalProperties: false,
            },
            dependencies: utilityType.dependencies,
          };
        default:
          throw new Error(`Unable to handle '${name}'`);
      }
    }

    return undefined;
  }

  // This is a special handle of a certain keyof situation.
  private processUnionType(context: IBuilderContext, type: ts.UnionType) {
    this.logger.trace('Processing type as union');

    const typeSymbol = type.aliasSymbol || type.symbol || type.getSymbol();

    const unionData: IUnionData = {
      kind: 'union',
      name: typeSymbol?.name || buildNameFromContext(context, {}),
      options: [],
      commonProperties: [],
      isNullable: false,
    };

    const dependencies: TypeData[] = [];

    for (const unionType of type.types) {
      const node = unionType.symbol.declarations[0];
      if (ts.isEnumMember(node)) {
        this.logger.trace(`Processing union type '${unionType.symbol.name}' as enum`);
        const enumResult = this.processor.process(context, node.parent);
        unionData.options.push(enumResult.main);
        dependencies.push(...enumResult.dependencies, enumResult.main);
      } else {
        this.logger.trace(`Processing union type '${unionType.symbol.name}'`);
        const result = this.processType(context, unionType);
        unionData.options.push(result.main);
        dependencies.push(...result.dependencies, result.main);
      }
    }

    // When referencing unioned enums, we can end up with duplicates so we need to de-duplicate
    unionData.options = getUniqueItems(unionData.options);

    // If our union is all qualified types, then convert it into an enum
    if (!unionData.options.find(x => x.kind !== 'qualified-type')) {
      return convertUnionToEnum(context, unionData);
    }

    // See if there are any common properties
    unionData.commonProperties.push(...findCommonProperties(unionData.options));

    return {
      main: unionData,
      dependencies,
    };
  }

  protected processType(context: IBuilderContext, type: ts.Type): NodeProcessorResult {
    if (type.isUnion()) {
      return this.processUnionType(context, type);
    }

    const symbol = type.aliasSymbol || type.symbol;
    if (symbol && symbol.declarations) {
      return this.processor.process(context, symbol.declarations[0]);
    }

    throw new Error('Unhandled type');
  }
}
