import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { getNodeNameSafely } from '../utils';

/*
 * This is for things like enum values used as types (e.g. enum values used as a descriminator types)
 */
export class LiteralTypeNodeProcessor extends BaseNodeProcessor<ts.LiteralTypeNode> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.LiteralType);
  }

  public process(context: IBuilderContext, node: ts.LiteralTypeNode): NodeProcessorResult {
    this.logger.trace(`Processing qualified name '${getNodeNameSafely(node)}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    return this.processor.process(context, node.literal);
  }
}
