import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { IInterfaceData } from '../../types/metadata';
import { getNodeNameSafely } from '../utils';
import { NodeWithPropertiesProcessor } from './property';
import { buildNameFromContext } from '../../utils';

/*
 * These are basically anonymous definitions that we need to make concrete.
 */
export class TypeLiteralNodeProcessor extends NodeWithPropertiesProcessor<ts.TypeLiteralNode> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.TypeLiteral);
  }

  public process(context: IBuilderContext, node: ts.TypeLiteralNode): NodeProcessorResult {
    this.logger.trace(`Processing type literal '${getNodeNameSafely(node)}'`);
    const name = buildNameFromContext(context, { separator: '' });

    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    const interfaceData: IInterfaceData = {
      kind: 'interface',
      name,
      properties: [],
      baseTypes: [],
      supportsAdditionalProperties: false,
    };

    const result: NodeProcessorResult = {
      main: interfaceData,
      dependencies: [],
    };

    for (const member of node.members) {
      if (ts.isIndexSignatureDeclaration(member)) {
        interfaceData.supportsAdditionalProperties = true;
        continue;
      }

      const { property, dependencies } = this.processProperty({ parent: context, name, kind: 'literal' }, member);
      interfaceData.properties.push(property);
      result.dependencies.push(...dependencies);
    }

    return result;
  }
}
