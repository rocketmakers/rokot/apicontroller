import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { getNodeNameSafely } from '../utils';

/*
 * This is for things like enum values used as types (e.g. enum values used as a descriminator types)
 */
export class QualifiedNameProcessor extends BaseNodeProcessor<ts.QualifiedName> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.QualifiedName);
  }

  public process(context: IBuilderContext, node: ts.QualifiedName): NodeProcessorResult {
    this.logger.trace(`Processing qualified name '${getNodeNameSafely(node)}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    const typeResult = this.processor.process(context, node.left);

    if (typeResult.main.kind === 'enum') {
      if (!typeResult.main.values.find(x => x.key === node.right.text)) {
        throw new Error("Right hand side of qualified name doesn't belong to left hand side");
      }

      return {
        main: {
          kind: 'qualified-type',
          name: `${typeResult.main.name}.${node.right.text}`,
          type: typeResult.main,
          value: node.right.text,
        },
        dependencies: [...typeResult.dependencies, typeResult.main],
      };
    }
    throw new Error('Currently only enum qualified names are supported');
  }
}
