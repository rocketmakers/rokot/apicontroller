import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { IUnionData, TypeData } from '../../types/metadata';
import { buildNameFromContext } from '../../utils';
import { getNodeNameSafely, findCommonProperties, convertUnionToEnum, getUniqueItems } from '../utils';

export class UnionNodeProcessor extends BaseNodeProcessor<ts.UnionTypeNode> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.UnionType);
  }

  public process(context: IBuilderContext, node: ts.UnionTypeNode): NodeProcessorResult {
    this.logger.trace(`Processing union '${getNodeNameSafely(node)}'`);

    // Check to see if we've been provided an enum member, which can happen if we have an enum with a single value
    const firstNode = node.types[0];
    if (ts.isEnumMember(firstNode)) {
      return this.processor.process(context, firstNode.parent);
    }

    const unionData: IUnionData = {
      kind: 'union',
      name: buildNameFromContext(context, {}),
      options: [],
      commonProperties: [],
      isNullable: false,
    };

    const dependencies: TypeData[] = [];

    for (let i = 0; i < node.types.length; i++) {
      const unionNode = node.types[i];

      // If we're unionising with null or undefined, then our union is nullable.
      if (unionNode.kind === ts.SyntaxKind.NullKeyword || unionNode.kind === ts.SyntaxKind.UndefinedKeyword) {
        unionData.isNullable = true;
        continue;
      }

      this.logger.trace(`Processing union type '${getNodeNameSafely(unionNode)}'`);
      const result = this.processor.process({ parent: context, name: i.toString(), kind: 'union' }, unionNode);
      unionData.options.push(result.main);
      dependencies.push(...result.dependencies, result.main);
    }

    // If our union is all qualified types, then convert it into an enum
    if (!unionData.options.find(x => x.kind !== 'qualified-type')) {
      return convertUnionToEnum(context, unionData);
    }

    // When referencing unioned enums, we can end up with duplicates so we need to de-duplicate
    unionData.options = getUniqueItems(unionData.options);

    // See if there are any common properties
    unionData.commonProperties.push(...findCommonProperties(unionData.options));

    return {
      main: unionData,
      dependencies,
    };
  }
}
