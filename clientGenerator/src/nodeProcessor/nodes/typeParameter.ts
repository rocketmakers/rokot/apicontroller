import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { getNodeNameSafely } from '../utils';

export class TypeParameterDeclarationProcessor extends BaseNodeProcessor<ts.TypeParameterDeclaration> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.TypeParameter);
  }

  public process(context: IBuilderContext, declaration: ts.TypeParameterDeclaration): NodeProcessorResult {
    this.logger.trace(`Processing type parameter declaration '${getNodeNameSafely(declaration)}'`);
    if (declaration.constraint) {
      return this.processor.process(context, declaration.constraint);
    }

    const typedParameter = this.getTypeParameter(context, getNodeNameSafely(declaration));
    if (typedParameter) {
      return typedParameter;
    }

    throw new Error(`Unable to resolve type parameter '${declaration.name.text}'`);
  }
}
