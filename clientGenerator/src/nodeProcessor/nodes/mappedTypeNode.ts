import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { TypeData, IInterfaceData } from '../../types/metadata';
import { getNodeNameSafely, convertToReferencableData } from '../utils';
import { buildNameFromContext } from '../../utils';

export class MappedTypeNodeProcessor extends BaseNodeProcessor<ts.MappedTypeNode> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.MappedType);
  }

  public process(context: IBuilderContext, node: ts.MappedTypeNode): NodeProcessorResult {
    this.logger.trace(`Processing mapped type node '${getNodeNameSafely(node)}'`);
    const dependencies: TypeData[] = [];
    const type = this.processor.checker.getTypeAtLocation(node);
    const properties = type.getProperties();

    if (!node.type) {
      throw new Error('Expected type to be present on mapped type node');
    }

    const interfaceData: IInterfaceData = {
      kind: 'interface',
      baseTypes: [],
      name: buildNameFromContext(context, {}),
      properties: [],
      supportsAdditionalProperties: false,
    };

    const propertyType = this.processor.process({ parent: context, name: 'Type', kind: 'unknown' }, node.type);
    dependencies.push(...propertyType.dependencies, propertyType.main);

    for (const property of properties) {
      interfaceData.properties.push({
        isNullable: !!node.questionToken,
        name: property.name,
        type: convertToReferencableData(propertyType.main),
      });
    }

    return {
      main: interfaceData,
      dependencies,
    };
  }
}
