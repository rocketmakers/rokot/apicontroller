import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';

export class IdentifierNodeProcessor extends BaseNodeProcessor<ts.Identifier> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.Identifier);
  }

  public process(context: IBuilderContext, node: ts.Identifier): NodeProcessorResult {
    this.logger.trace(`Processing identifier '${node.text}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    // For identifiers, we'll jump through our types in case the type is defined via an import
    const symbol = this.processor.checker.getSymbolAtLocation(node);
    if (symbol) {
      const symbolDeclaration = symbol.declarations[0];
      if (ts.isImportSpecifier(symbolDeclaration)) {
        // If we're an import, then get our symbol and follow the yellow brick road to our aliased symbol
        const declarationSymbol = this.processor.checker.getSymbolAtLocation(symbolDeclaration.name);
        if (declarationSymbol) {
          const aliasedSymbol = this.processor.checker.getAliasedSymbol(declarationSymbol);
          if (aliasedSymbol) {
            return this.processor.process(context, aliasedSymbol.declarations[0]);
          }
        }
      }

      return this.processor.process(context, symbolDeclaration);
    }

    throw new Error(`Unable to process identifier '${node.text}'`);
  }
}
