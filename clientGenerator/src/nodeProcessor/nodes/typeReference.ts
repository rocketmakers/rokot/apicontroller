import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { BaseNodeProcessor } from '.';
import { NodeProcessor, NodeProcessorResult } from '..';
import { IBuilderContext } from '../../types/types';
import { getNodeNameSafely } from '../utils';

export class TypeReferenceNodeProcessor extends BaseNodeProcessor<ts.TypeReferenceNode> {
  constructor(processor: NodeProcessor, logger: Logger) {
    super(processor, logger, ts.SyntaxKind.TypeReference);
  }

  public process(context: IBuilderContext, node: ts.TypeReferenceNode): NodeProcessorResult {
    const name = getNodeNameSafely(node);

    // check to see if our reference is defined in our type parameters
    const typeParameter = this.getTypeParameter(context, name);
    if (typeParameter) {
      return typeParameter;
    }

    this.logger.trace(`Processing type reference arguments for '${name}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    const typescriptResult = this.tryProcessTypescriptUtilities(context, node.typeName, node.typeArguments);
    if (typescriptResult) {
      return typescriptResult;
    }

    const { typeParameters, dependencies } = this.resolveTypeArguments(context, node.typeArguments);

    this.logger.trace(`Processing type reference '${name}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    const typeReferenceResult = this.processor.process({ ...context, typeParameters }, node.typeName);
    dependencies.push(...typeReferenceResult.dependencies);

    return {
      main: typeReferenceResult.main,
      dependencies,
    };
  }
}
