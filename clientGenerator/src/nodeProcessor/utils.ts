import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import {
  ITypeParameterData,
  TypeData,
  ReferencableTypeData,
  IPropertyData,
  IArrayData,
  AllTypeData,
  IEnumData,
  IIntersectionData,
  ILiteralData,
  IOpaqueTypeDefinitionData,
  IOpaqueTypeImplementationData,
  IQualifiedTypeData,
  IReferenceData,
  IUnionData,
  IClassData,
  IInterfaceData,
} from '../types/metadata';
import { IBuilderContext } from '../types/types';
import { NodeProcessor, NodeProcessorResult } from '.';
import { assertNever, buildNameFromContext } from '../utils';

export function getNodeNameSafely(node: ts.Node): string {
  try {
    const nodeName = (node as any).name;
    if (nodeName) {
      return getNodeNameSafely(nodeName);
    }

    return node.getText();
  } catch {
    return 'Unknown';
  }
}

export function toPascalCase(str: string) {
  return `${str.substr(0, 1).toUpperCase()}${str.length > 1 ? str.substring(1) : ''}`;
}

export function resolveTypeParameters(
  logger: Logger,
  processor: NodeProcessor,
  context: IBuilderContext,
  params: ts.NodeArray<ts.TypeParameterDeclaration> | undefined
): {
  typeParameters: ITypeParameterData[];
  dependencies: TypeData[];
} {
  const typeParameters: ITypeParameterData[] = [];
  const dependencies: TypeData[] = [];
  const defTypeParameters = params || [];

  if (defTypeParameters.length > 0) {
    for (let i = 0; i < defTypeParameters.length; i++) {
      const param = defTypeParameters[i];

      const resolvedType = context.typeParameters && context.typeParameters[i];
      if (resolvedType) {
        // If we have a resolved type in our context with a determined type, then treat this parameter
        // as the same type as it must have been defined further up the tree.
        if (resolvedType.type) {
          typeParameters.push({
            kind: 'type-parameter',
            name: param.name.text,
            type: resolvedType.type,
          });
        } else {
          // Attempt to process our parameter as it might be an explicit type to be passed down the tree
          // (e.g. generic inheritance)
          logger.trace(`Processing type parameter '${param.name.text}'`);
          const type = processor.process(context, param);
          typeParameters.push({
            kind: 'type-parameter',
            name: param.name.text,
            type: type.main,
          });

          dependencies.push(...type.dependencies, type.main);
        }
      } else {
        throw new Error(
          `Failed to resolve type parameter '${param.name.text}' from context for '${buildNameFromContext(
            context,
            {}
          )}'`
        );
      }
    }
  }

  return { typeParameters, dependencies };
}

export function convertToReferencableData(type: TypeData): ReferencableTypeData {
  switch (type.kind) {
    case 'class':
    case 'interface':
      return {
        kind: 'reference',
        reference: {
          kind: type.kind,
          name: type.name,
        },
      };
    case 'array':
    case 'enum':
    case 'literal':
    case 'qualified-type':
    case 'union':
    case 'intersection':
    case 'opaque-type-def':
    case 'opaque-type-impl':
      return type;
    default:
      assertNever(type, 'Unexpected kind');
      throw new Error('Unexpected kind');
  }
}

export function isSameType(first: AllTypeData, second: AllTypeData): boolean {
  if (first.kind !== second.kind) {
    return false;
  }

  switch (first.kind) {
    case 'array':
      return isSameType(first.type, (second as IArrayData).type);
    case 'enum':
      return first.name === (second as IEnumData).name;
    case 'intersection':
      return first.name === (second as IIntersectionData).name;
    case 'literal':
      return first.name === (second as ILiteralData).name;
    case 'opaque-type-def':
      const opaqueType = second as IOpaqueTypeDefinitionData;
      return first.name === opaqueType.name && isSameType(first.type, opaqueType.type);
    case 'opaque-type-impl':
      const opaqueImpl = second as IOpaqueTypeImplementationData;
      return first.name === opaqueImpl.name && isSameType(first.type, opaqueImpl.type);
    case 'qualified-type':
      // For qualified types, we're only interested in the underlying type as the value will probably differ.
      const qualifiedType = second as IQualifiedTypeData;
      return isSameType(first.type, qualifiedType.type);
    case 'reference':
      const referenceType = second as IReferenceData;
      return (
        first.reference.name === referenceType.reference.name && first.reference.kind === referenceType.reference.kind
      );
    case 'union':
      return first.name === (second as IUnionData).name;
    case 'class':
      return first.name === (second as IClassData).name;
    case 'interface':
      return first.name === (second as IInterfaceData).name;
    default:
      assertNever(first, 'Unexpected kind');
      return false;
  }
}

export function findCommonProperties(data: TypeData[]): IPropertyData[] {
  const commonProperties: IPropertyData[] = [];

  // This will only work with classes and interfaces
  if (!data.find(x => x.kind !== 'class' && x.kind !== 'interface')) {
    // We just need to check the first type, as if there are no properties in there then
    // we know we have no common properties.
    const targetType = data[0];
    if (targetType.kind === 'class' || targetType.kind === 'interface') {
      for (const property of targetType.properties) {
        let isPropertyPresent = true;
        for (const otherOption of data) {
          if (otherOption.kind === 'class' || otherOption.kind === 'interface') {
            const foundProperty = otherOption.properties.find(
              x => x.name === property.name && isSameType(x.type, property.type)
            );
            if (!foundProperty) {
              isPropertyPresent = false;
              break;
            }
          } else {
            throw new Error('Union options should only contain classes and interfaces');
          }
        }

        if (isPropertyPresent) {
          commonProperties.push(property);
        }
      }
    } else {
      throw new Error('Union options should only contain classes and interfaces');
    }
  }

  return commonProperties;
}

export function getAllProperties(target: TypeData, knownTypes: TypeData[]): IPropertyData[] {
  const properties: IPropertyData[] = [];
  switch (target.kind) {
    case 'class':
    case 'interface':
      properties.push(...target.properties);

      for (const baseType of target.baseTypes) {
        if (baseType.kind === 'reference') {
          const reference = knownTypes.find(
            x => x.kind === baseType.reference.kind && x.name === baseType.reference.name
          );
          if (reference) {
            properties.push(...getAllProperties(reference, knownTypes));
          } else {
            throw new Error(
              `Failed to find defintion of reference '${baseType.reference.name}' of kind '${baseType.reference.kind}'`
            );
          }
        } else {
          properties.push(...getAllProperties(baseType, knownTypes));
        }
      }

      break;
    case 'opaque-type-def':
    case 'opaque-type-impl':
    case 'enum':
    case 'literal':
    case 'qualified-type':
    case 'array':
      // Have no properties to extract, so ignore
      break;
    case 'union':
    case 'intersection':
      for (const option of target.options) {
        properties.push(...getAllProperties(option, knownTypes));
      }

      break;
    default:
      assertNever(target, `Unexpected type: '${target}'`);
      break;
  }

  // Filter out any properties that may exist on multiple types.
  const uniqueProperties: IPropertyData[] = [];
  for (const property of properties) {
    if (!uniqueProperties.find(x => x.name === property.name && x.type.kind === property.type.kind)) {
      uniqueProperties.push(property);
    }
  }

  return uniqueProperties;
}

export function getName(data: AllTypeData): string | undefined {
  switch (data.kind) {
    case 'array':
      return `ArrayOf_${getName(data.type)}`;
    case 'reference':
      return data.reference.name;
    case 'enum':
    case 'intersection':
    case 'literal':
    case 'opaque-type-def':
    case 'opaque-type-impl':
    case 'qualified-type':
    case 'class':
    case 'union':
    case 'interface':
      return data.name;
    default:
      assertNever(data, 'Unexpected kind');
      return undefined;
  }
}

export function convertUnionToEnum(context: IBuilderContext, unionData: IUnionData): NodeProcessorResult {
  const enumData: IEnumData = {
    kind: 'enum',
    name: buildNameFromContext(context, {}),
    values: [],
  };

  for (const option of unionData.options) {
    if (option.kind !== 'qualified-type') {
      throw new Error('Should have only found qualified types');
    }

    switch (option.type.kind) {
      case 'literal':
        enumData.values.push({
          key: option.name,
          value: option.value,
        });
        break;
      case 'enum':
        // Our qualified type will reference the enum member, where we just want the underlying value
        const enumValue = option.type.values.find(x => x.key === option.value);
        if (!enumValue) {
          throw new Error(`Failed to find value '${option.value}' within enum '${option.type.name}'`);
        }

        enumData.values.push({
          key: enumValue.value,
          value: enumValue.value,
        });
        break;
      default:
        assertNever(option.type, 'Unexpected qualified-type kind');
        break;
    }
  }

  return {
    main: enumData,
    dependencies: [],
  };
}

export function getUniqueItems(items: TypeData[]) {
  const uniqueItems: TypeData[] = [];
  for (const item of items) {
    const existingItem = uniqueItems.find(i => isSameType(i, item));
    if (!existingItem) {
      uniqueItems.push(item);
    }
  }

  return uniqueItems;
}
