import * as ts from 'typescript';
import { Logger } from '@rocketmakers/log';
import { TypeData } from '../types/metadata';
import { IBuilderContext } from '../types/types';
import { ITypeOverride, defaultTypeOverrides } from '../types/typeOverride';
import { INodeProcessor } from './nodes';
import { IntersectionNodeProcessor } from './nodes/intersection';
import { EnumDeclarationProcessor } from './nodes/enum';
import { EnumMemberProcessor } from './nodes/enumMember';
import { ClassDeclarationProcessor } from './nodes/class';
import { TypeParameterDeclarationProcessor } from './nodes/typeParameter';
import { InterfaceDeclarationProcessor } from './nodes/interface';
import { IdentifierNodeProcessor } from './nodes/identifier';
import { TypeAliasDeclarationProcessor } from './nodes/typeAlias';
import { IndexedAccessTypeNodeProcessor } from './nodes/indexAccessType';
import { UnionNodeProcessor } from './nodes/union';
import { getNodeNameSafely, isSameType } from './utils';
import { TypeReferenceNodeProcessor } from './nodes/typeReference';
import { QualifiedNameProcessor } from './nodes/qualifiedName';
import { LiteralTypeNodeProcessor } from './nodes/literalType';
import { ArrayNodeProcessor } from './nodes/array';
import { ExpressionWithTypeArgumentsNodeProcessor } from './nodes/expressionWithTypeArguments';
import { TypeLiteralNodeProcessor } from './nodes/typeLiteral';
import { MappedTypeNodeProcessor } from './nodes/mappedTypeNode';
import { TypeOperatorProcessor } from './nodes/typeOperator';

export type NodeProcessorResult = { main: TypeData; dependencies: TypeData[] };

export class NodeProcessor {
  public discoveredTypes: TypeData[] = [];

  private nodeProcessors: INodeProcessor[] = [];

  constructor(private logger: Logger, public checker: ts.TypeChecker, private typeOverrides: ITypeOverride[] = []) {
    this.typeOverrides.push(...defaultTypeOverrides);

    this.nodeProcessors.push(
      new ArrayNodeProcessor(this, this.logger),
      new ClassDeclarationProcessor(this, this.logger),
      new EnumDeclarationProcessor(this, this.logger),
      new EnumMemberProcessor(this, this.logger),
      new ExpressionWithTypeArgumentsNodeProcessor(this, this.logger),
      new IdentifierNodeProcessor(this, this.logger),
      new IndexedAccessTypeNodeProcessor(this, this.logger),
      new InterfaceDeclarationProcessor(this, this.logger),
      new IntersectionNodeProcessor(this, this.logger),
      new LiteralTypeNodeProcessor(this, this.logger),
      new MappedTypeNodeProcessor(this, this.logger),
      new QualifiedNameProcessor(this, this.logger),
      new TypeAliasDeclarationProcessor(this, this.logger),
      new TypeLiteralNodeProcessor(this, this.logger),
      new TypeParameterDeclarationProcessor(this, this.logger),
      new TypeReferenceNodeProcessor(this, this.logger),
      new UnionNodeProcessor(this, this.logger),
      new TypeOperatorProcessor(this, this.logger)
    );
  }

  private processNode(context: IBuilderContext, node: ts.Node): NodeProcessorResult {
    const name = getNodeNameSafely(node);
    this.logger.trace(`Processing node '${name}'`);
    this.logger.trace(`Type Parameters: ${JSON.stringify(context.typeParameters)}`);

    // Check to see if we have any overrides
    const override = this.typeOverrides.find(x => x.name === name);
    if (override) {
      this.logger.trace('Using override');
      return { main: override.type, dependencies: [] };
    }

    for (const processor of this.nodeProcessors) {
      if (processor.canProcess(node)) {
        return processor.process(context, node);
      }
    }

    // Handle literals
    if (node.kind === ts.SyntaxKind.StringKeyword) {
      return { main: { kind: 'literal', name: 'string' }, dependencies: [] };
    }

    if (ts.isStringLiteral(node)) {
      return {
        main: {
          kind: 'qualified-type',
          name: node.text,
          type: {
            kind: 'literal',
            name: 'string',
          },
          value: node.text,
        },
        dependencies: [],
      };
    }

    if (node.kind === ts.SyntaxKind.NumberKeyword) {
      return { main: { kind: 'literal', name: 'number' }, dependencies: [] };
    }

    if (ts.isBigIntLiteral(node) || ts.isNumericLiteral(node)) {
      return {
        main: {
          kind: 'qualified-type',
          name: node.text,
          type: {
            kind: 'literal',
            name: 'number',
          },
          value: node.text,
        },
        dependencies: [],
      };
    }

    if (node.kind === ts.SyntaxKind.BooleanKeyword) {
      return { main: { kind: 'literal', name: 'boolean' }, dependencies: [] };
    }

    if (node.kind === ts.SyntaxKind.VoidKeyword) {
      return { main: { kind: 'literal', name: 'void' }, dependencies: [] };
    }

    if (node.kind === ts.SyntaxKind.AnyKeyword) {
      return { main: { kind: 'literal', name: 'any' }, dependencies: [] };
    }

    if (node.kind === ts.SyntaxKind.UndefinedKeyword) {
      return { main: { kind: 'literal', name: 'undefined' }, dependencies: [] };
    }

    throw new Error(`Unexpected node kind: ${node.kind}`);
  }

  public process(context: IBuilderContext, node: ts.Node): NodeProcessorResult {
    const result = this.processNode(context, node);
    if (!this.discoveredTypes.find(x => isSameType(x, result.main))) {
      this.discoveredTypes.push(result.main);
    }

    for (const dependency of result.dependencies) {
      if (!this.discoveredTypes.find(x => isSameType(x, dependency))) {
        this.discoveredTypes.push(dependency);
      }
    }

    return result;
  }
}
