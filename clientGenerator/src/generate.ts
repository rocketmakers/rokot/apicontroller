import * as ts from 'typescript';
import * as fs from 'fs';
import { Logger } from '@rocketmakers/shell-commands/lib/logger';
import { FileSystem } from '@rocketmakers/shell-commands/lib/fs';
import * as path from 'path';
import { FileWriter, IClientMetadata, BaseClientInterpreter } from './interpreters';
import { OpenApiInterpreter } from './interpreters/openApi';
import { ClientGenerator } from './clientGenerator';
import { ApiReflector } from './apiReflector';
import { getDefaultCompilerOptions } from './utils/typescript';
import { TypescriptInterpreter } from './interpreters/typescript';
import { assertNever } from './utils';

export interface IOpenApiConfig {
  kind: 'openapi';
  outputPath: string;
}
export interface ITypescriptConfig {
  kind: 'typescript';
  outputPath: string;
  isMonoParams: boolean;
}

export type IInterpreterConfig = IOpenApiConfig | ITypescriptConfig;

export type InterpreterKind = IInterpreterConfig['kind'];

export interface IClientBuilderConfig {
  entryPoint: string;
  exclusionPaths: string[];
  tsConfigPath?: string;
  overridesPath?: string;
  interpreters: IInterpreterConfig[];
  metadata: IClientMetadata;
}

export async function generate(logger: Logger, config: IClientBuilderConfig) {
  // Build up our interpreters
  const interpreters: BaseClientInterpreter[] = [];
  for (const interpreterConfig of config.interpreters) {
    if (FileSystem.exists(interpreterConfig.outputPath)) {
      await new Promise(res => {
        fs.unlink(interpreterConfig.outputPath, () => res());
      });
    }

    const writer = new FileWriter(interpreterConfig.outputPath);
    switch (interpreterConfig.kind) {
      case 'openapi':
        interpreters.push(new OpenApiInterpreter(writer, config.metadata));
        break;
      case 'typescript':
        interpreters.push(new TypescriptInterpreter(writer, config.metadata, interpreterConfig.isMonoParams));
        break;
      default:
        assertNever(interpreterConfig, 'Unexpected interpreter kind');
        break;
    }
  }

  let tsOptions: ts.CompilerOptions = getDefaultCompilerOptions();
  if (config.tsConfigPath) {
    const tsConfig = ts.readConfigFile(config.tsConfigPath, p => {
      return FileSystem.readFile(p);
    });

    if (!tsConfig.config.compilerOptions) {
      throw new Error("'compilerOptions' not in provided tsConfig");
    } else {
      tsOptions = ts.convertCompilerOptionsFromJson(
        tsConfig.config.compilerOptions,
        path.dirname(config.tsConfigPath),
        path.basename(config.tsConfigPath)
      ).options;
    }
  }

  const overrides: [] = [];
  if (config.overridesPath && (await FileSystem.exists(config.overridesPath))) {
    const overridesJson = JSON.parse((await FileSystem.readFileAsync(config.overridesPath)).toString());
    if (overridesJson.overrides && overridesJson.overrides.length > 0) {
      overrides.push(...(overridesJson.overrides as []));
    }
  }

  logger.info('Gathering controller endpoints...');
  const reflector = new ApiReflector(logger, [config.entryPoint], tsOptions, config.exclusionPaths);
  const reflectorResult = reflector.reflect();

  logger.info('Building client...');
  const builder = new ClientGenerator(logger, reflector.checker, interpreters, overrides);
  await builder.process(reflectorResult);
}
