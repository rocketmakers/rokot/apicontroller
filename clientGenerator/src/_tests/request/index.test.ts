// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
import * as path from 'path';
import { testClientGeneratorScenario } from '..';
import { InterpreterData } from '../../interpreters';
import { IControllerData } from '../../types/metadata';

describe('Request', () => {
  testClientGeneratorScenario(
    'When a controller has not route specified, then none is added to the child routes',
    path.join(__dirname, 'controllerWithoutRoute.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(3);

      const expectedController: Partial<IControllerData> = {
        kind: 'controller',
        name: 'Test',
        root: ''
      };
      chai.expect(data[2], 'Expected controller to be present').to.deep.include(expectedController);
    });
});
