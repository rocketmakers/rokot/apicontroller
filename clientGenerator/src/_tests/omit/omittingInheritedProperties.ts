/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
interface IBaseInterface {
  baseProperty: string;
}

interface IDerivedInterface extends IBaseInterface {
  derivedProperty: string;
}

type OmittedDerivedInterface = Omit<IDerivedInterface, 'baseProperty'>;

class BaseClass {
  baseProperty: string;
}

class DerivedClass extends BaseClass {
  derivedProperty: string;
}

type OmittedDerivedClass = Omit<DerivedClass, 'baseProperty'>;

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getOmittedInterface(req: IGetRequest<OmittedDerivedInterface, void, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('all')
  @api.verbs('get')
  public async getInlineOmittedInterface(req: IGetRequest<{ body: Omit<IDerivedInterface, 'baseProperty'> }, void, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('all')
  @api.verbs('get')
  public async getOmittedClass(req: IGetRequest<OmittedDerivedClass, void, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('all')
  @api.verbs('get')
  public async getInlineOmittedClass(req: IGetRequest<{ body: Omit<OmittedDerivedClass, 'baseProperty'> }, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
