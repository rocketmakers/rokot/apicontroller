/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
interface IBaseInterface {
  propertyOne: string;
  propertyTwo: string;
}

interface IUpdatedOn {
  updatedOn: string;
}

interface IIdentifiable<TId> {
  id: TId;
}

type PartialUpdateOf<TCreate, TId> = Partial<TCreate> & IUpdatedOn & IIdentifiable<TId>;

interface IPartialIntersection
  extends PartialUpdateOf<Pick<IBaseInterface, 'propertyOne'>, string> {}


@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async get(req: IGetRequest<{ body: Omit<IPartialIntersection, 'id'> }, void, void>): Promise<void> {
    return req.sendOk([]);
}
