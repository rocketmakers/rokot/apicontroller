// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
import * as path from 'path';
import { testClientGeneratorScenario } from '..';
import { InterpreterData } from '../../interpreters';
import { IControllerData, IInterfaceData } from '../../types/metadata';

describe('omit', () => {
  testClientGeneratorScenario(
    'When an omitted class is extended from, then an omitted version of the class is created',
    path.join(__dirname, 'extendOmit.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(10);

      // We're intentionally only checking the types we care about
      const expectedOmitClassOne: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IInterfaceOneOmitKind',
        properties: [
          {
            name: 'interfaceOneProperty',
            isNullable: false,
            type: {
              kind: 'literal',
              name: 'string',
            },
          },
        ],
      };
      chai.expect(data[4], 'Expected literal to be present').to.deep.include(expectedOmitClassOne);

      const expectedOmitClassTwo: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IInterfaceTwoOmitKind',
        properties: [
          {
            name: 'interfaceTwoProperty',
            isNullable: false,
            type: {
              kind: 'literal',
              name: 'string',
            },
          },
        ],
      };
      chai.expect(data[6], 'Expected literal to be present').to.deep.include(expectedOmitClassTwo);

      const expectedClassThree: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IInterfaceThree',
        baseTypes: [
          {
            kind: 'reference',
            reference: {
              kind: 'interface',
              name: 'IInterfaceOneOmitKind',
            },
          },
          {
            kind: 'reference',
            reference: {
              kind: 'interface',
              name: 'IInterfaceTwoOmitKind',
            },
          },
        ],
      };
      chai.expect(data[7], 'Expected literal to be present').to.deep.include(expectedClassThree);

      const expectedController: Partial<IControllerData> = {
        kind: 'controller',
        name: 'Test',
        root: 'test',
      };
      chai.expect(data[9], 'Expected controller to be present').to.deep.include(expectedController);
    });

  testClientGeneratorScenario(
    'When an omit is used to omit a property on a base class several times, then the property is omitted',
    path.join(__dirname, 'omittingInheritedProperties.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(11);

      // We're intentionally only checking the types we care about
      const expectedOmitInterface: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'OmittedDerivedInterface',
        properties: [
          {
            name: 'derivedProperty',
            isNullable: false,
            type: {
              kind: 'literal',
              name: 'string',
            },
          },
        ],
      };
      chai.expect(data[2], 'Expected literal to be present').to.deep.include(expectedOmitInterface);

      const expectedInlineOmitInterface: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IDerivedInterfaceOmitBaseProperty',
        properties: [
          {
            name: 'derivedProperty',
            isNullable: false,
            type: {
              kind: 'literal',
              name: 'string',
            },
          },
        ],
      };
      chai.expect(data[4], 'Expected literal to be present').to.deep.include(expectedInlineOmitInterface);

      const expectedOmitClass: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'OmittedDerivedClass',
        properties: [
          {
            name: 'derivedProperty',
            isNullable: false,
            type: {
              kind: 'literal',
              name: 'string',
            },
          },
        ],
      };
      chai.expect(data[7], 'Expected literal to be present').to.deep.include(expectedOmitClass);

      const expectedInlineOmitClass: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'OmittedDerivedClassOmitBaseProperty',
        properties: [
          {
            name: 'derivedProperty',
            isNullable: false,
            type: {
              kind: 'literal',
              name: 'string',
            },
          },
        ],
      };
      chai.expect(data[8], 'Expected literal to be present').to.deep.include(expectedInlineOmitClass);
    });

    testClientGeneratorScenario(
      'When an omit is used to omit a property on an intersection, then the property is omitted',
      path.join(__dirname, 'omittingInheritedIntersectionProperty.ts'),
      (data: InterpreterData[]) => {
        chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(10);

        const actualPartialInterfaceIntersection = data.find(x => x.kind === 'interface' && x.name === 'IPartialIntersectionOmitId');
        const expectedPartialInterfaceIntersection: Partial<IInterfaceData> = {
          kind: 'interface',
          name: 'IPartialIntersectionOmitId',
          properties: [
            {
              name: 'propertyOne',
              isNullable: true,
              type: {
                kind: 'literal',
                name: 'string',
              },
            },
            {
              name: 'updatedOn',
              isNullable: false,
              type: {
                kind: 'literal',
                name: 'string',
              },
            },
          ],
        };

        chai.expect(actualPartialInterfaceIntersection, 'Expected partial interface to be present').to.deep.include(expectedPartialInterfaceIntersection);
      });
});
