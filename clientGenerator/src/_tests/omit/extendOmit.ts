/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
enum TestEnum {
  One = 'TestOne',
  Two = 'TestTwo',
  Three = 'TestThree',
}

interface IInterfaceOne {
  kind: TestEnum.One;
  interfaceOneProperty: string;
}

interface IInterfaceTwo {
  kind: TestEnum.Two;
  interfaceTwoProperty: string;
}

interface IInterfaceThree extends Omit<IInterfaceOne, 'kind'>, Omit<IInterfaceTwo, 'kind'> {
  kind: TestEnum.Three;
  interfaceThreeProperty: string;
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IInterfaceThree, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
