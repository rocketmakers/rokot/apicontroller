/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
export interface IGeneric<TKind extends string> {
  kind: TKind
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IGeneric<'first@second'>, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
