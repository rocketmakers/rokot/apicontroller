/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
export enum TestEnum {
  One = 'TestOne',
  Two = 'TestTwo',
}

export interface IGeneric<TKind extends TestEnum> {
  kind: TKind
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IGeneric<TestEnum.One>, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
