// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
import * as path from 'path';
import { testClientGeneratorScenario } from '..';
import { InterpreterData } from '../../interpreters';
import { IEnumData, IInterfaceData } from '../../types/metadata';

describe('Generics', () => {
  testClientGeneratorScenario(
    'When a generic is resolved with an enum member, then the class name is safe',
    path.join(__dirname, 'genericWithEnumMember.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(5);

      const expectedEnum: Partial<IEnumData> = {
        kind: 'enum',
        name: 'TestEnum',
        values: [
          {
            key: 'One',
            value: 'TestOne',
          },
          {
            key: 'Two',
            value: 'TestTwo',
          },
        ],
      };
      chai.expect(data[0], 'Expected enum to be present').to.deep.include(expectedEnum);

      const expectedInterface: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IGeneric_TestEnum_One',
        baseTypes: [],
        properties: [
          {
            name: 'kind',
            type: {
              kind: 'qualified-type',
              name: 'TestEnum.One',
              type: data[0] as IEnumData,
              value: 'One',
            },
            isNullable: false,
          },
        ],
      };
      chai.expect(data[2], 'Expected interface to be present').to.deep.include(expectedInterface);
    });

  testClientGeneratorScenario(
    'When a generic is resolved with an enum member, then the class name is safe',
    path.join(__dirname, 'genericWithStringLiteralInvalidChars.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(4);

      const expectedInterface: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IGeneric_first_second',
        baseTypes: [],
        properties: [
          {
            name: 'kind',
            type: {
              kind: 'qualified-type',
              name: 'first@second',
              type: {
                kind: 'literal',
                name: 'string',
              },
              value: 'first@second',
            },
            isNullable: false,
          },
        ],
      };
      chai.expect(data[1], 'Expected interface to be present').to.deep.include(expectedInterface);
    }
  );

});
