/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */

export interface IKeyOfTest<T> {
  key: keyof T;
}

export class Test {
  hello: string;
  world: string;
}

export interface IUsedKeyOfTest extends IKeyOfTest<Test> {
}

@api.controller('TestController')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IUsedKeyOfTest, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
