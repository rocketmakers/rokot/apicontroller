/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */

export interface IKeyOfTest<T> {
  key: keyof T;
}

export interface IUsedKeyOfTest extends IKeyOfTest<string> {
}

@api.controller('TestController')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IUsedKeyOfTest, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
