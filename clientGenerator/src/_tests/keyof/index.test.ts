// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
import * as path from 'path';
import { testClientGeneratorScenario } from '..';
import { InterpreterData } from '../../interpreters';
import { IEnumData, IInterfaceData } from '../../types/metadata';

describe('Keyof Property', () => {
  testClientGeneratorScenario(
    'When a property using keyof is used in a generic of type interface, then the generated type has all available keys',
    path.join(__dirname, 'genericKeyOfInterface.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(7);

      const expectedEnum: Partial<IEnumData> = {
        kind: 'enum',
        name: 'ITestPropertyKeysEnum',
        values: [{
          key: 'hello',
          value: 'hello'
        },
        {
          key: 'world',
          value: 'world'
        }]
      };
      chai.expect(data[2], 'Expected enum containing properties to be present').to.deep.include(expectedEnum);

      const expectedKeyOfObject: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IKeyOfTest_ITest',
        properties: [{
          name: 'key',
          isNullable: false,
          type: {
            kind: 'enum',
            name: expectedEnum.name || '',
            values: expectedEnum.values || []
          }
        }]
      };
      chai.expect(data[3], 'Expected object of keyof property to reference generated enum').to.deep.include(expectedKeyOfObject);
    });

  testClientGeneratorScenario(
    'When a property using keyof is used in a generic of type class, then the generated type has all available keys',
    path.join(__dirname, 'genericKeyOfClass.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(7);

      const expectedEnum: Partial<IEnumData> = {
        kind: 'enum',
        name: 'TestPropertyKeysEnum',
        values: [{
          key: 'hello',
          value: 'hello'
        },
        {
          key: 'world',
          value: 'world'
        }]
      };
      chai.expect(data[2], 'Expected enum containing properties to be present').to.deep.include(expectedEnum);

      const expectedKeyOfObject: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IKeyOfTest_Test',
        properties: [{
          name: 'key',
          isNullable: false,
          type: {
            kind: 'enum',
            name: expectedEnum.name || '',
            values: expectedEnum.values || []
          }
        }]
      };
      chai.expect(data[3], 'Expected object of keyof property to reference generated enum').to.deep.include(expectedKeyOfObject);
    });

  testClientGeneratorScenario(
    'When a property using keyof is used in a generic of value type, then the generated type is a string',
    path.join(__dirname, 'genericKeyOfValueType.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(5);

      const expectedKeyOfObject: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IKeyOfTest_string',
        properties: [{
          name: 'key',
          isNullable: false,
          type: {
            kind: 'literal',
            name: 'string',
          }
        }]
      };
      chai.expect(data[1], 'Expected object of keyof property to reference string').to.deep.include(expectedKeyOfObject);
    });

  testClientGeneratorScenario(
    'When a property using keyof is used in a generic of type other than interface or class, then the generated type has all available keys',
    path.join(__dirname, 'genericKeyOfType.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(11);

      const actualKeyOfType = data.find(x => x.kind === 'enum' && x.name === 'PartialUpdateOf_IBaseInterface_stringPropertyKeysEnum');

      const expectedKeyOfObject: Partial<IEnumData> = {
        kind: 'enum',
        name: 'PartialUpdateOf_IBaseInterface_stringPropertyKeysEnum',
        values: [{
          key: 'propertyOne',
          value: 'propertyOne',
        },
        {
          key: 'propertyTwo',
          value: 'propertyTwo',
        },
        {
          key: 'updatedOn',
          value: 'updatedOn',
        },
        {
          key: 'id',
          value: 'id',
        }]
      };
      chai.expect(actualKeyOfType, 'Expected object of keyof property to reference string').to.deep.include(expectedKeyOfObject);
    });
});
