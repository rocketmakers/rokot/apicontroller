interface IBaseInterface {
  propertyOne: string;
  propertyTwo: string;
}

interface IUpdatedOn {
  updatedOn: string;
}

interface IIdentifiable<TId> {
  id: TId;
}

type PartialUpdateOf<TCreate, TId> = Partial<TCreate> & IUpdatedOn & IIdentifiable<TId>;

interface IKeyOfTest<T> {
  key: keyof T;
}

interface IUsedKeyOfTest extends IKeyOfTest<PartialUpdateOf<IBaseInterface, string>> {
}

  @api.controller('TestController')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IUsedKeyOfTest, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}