class BaseProfile<TDateType, TProfileType> {
  profile: TProfileType;

  date: TDateType;
}

class Profile<TProfile> extends BaseProfile<number, TProfile> {}

interface IBaseParams<TDateType, TPropertyTwoType> {
  propertyOne: TPropertyTwoType;
  propertyTwo: TDateType[];
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IParams<TPropertyTwoType> extends IBaseParams<number, TPropertyTwoType> {}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<Profile<string>, IParams<string>, void>): Promise<void> {
    return req.sendOk([]);
  }
}
