enum TestEnum {
  One = 'TestOne',
  Two = 'TestTwo',
  Three = 'TestThree',
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<TestEnum, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
