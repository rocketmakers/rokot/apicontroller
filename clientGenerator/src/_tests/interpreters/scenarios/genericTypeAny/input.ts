class BaseProfile<TDateType, TProfileType> {
  profile: TProfileType;

  date: TDateType;
}

class Profile<TProfile> extends BaseProfile<string, TProfile> {}

interface IBaseParams<TDateType, TPropertyTwoType> {
  propertyOne: TPropertyTwoType;
  propertyTwo: TDateType[];
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IParams<TPropertyTwoType> extends IBaseParams<number, TPropertyTwoType> {}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll<TProfile>(req: IGetRequest<Profile<TProfile>, IParams<TProfile>, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('allProfileString')
  @api.verbs('get')
  public async getAllProfileString<TProfileString extends string>(
    req: IGetRequest<Profile<TProfileString>, IParams<TProfileString>, void>
  ): Promise<void> {
    return req.sendOk([]);
  }
}
