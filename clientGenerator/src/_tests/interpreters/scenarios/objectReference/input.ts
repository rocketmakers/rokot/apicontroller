class TestReference {
  stringTest: string;

  numberTest: number;

  boolTest: boolean;
}

class Test {
  reference: TestReference;

  references: TestReference[];
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<Test[], void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
