enum EnumTest {
  One = 'OneTest',
}

class TestTwo {
  stringTest: string;
}

class TestClass {
  stringTest: string;

  numberTest: number;

  boolTest: boolean;

  referenceTest: TestTwo;

  enumTest: EnumTest;
}

type PartialTest = Partial<TestClass>;

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<PartialTest, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
