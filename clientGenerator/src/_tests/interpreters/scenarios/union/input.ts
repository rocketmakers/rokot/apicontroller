class StringClass {
  property: string;
}

class NumberClass {
  property: number;
}

class BooleanClass {
  property: boolean;
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<StringClass | NumberClass | BooleanClass, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
