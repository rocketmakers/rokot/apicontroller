export interface IOneMetadata {
  kind: 'one';
}

export interface ITwoMetadata {
  kind: 'two';
}

export type MetadataTypes = {
  One: IOneMetadata;
  Two: ITwoMetadata;
};

export type Metadata = MetadataTypes[keyof MetadataTypes];

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<Metadata, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
