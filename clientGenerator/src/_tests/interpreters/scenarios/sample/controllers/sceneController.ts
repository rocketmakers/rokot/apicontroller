import { ProjectId } from '../data/types/opaqueTypes';
import { IScene, ICreatableScene } from '../data/types/sceneTypes';

@api.controller('SceneController', '/scenes')
export class SceneController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('')
  @api.verbs('post')
  public async create(req: IRequest<{ scene: ICreatableScene }, IScene, void, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route(':id')
  @api.verbs('get')
  public async getScenesByProjectId(req: IGetRequest<IScene[], { id: ProjectId }, void>): Promise<void> {
    return req.sendOk([]);
  }
}
