import { IProject, IProjectsQuery, ICreatableProject } from '../data/types/projectTypes';
import { SceneId, ProjectId } from '../data/types/opaqueTypes';
import { IProjectUnitResponse } from '../data/types/projectUnitTypes';

import { ISearchResponse } from '../data/types/search';

@api.controller('ProjectController', '/projects')
export class ProjectController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('')
  @api.verbs('post')
  public async create(req: IRequest<{ project: ICreatableProject }, IProject, void, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('my')
  @api.verbs('get')
  public async getMyProjects(req: IGetRequest<ISearchResponse<IProject>, void, IProjectsQuery>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route(':projectId')
  @api.verbs('get')
  public async getById(req: IGetRequest<IProject, { projectId: ProjectId }, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route(':projectId/scenes/:sceneId/units')
  @api.verbs('get')
  public async getUnitsByScene(
    req: IGetRequest<IProjectUnitResponse[], { projectId: ProjectId; sceneId: SceneId }, void>
  ): Promise<void> {
    return req.sendOk([]);
  }
}
