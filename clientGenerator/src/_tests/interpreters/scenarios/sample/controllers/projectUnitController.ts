import { ICreatableProjectUnit, IProjectUnit } from '../data/types/projectUnitTypes';

@api.controller('ProjectUnitController', '/project-units')
export class ProjectUnitController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('')
  @api.verbs('post')
  public async create(req: IRequest<ICreatableProjectUnit, IProjectUnit, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
