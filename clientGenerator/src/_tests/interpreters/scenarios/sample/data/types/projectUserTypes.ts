import { ProjectUserId, UserId, ProjectId } from './opaqueTypes';

export enum ProjectUserRoleType {
  Admin = 'Admin',
  Developer = 'Developer',
}

export interface IProjectUser {
  id: ProjectUserId;
  projectId: ProjectId;
  userId: UserId;
  role: ProjectUserRoleType;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICreatableProjectUser extends Omit<IProjectUser, 'id'> {}
