import { SceneId, ProjectId } from './opaqueTypes';

import { IUpdatedOn } from './core';

export interface IScene {
  id: SceneId;
  projectId: ProjectId;
  name: string;
  description?: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICreatableScene extends Omit<IScene, 'id'> {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IUpdatableScene extends Partial<Omit<IScene, 'projectId' | 'id'>>, IUpdatedOn {
  id: SceneId;
}
