import { DateTimeString } from './opaqueTypes';

export interface IUpdatedOn {
  updatedOn: DateTimeString;
}
