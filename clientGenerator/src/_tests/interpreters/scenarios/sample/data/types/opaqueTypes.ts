/**
 * Opaque Types
 */

export declare type OpaqueString<Key extends string> = string & {
  __opaque: Key;
};

export type ProjectId = OpaqueString<'ProjectId'>;
export type ProjectUnitId = OpaqueString<'ProjectUnitId'>;
export type ProjectUnitInstanceId = OpaqueString<'ProjectUnitInstanceId'>;
export type ProjectTagId = OpaqueString<'ProjectTagId'>;
export type ProjectUserId = OpaqueString<'ProjectUserId'>;
export type SceneId = OpaqueString<'SceneId'>;
export type TagId = OpaqueString<'TagId'>;
export type ProjectUnitTagId = OpaqueString<'ProjectUnitTagId'>;
export type TagName = OpaqueString<'TagName'>;
export type DateTimeString = OpaqueString<'DateTimeString'>;
export type AspectRatioName = OpaqueString<'AspectRatioName'>;
export type UserId = OpaqueString<'UserId'>;
