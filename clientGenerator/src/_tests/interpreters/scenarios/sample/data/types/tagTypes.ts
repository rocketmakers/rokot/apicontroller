import { TagName } from './opaqueTypes';

export interface ITaggable {
  mandatoryTags: TagName[];
  excludedTags: TagName[];
}

export enum TagKind {
  Mandatory = 'Mandatory',
  Excluded = 'Excluded',
}
