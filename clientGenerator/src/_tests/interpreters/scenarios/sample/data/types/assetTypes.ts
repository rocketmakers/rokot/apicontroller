export enum AssetKind {
  Image = 'Image',
  Video = 'Video',
  ImageOrVideo = 'ImageOrVideo',
  ThreeDimensional = 'ThreeDimensional',
  Audio = 'Audio',
}

export enum ThreeDimensionalMaterial {
  Metal = 'Metal',
}
