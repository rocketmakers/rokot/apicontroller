import { SceneId, ProjectUnitId, AspectRatioName } from './opaqueTypes';
import { IProjectUnitInstance } from './projectUnitInstanceTypes';

import { AssetKind } from './assetTypes';
import { ITaggable } from './tagTypes';

export interface IImageAssetPublisherMetadata {
  kind: AssetKind.Image;
  aspectRatio: AspectRatioName;
}

export interface IVideoAssetPublisherMetadata {
  kind: AssetKind.Video;
  aspectRatio: AspectRatioName;
  isMuted: boolean;
  maxLengthInSeconds: number;
}

export enum ImageOrVideoPreference {
  Image = 'Image',
  Video = 'Video',
}

export interface IImageOrVideoAssetPublisherMetadata
  extends Omit<IImageAssetPublisherMetadata, 'kind'>,
    Omit<IVideoAssetPublisherMetadata, 'kind'> {
  kind: AssetKind.ImageOrVideo;
  preference: ImageOrVideoPreference;
}

export interface IAudioAssetPublisherMetadata {
  kind: AssetKind.Audio;
  maxLengthInSeconds: number;
}

export interface IThreeDimensionalAssetPublisherMetadata {
  kind: AssetKind.ThreeDimensional;
  isMovable: boolean;
}

export type ProjectUnitMetadata = ProjectUnitMetadataTypes[keyof ProjectUnitMetadataTypes];

export type ProjectUnitMetadataTypes = {
  Image: IImageAssetPublisherMetadata;
  Video: IVideoAssetPublisherMetadata;
  ImageOrVideo: IImageOrVideoAssetPublisherMetadata;
  Audio: IAudioAssetPublisherMetadata;
  ThreeDimensional: IThreeDimensionalAssetPublisherMetadata;
};

export interface IProjectUnit {
  id: ProjectUnitId;
  sceneId: SceneId;
  name?: string;
  description?: string;
  metadata: ProjectUnitMetadata;
}

export interface IProjectUnitResponse extends IProjectUnit, ITaggable {
  instances: IProjectUnitInstance[];
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICreatableProjectUnit extends Omit<IProjectUnit, 'id'>, ITaggable {}
