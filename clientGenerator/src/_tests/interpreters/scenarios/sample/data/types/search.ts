export interface ISearchResponsePaging {
  current: string;
  previous?: string;
  next?: string;
}
export interface ISearchResponse<T> {
  paging?: ISearchResponsePaging;
  items: T[];
  itemCount: number;
}
export interface IPagination {
  page?: string;
  pageSize?: number;
}
export declare enum QueryOperator {
  between = 'between',
  equals = 'equals',
  like = 'like',
  lessThanOrEqual = 'lessThanOrEqual',
  greaterThanOrEqual = 'greaterThanOrEqual',
}
export interface IQueryWhere {
  key: string;
  operator: QueryOperator;
  valueOne: any;
  valueTwo?: any;
}
export interface IQuery {
  where: IQueryWhere[];
}
