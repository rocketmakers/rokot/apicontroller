import { ProjectUnitInstanceId, ProjectUnitId } from './opaqueTypes';

export interface IProjectUnitInstance {
  id: ProjectUnitInstanceId;
  projectUnitId: ProjectUnitId;
}
