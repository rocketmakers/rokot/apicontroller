import { ProjectUnitTagId, ProjectUnitId, TagName } from './opaqueTypes';

import { TagKind } from './tagTypes';

export enum InclusionType {
  Mandatory = 'Mandatory',
  Excluded = 'Excluded',
}

export interface IProjectUnitTag {
  id: ProjectUnitTagId;
  projectUnitId: ProjectUnitId;
  tag: TagName;
  kind: TagKind;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICreatableProjectUnitTag extends Omit<IProjectUnitTag, 'id'> {}
