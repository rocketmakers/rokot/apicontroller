import { ProjectTagId, ProjectId, TagName } from './opaqueTypes';

import { TagKind } from './tagTypes';

export interface IProjectTag {
  id: ProjectTagId;
  projectId: ProjectId;
  tag: TagName;
  kind: TagKind;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICreatableProjectTag extends Omit<IProjectTag, 'id'> {}
