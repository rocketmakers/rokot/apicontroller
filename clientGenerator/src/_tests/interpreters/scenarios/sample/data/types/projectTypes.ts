import { ProjectId } from './opaqueTypes';
import { IPagination, IQuery } from './search';
import { IUpdatedOn } from './core';

export interface IProject {
  id: ProjectId;
  name: string;
  description?: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ICreatableProject extends Omit<IProject, 'id'> {}

export interface IUpdatableProject extends Partial<ICreatableProject>, IUpdatedOn {
  id: ProjectId;
}

export interface IProjectsQuery extends IPagination, IQuery {}
