class StringClass {
  property: string;
}

@api.controller('TestController')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('delete')
  @api.verbs('delete')
  public async delete(req: IRequest<void, void, void, StringClass>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('get')
  @api.verbs('get')
  public async get(req: IGetRequest<void, void, StringClass>): Promise<void> {
    return req.sendOk([]);
  }
}
