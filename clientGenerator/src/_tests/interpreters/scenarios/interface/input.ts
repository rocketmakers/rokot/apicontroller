interface ITest {
  stringTest: string;
  numberTest: number;
  boolTest: boolean;
  stringOptional?: string;
  numberOptional?: number;
  boolOptional?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IEmptyTest {}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<ITest[], void, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('getEmpty')
  @api.verbs('get')
  public async getEmpty(req: IGetRequest<IEmptyTest, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
