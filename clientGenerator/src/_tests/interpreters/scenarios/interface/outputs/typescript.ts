/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
// This document is generated by rokot-apicontroller-client-generator. DO NOT edit directly.

export interface HeaderMap {
    [k: string]: string;
}

export interface IFetcher {
    request<T>(url: string, verb: string, contentType: string, body?: any, headers?: HeaderMap): Promise<T>;
    buildQueryString(query?: any): string;
    requestValidate?(routeUniqueName: string, paramValues: {
        body?: any;
        params?: any;
        query?: any;
    }): Promise<void>;
}

export interface ITest {
    stringTest: string;
    numberTest: number;
    boolTest: boolean;
    stringOptional?: string;
    numberOptional?: number;
    boolOptional?: boolean;
}

export interface IEmptyTest {
}

export class TestController {
    constructor(private fetcher: IFetcher) { }
    getAll = (headers?: HeaderMap): Promise<ITest[]> => {
        return this.fetcher.request<ITest[]>(`test/all`, "get", "application/json", undefined, headers);
    };
    getEmpty = (headers?: HeaderMap): Promise<IEmptyTest> => {
        return this.fetcher.request<IEmptyTest>(`test/getEmpty`, "get", "application/json", undefined, headers);
    };
}

export class ApiClient {
    constructor(private fetcher: IFetcher) { }
    test = new TestController(this.fetcher);
}