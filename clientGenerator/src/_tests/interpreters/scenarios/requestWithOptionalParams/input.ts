class ParamsClass {
  id?: string;
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('get/:id?/')
  @api.verbs('get')
  public async get(req: IGetRequest<void, ParamsClass, void>): Promise<void> {
    return req.sendOk([]);
  }
}
