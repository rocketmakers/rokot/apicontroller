interface IBaseInterface {
  baseInterfaceProperty: string;
}

interface IDerivedInterface extends IBaseInterface {
  id: number;
  derivedInterfaceProperty: string;
}

class BaseClass {
  baseClassProperty: string;
}

class DerivedClass extends BaseClass {
  id: string;
  derivedClassProperty: string;
}

type UnionType = IDerivedInterface | DerivedClass;

type IntersectionType = IDerivedInterface & DerivedClass;

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  // Test parameters with classes

  @api.route('class/:baseClassProperty/:derivedClassProperty')
  @api.verbs('post')
  public async postClass(req: IRequest<void, void, DerivedClass, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('class/:baseClassProperty/:derivedClassProperty')
  @api.verbs('get')
  public async getClass(req: IGetRequest<void, DerivedClass, void>): Promise<void> {
    return req.sendOk([]);
  }

  // Test parameters with interfaces

  @api.route('interface/:baseInterfaceProperty/:derivedInterfaceProperty')
  @api.verbs('post')
  public async postInterface(req: IRequest<void, void, IDerivedInterface, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('interface/:baseInterfaceProperty/:derivedInterfaceProperty')
  @api.verbs('get')
  public async getInterface(req: IGetRequest<void, IDerivedInterface, void>): Promise<void> {
    return req.sendOk([]);
  }

  // Test parameters with unions

  @api.route('union/:id')
  @api.verbs('post')
  public async postUnion(req: IRequest<void, void, UnionType, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('union/:id')
  @api.verbs('get')
  public async getUnion(req: IGetRequest<void, UnionType, void>): Promise<void> {
    return req.sendOk([]);
  }

  // Test parameters with intersections

  @api.route('intersection/:baseInterfaceProperty/:derivedInterfaceProperty/:baseClassProperty/:derivedClassProperty')
  @api.verbs('post')
  public async postIntersection(req: IRequest<void, void, IntersectionType, void>): Promise<void> {
    return req.sendOk([]);
  }

  // Test that we can have content after our parameters
  @api.route('intersection/:baseInterfaceProperty/:derivedInterfaceProperty/:baseClassProperty/:derivedClassProperty/test')
  @api.verbs('get')
  public async getIntersection(req: IGetRequest<void, IntersectionType, void>): Promise<void> {
    return req.sendOk([]);
  }
}
