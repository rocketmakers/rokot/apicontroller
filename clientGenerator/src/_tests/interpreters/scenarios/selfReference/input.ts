export interface IUserData {
  /** The user id */
  id: string;
  /** The original user if impersonating */
  impersonator?: IUser;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IUser extends IUserData {}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IUser, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
