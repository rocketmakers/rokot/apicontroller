interface IBaseInterface {
  baseInterfaceProperty: string;
}

interface IDerivedInterface extends IBaseInterface {
  derivedInterfaceProperty: string;
}

class BaseClass {
  baseClassProperty: string;
}

class DerivedClass extends BaseClass {
  derivedClassProperty: string;
}

type UnionType = IDerivedInterface | DerivedClass;

type IntersectionType = IDerivedInterface & DerivedClass;

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  // Test parameters with classes

  @api.route('class')
  @api.verbs('post')
  public async postClass(req: IRequest<void, void, void, DerivedClass>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('class')
  @api.verbs('get')
  public async getClass(req: IGetRequest<void, void, DerivedClass>): Promise<void> {
    return req.sendOk([]);
  }

  // Test parameters with interfaces

  @api.route('interface')
  @api.verbs('post')
  public async postInterface(req: IRequest<void, void, void, IDerivedInterface>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('interface')
  @api.verbs('get')
  public async getInterface(req: IGetRequest<void, void, IDerivedInterface>): Promise<void> {
    return req.sendOk([]);
  }

  // Test parameters with unions

  @api.route('union')
  @api.verbs('post')
  public async postUnion(req: IRequest<void, void, void, UnionType>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('union')
  @api.verbs('get')
  public async getUnion(req: IGetRequest<void, void, UnionType>): Promise<void> {
    return req.sendOk([]);
  }

  // Test parameters with intersections

  @api.route('intersection')
  @api.verbs('post')
  public async postIntersection(req: IRequest<void, void, void, IntersectionType>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('intersection')
  @api.verbs('get')
  public async getIntersection(req: IGetRequest<void, void, IntersectionType>): Promise<void> {
    return req.sendOk([]);
  }
}
