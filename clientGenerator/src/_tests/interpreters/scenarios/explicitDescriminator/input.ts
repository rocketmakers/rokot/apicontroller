class ClassOne {
  kind: 'one';

  property: string;
}

class ClassTwo {
  kind: 'two';

  property: number;
}

class ClassThree {
  kind: 'three';

  property: boolean;
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<ClassOne | ClassTwo | ClassThree, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
