class Test {
  stringTest: string;

  numberTest: number;

  boolTest: boolean;
}

type OmitPartialPickedTest = Omit<Partial<Pick<Test, 'stringTest' | 'numberTest'>>, 'stringTest'>;

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<OmitPartialPickedTest, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
