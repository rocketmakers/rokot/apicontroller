import { TestEnum } from './imports';

class ClassOne {
  kind: TestEnum.One;

  property: string;
}

class ClassTwo {
  kind: TestEnum.Two;

  property: number;
}

class ClassThree {
  kind: TestEnum.Three;

  property: boolean;
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<ClassOne | ClassTwo | ClassThree, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
