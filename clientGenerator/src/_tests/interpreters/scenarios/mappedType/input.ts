export interface IAuthUserMinimal {
  username: string;
  forename: string;
}

export type ISearch = { [P in keyof Omit<IAuthUserMinimal, 'id' | 'createdBy'>]?: boolean };

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<void, void, ISearch>): Promise<void> {
    return req.sendOk([]);
  }
}
