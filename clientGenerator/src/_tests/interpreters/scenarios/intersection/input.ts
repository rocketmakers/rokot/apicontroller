import { IntersectionExample } from './intersection';

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IntersectionExample, void, void>): Promise<void> {
    return req.sendOk([]);
  }

  @api.route('intersectionQuery')
  @api.verbs('get')
  public async intersectionQuery(req: IGetRequest<void, void, IntersectionExample>): Promise<void> {
    return req.sendOk([]);
  }
}
