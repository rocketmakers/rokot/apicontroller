export interface ITestClass {
  stringProperty: string;
}

export interface IIdentifiable<TId> {
  id: TId;
}
