import { IIdentifiable, ITestClass } from './class';

export type IntersectionExample = ITestClass & IIdentifiable<string>;
