export type OpaqueString<Key extends string> = string & { __opaque: Key };
export type OpaqueNumber<Key extends string> = number & { __opaque: Key };

export type DateString = OpaqueString<'Date'>;
export type DateTimeString = OpaqueString<'DateTime'>;
export type IdString = OpaqueString<'Id'>;
export type IdNumber = OpaqueNumber<'Id'>;

export interface IExampleOpaqueTypes {
  date: DateString;
  dateTime: DateTimeString;
  idString: IdString;
  idNumber: IdNumber;
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() { }

  @api.route('get/:id')
  @api.verbs('get')
  public async get(
    req: IGetRequest<IExampleOpaqueTypes, { id: IdString }, void>
  ): Promise<void> {
    return req.sendOk([]);
  }
}
