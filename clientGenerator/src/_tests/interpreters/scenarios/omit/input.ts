class Test {
  stringTest: string;

  numberTest: number;

  boolTest: boolean;
}

type OmitTest = Omit<Test, 'stringTest' | 'numberTest'>;

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<OmitTest, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
