class StringClass {
  property: string;
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('post')
  public async getAll(req: IRequest<StringClass, void, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
