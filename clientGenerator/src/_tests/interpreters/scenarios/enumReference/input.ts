import { ClassOne } from './class';

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<ClassOne, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
