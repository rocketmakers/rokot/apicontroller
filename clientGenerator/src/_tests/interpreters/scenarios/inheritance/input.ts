interface IUpdatedOn {
  updatedOn: string;
}

interface IUpdatable extends IUpdatedOn {
  updatedBy: string;
}

class BaseClass {
  kind: string;
}

class Profile extends BaseClass implements IUpdatable {
  profile: any;
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<Profile, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
