// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
// eslint-disable-next-line import/no-extraneous-dependencies
import * as TypeMoq from 'typemoq';
import { FileSystem } from '@rocketmakers/shell-commands/lib/fs';
import * as path from 'path';
import { Logger } from '@rocketmakers/log';
import { ApiReflector } from '../../apiReflector';
import { ClientGenerator } from '../../clientGenerator';
import { OpenApiInterpreter } from '../../interpreters/openApi';
import { BaseClientInterpreter, IWriter, IClientMetadata } from '../../interpreters';
import { getDefaultCompilerOptions } from '../../utils/typescript';
import { TypescriptInterpreter } from '../../interpreters/typescript';

const interpreters = ['openapi', 'typescript'];
// const interpreters = ['typescript'];
// const interpreters = ['openapi'];

// Used to debug tests
const outputTrace = false;
const outputContent = false;

describe('ClientBuilder', () => {
  for (const interpreterName of interpreters) {
    describe(interpreterName, () => {
      const cases = FileSystem.getFolders(path.join(__dirname, 'scenarios'));
      for (const testCase of cases) {
        const inputPath = path.join(testCase.path, 'input.ts');
        if (FileSystem.exists(inputPath) === false) {
          continue;
        }

        // target specific case
        // if (testCase.path.endsWith('opaqueTypes') === false) {
        //   continue;
        // }

        const expectedOutputs = FileSystem.getFiles(path.join(testCase.path, 'outputs'));
        if (!expectedOutputs) {
          throw new Error(`Failed to rerieve outputs for '${testCase.path}'`);
        }

        const expectedOutputFile = expectedOutputs.find(
          x => x.name.substring(0, x.name.lastIndexOf('.')).toLowerCase() === interpreterName
        );

        if (expectedOutputFile) {
          it(`When '${testCase.name}' is provided, then expected output should be generated`, async () => {
            // Arrange
            const mockedLogger = TypeMoq.Mock.ofType<Logger>();

            if (outputTrace) {
              // eslint-disable-next-line no-console
              mockedLogger.setup(x => x.trace(TypeMoq.It.isAny())).callback(msg => console.log(msg));
            }

            const reflector = new ApiReflector(mockedLogger.object, [inputPath], getDefaultCompilerOptions());
            const reflectorResult = reflector.reflect();

            const mockedWriter = TypeMoq.Mock.ofType<IWriter>();
            let writerOutput = '';
            // eslint-disable-next-line no-return-assign
            mockedWriter.setup(x => x.write(TypeMoq.It.isAny())).callback(content => (writerOutput += content));

            const metadata: IClientMetadata = {
              name: 'test',
              version: '1.0.0',
              servers: [],
            };

            let interpreter: BaseClientInterpreter;

            switch (interpreterName) {
              case 'openapi':
                interpreter = new OpenApiInterpreter(mockedWriter.object, metadata);
                break;
              case 'typescript':
                interpreter = new TypescriptInterpreter(mockedWriter.object, metadata, false);
                break;
              default:
                throw new Error(`Unexpected interpreter of '${interpreterName}'`);
            }

            const builder = new ClientGenerator(mockedLogger.object, reflector.checker, [interpreter]);

            // Act
            await builder.process(reflectorResult);

            // Assert
            if (outputContent) {
              // eslint-disable-next-line no-console
              console.log({ writerOutput });
            }

            const expectedOutput = await FileSystem.readFileAsync(expectedOutputFile.path);
            chai
              .expect(writerOutput.trim(), 'Output from writer did not match expected output')
              .is.equal(expectedOutput.toString().trim());
          }).timeout(20000);
        } else {
          it(`When ${testCase.name} is provided, then expected output should be generated`, () => {
            throw new Error(`Expected output not found for interpreter '${interpreterName}'`);
          });
        }
      }
    });
  }
});
