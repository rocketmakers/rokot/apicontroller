// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
import * as path from 'path';
import { testClientGeneratorScenario } from '..';
import { InterpreterData } from '../../interpreters';
import { IInterfaceData } from '../../types/metadata';

describe('AnonymousTypes', () => {
  testClientGeneratorScenario(
    'When an anonymous type is defined in a class, then the generated name of the anonymous type only goes up to the class',
    path.join(__dirname, 'anonymousTypeInClass.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(5);

      const expectedController: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'TestClassAnon',
        properties: [{
          name: 'foo',
          isNullable: false,
          type: {
            kind: 'literal',
            name: 'string'
          }
        }]
      };
      chai.expect(data[1], 'Expected controller to be present').to.deep.include(expectedController);
    });

  testClientGeneratorScenario(
    'When an anonymous type is defined in an interface, then the generated name of the anonymous type only goes up to the interface',
    path.join(__dirname, 'anonymousTypeInInterface.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(5);

      const expectedController: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'ITestInterfaceAnon',
        properties: [{
          name: 'foo',
          isNullable: false,
          type: {
            kind: 'literal',
            name: 'string'
          }
        }]
      };
      chai.expect(data[1], 'Expected controller to be present').to.deep.include(expectedController);
    });

  testClientGeneratorScenario(
    'When an anonymous type is defined in an interface within a class, then the generated name of the anonymous type only goes up to the first parent interface',
    path.join(__dirname, 'anonymousTypeInChildInterface.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(6);

      const expectedController: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IChildInterfaceAnon',
        properties: [{
          name: 'foo',
          isNullable: false,
          type: {
            kind: 'literal',
            name: 'string'
          }
        }]
      };
      chai.expect(data[1], 'Expected controller to be present').to.deep.include(expectedController);
    });

    testClientGeneratorScenario(
      'When an anonymous type is defined in something other than an interface or class, then the generated name goes all the way to the top',
      path.join(__dirname, 'anonymousTypeInLine.ts'),
      (data: InterpreterData[]) => {
        chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(4);
  
        const expectedController: Partial<IInterfaceData> = {
          kind: 'interface',
          name: 'TestGetAllResponse',
          properties: [{
            name: 'foo',
            isNullable: false,
            type: {
              kind: 'literal',
              name: 'string'
            }
          }]
        };
        chai.expect(data[1], 'Expected controller to be present').to.deep.include(expectedController);
      });
});
