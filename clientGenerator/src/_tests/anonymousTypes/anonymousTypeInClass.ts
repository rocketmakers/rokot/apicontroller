/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */

export class TestClass {
  anon: {
    foo: string;
  }
}

@api.controller('TestController')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<TestClass, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
