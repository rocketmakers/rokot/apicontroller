/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */

export interface IChildInterface {
  anon: {
    foo: string;
  }
}

export interface IParentInterface {
  child: IChildInterface
}

@api.controller('TestController')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IParentInterface, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
