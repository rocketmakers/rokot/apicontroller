// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
import * as path from 'path';
import { testClientGeneratorScenario } from '..';
import { InterpreterData } from '../../interpreters';
import { ILiteralData, IControllerData, IClassData, IInterfaceData } from '../../types/metadata';

describe('Imports', () => {
  testClientGeneratorScenario(
    'When a class is referenced via an import, then the class is resolved as a type',
    path.join(__dirname, 'directImport.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(4);

      const expectedLiteral: Partial<ILiteralData> = {
        kind: 'literal',
        name: 'string',
      };
      chai.expect(data[0], 'Expected literal to be present').to.deep.include(expectedLiteral);

      const expectedClass: Partial<IClassData> = {
        kind: 'class',
        name: 'TestClass',
        baseTypes: [],
        properties: [
          {
            name: 'stringProperty',
            type: {
              kind: 'literal',
              name: 'string',
            },
            isNullable: false,
          },
        ],
      };
      chai.expect(data[1], 'Expected class to be present').to.deep.include(expectedClass);

      const expectedSecondLiteral: Partial<ILiteralData> = {
        kind: 'literal',
        name: 'void',
      };
      chai.expect(data[2], 'Expected literal to be present').to.deep.include(expectedSecondLiteral);

      const expectedController: Partial<IControllerData> = {
        kind: 'controller',
        name: 'Test',
        root: 'test',
      };
      chai.expect(data[3], 'Expected controller to be present').to.deep.include(expectedController);
    });

  testClientGeneratorScenario(
    'When an alias is referenced via an import, then the aliased type is discovered with the alias name',
    path.join(__dirname, 'aliasImport.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(4);

      const expectedLiteral: Partial<ILiteralData> = {
        kind: 'literal',
        name: 'string',
      };
      chai.expect(data[0], 'Expected literal to be present').to.deep.include(expectedLiteral);

      const expectedInterface: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'PartialTestClass',
        baseTypes: [],
        properties: [
          {
            name: 'stringProperty',
            type: {
              kind: 'literal',
              name: 'string',
            },
            isNullable: true,
          },
        ],
      };
      chai.expect(data[1], 'Expected class to be present').to.deep.include(expectedInterface);

      const expectedSecondLiteral: Partial<ILiteralData> = {
        kind: 'literal',
        name: 'void',
      };
      chai.expect(data[2], 'Expected literal to be present').to.deep.include(expectedSecondLiteral);

      const expectedController: Partial<IControllerData> = {
        kind: 'controller',
        name: 'Test',
        root: 'test',
      };
      chai.expect(data[3], 'Expected controller to be present').to.deep.include(expectedController);
    });
});
