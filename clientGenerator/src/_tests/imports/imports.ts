/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
export class TestClass {
  stringProperty: string;
}

export type PartialTestClass = Partial<TestClass>;
