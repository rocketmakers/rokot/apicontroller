/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
import { TestClass } from './imports';

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<TestClass, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
