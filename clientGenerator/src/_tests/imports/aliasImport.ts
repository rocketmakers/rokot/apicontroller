/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
import { PartialTestClass } from './imports';

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('partial')
  @api.verbs('get')
  public async partial(req: IGetRequest<PartialTestClass, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
