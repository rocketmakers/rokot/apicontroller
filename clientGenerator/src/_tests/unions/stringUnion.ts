/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
type Order = 'ASC' | 'DESC';

export interface ISearch {
  username: Order;
  forename: Order;
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<void, void, ISearch>): Promise<void> {
    return req.sendOk([]);
  }
}
