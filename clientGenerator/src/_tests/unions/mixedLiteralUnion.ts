/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
enum TestEnum {
  One = 'TestOne',
  Two = 'TestTwo',
}

type MixedUnion = 'none' | TestEnum.One | TestEnum.Two;

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<void, void, MixedUnion>): Promise<void> {
    return req.sendOk([]);
  }
}
