/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
export type NullableNumber = number | null;
export type UndefinableBool = number | undefined;

export interface ISearch {
  score: NullableNumber;
  include: UndefinableBool;
}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<void, void, ISearch>): Promise<void> {
    return req.sendOk([]);
  }
}
