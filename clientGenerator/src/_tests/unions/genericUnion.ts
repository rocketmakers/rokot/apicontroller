/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
interface IClassOne {
  propertyOne: string;
}

interface IClassTwo<TPropertyType> {
  propertyOne: TPropertyType;
}

type GenericUnion<TType> = IClassOne | IClassTwo<TType>;

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<void, void, GenericUnion<number>>): Promise<void> {
    return req.sendOk([]);
  }
}
