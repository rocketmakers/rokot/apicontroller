/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
enum ExampleEnumOne {
  One = 'One',
  Two = 'Two'
}

enum ExampleEnumTwo {
  Three = 'Three',
  Four = 'Four'
}

interface IExampleOne {
  kind: ExampleEnumOne;
}

interface IExampleTwo {
  kind: ExampleEnumTwo;
}

type Union = IExampleOne | IExampleTwo;

type IndexedUnion = Union['kind'];

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<void, void, IndexedUnion>): Promise<void> {
    return req.sendOk([]);
  }
}
