// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
import * as path from 'path';
import { testClientGeneratorScenario } from '..';
import { InterpreterData } from '../../interpreters';
import { IControllerData, IInterfaceData, IUnionData, IPropertyData, ILiteralData, IEnumData } from '../../types/metadata';

describe('Unions', () => {
  testClientGeneratorScenario(
    'When a union is discovered that consists of just strings, then the union is converted to an enum',
    path.join(__dirname, 'stringUnion.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(4);

      const expectedLiteral: Partial<ILiteralData> = {
        kind: 'literal',
        name: 'void',
      };
      chai.expect(data[0], 'Expected literal to be present').to.deep.include(expectedLiteral);

      const expectedEnum: Partial<IEnumData> = {
        kind: 'enum',
        name: 'Order',
        values: [
          {
            key: 'ASC',
            value: 'ASC',
          },
          {
            key: 'DESC',
            value: 'DESC',
          },
        ],
      };
      chai.expect(data[1], 'Expected enum to be present').to.deep.include(expectedEnum);

      const expectedInterface: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'ISearch',
      };
      chai.expect(data[2], 'Expected controller to be present').to.deep.include(expectedInterface);

      // Check that each property is pointing at our enum/union
      for (const property of (data[2] as IInterfaceData).properties) {
        chai.expect(property, 'Expected controller to be present').to.deep.include({
          type: expectedEnum,
        });
      }

      const expectedController: Partial<IControllerData> = {
        kind: 'controller',
        name: 'Test',
        root: 'test',
      };
      chai.expect(data[3], 'Expected controller to be present').to.deep.include(expectedController);
    });

  testClientGeneratorScenario(
    'When a union with a generic is discovered and the generic is resolved by the controller, then the union is converted to that resolved generic',
    path.join(__dirname, 'genericUnion.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(7);

      const expectedVoidLiteral: Partial<ILiteralData> = {
        kind: 'literal',
        name: 'void',
      };
      chai.expect(data[0], 'Expected void to be present').to.deep.include(expectedVoidLiteral);

      const expectedNumberLiteral: Partial<ILiteralData> = {
        kind: 'literal',
        name: 'number',
      };
      chai.expect(data[1], 'Expected number to be present').to.deep.include(expectedNumberLiteral);

      const expectedStringLiteral: Partial<ILiteralData> = {
        kind: 'literal',
        name: 'string',
      };
      chai.expect(data[2], 'Expected string to be present').to.deep.include(expectedStringLiteral);

      const expectedFirstInterface: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IClassOne',
        properties: [
          {
            name: 'propertyOne',
            isNullable: false,
            type: {
              kind: 'literal',
              name: 'string',
            },
          },
        ],
      };
      chai.expect(data[3], 'Expected interface to be present').to.deep.include(expectedFirstInterface);

      const expectedSecondInterface: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IClassTwo_number',
        properties: [
          {
            name: 'propertyOne',
            isNullable: false,
            type: {
              kind: 'literal',
              name: 'number',
            },
          },
        ],
      };
      chai.expect(data[4], 'Expected interface to be present').to.deep.include(expectedSecondInterface);

      const expectedUnion: Partial<IUnionData> = {
        kind: 'union',
        name: 'GenericUnion_number',
      };
      chai.expect(data[5], 'Expected union to be present').to.deep.include(expectedUnion);

      const actualUnion = data[5] as IUnionData;
      chai
        .expect(actualUnion.options[0], 'Expected interface to be present as option in union')
        .to.deep.include(expectedFirstInterface);
      chai
        .expect(actualUnion.options[1], 'Expected interface to be present as option in union')
        .to.deep.include(expectedSecondInterface);

      const expectedController: Partial<IControllerData> = {
        kind: 'controller',
        name: 'Test',
        root: 'test',
      };
      chai.expect(data[6], 'Expected controller to be present').to.deep.include(expectedController);
    });

  testClientGeneratorScenario(
    'When a union is discovered that consists of strings and enum members, then the union is converted to an enum',
    path.join(__dirname, 'mixedLiteralUnion.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(3);

      const expectedEnum: Partial<IEnumData> = {
        kind: 'enum',
        name: 'MixedUnion',
        values: [
          {
            key: 'none',
            value: 'none',
          },
          {
            key: 'TestOne',
            value: 'TestOne',
          },
          {
            key: 'TestTwo',
            value: 'TestTwo',
          },
        ],
      };
      chai.expect(data[1], 'Expected enum to be present').to.deep.include(expectedEnum);

      const expectedController: Partial<IControllerData> = {
        kind: 'controller',
        name: 'Test',
        root: 'test',
      };
      chai.expect(data[2], 'Expected controller to be present').to.deep.include(expectedController);
    });

  testClientGeneratorScenario(
    'When a union is discovered that has a null value, then the property is marked as nullable',
    path.join(__dirname, 'nullableUnion.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(6);

      const expectedNullableUnion: Partial<IUnionData> = {
        kind: 'union',
        name: 'NullableNumber',
        isNullable: true,
      };
      chai.expect(data[2], 'Expected union to be present').to.deep.include(expectedNullableUnion);

      const expectedUndefinedUnion: Partial<IUnionData> = {
        kind: 'union',
        name: 'UndefinableBool',
        isNullable: true,
      };
      chai.expect(data[3], 'Expected union to be present').to.deep.include(expectedUndefinedUnion);

      const expectedInterface: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'ISearch',
      };
      chai.expect(data[4], 'Expected union to be present').to.deep.include(expectedInterface);

      const actualInterface = data[4] as IInterfaceData;
      chai.expect(actualInterface.properties.length, 'Unexpected number of properties on interface').to.equal(2);

      const expectedNullableProperty: Partial<IPropertyData> = {
        isNullable: true,
        name: 'score'
      }

      chai.expect(actualInterface.properties[0], 'Unexpected property').to.deep.include(expectedNullableProperty);

      const expectedUndefinableProperty: Partial<IPropertyData> = {
        isNullable: true,
        name: 'include'
      }

      chai.expect(actualInterface.properties[1], 'Unexpected property').to.deep.include(expectedUndefinableProperty);

      const expectedController: Partial<IControllerData> = {
        kind: 'controller',
        name: 'Test',
        root: 'test',
      };
      chai.expect(data[5], 'Expected controller to be present').to.deep.include(expectedController);
    });

  testClientGeneratorScenario(
    'When a type references an indexed union of enum types, then the enums should be unionised',
    path.join(__dirname, 'enumUnion.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(5);

      const expectedUnion: Partial<IUnionData> = {
        kind: 'union',
        name: 'IndexedUnion',
        options: [
          {
            kind: 'enum',
            name: 'ExampleEnumOne',
            values: [
              {
                key: 'One',
                value: 'One'
              },
              {
                key: 'Two',
                value: 'Two'
              }
            ]
          },
          {
            kind: 'enum',
            name: 'ExampleEnumTwo',
            values: [
              {
                key: 'Three',
                value: 'Three'
              },
              {
                key: 'Four',
                value: 'Four'
              }
            ]
          }
        ]
      };
      chai.expect(data[3], 'Expected union to be present').to.deep.include(expectedUnion);
    });
});
