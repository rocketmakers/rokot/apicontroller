/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */

export interface IAnyProperty {
  [key: string]: object | any;
}

@api.controller('TestController')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IAnyProperty, void, void>): Promise<void> {
    return req.sendOk([]);
  }
}
