// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
import * as path from 'path';
import { testClientGeneratorScenario } from '..';
import { InterpreterData } from '../../interpreters';
import { IInterfaceData } from '../../types/metadata';

describe('Any Property', () => {
  testClientGeneratorScenario(
    'When an "any property" is defined on an interface',
    path.join(__dirname, 'anyPropertyInterface.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(3);

      const expectedKeyOfObject: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IAnyProperty',
        properties: [],
        supportsAdditionalProperties: true,
      };
      chai.expect(data[0], 'Expected object of keyof property to reference generated enum').to.deep.include(expectedKeyOfObject);
    });
});
