/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
export type OpaqueString<Key extends string> = string & { __opaque: Key };
export type OpaqueNumber<Key extends string> = number & { __opaque: Key };

export type TestGuidId = OpaqueString<'guid'>;
export type TestNumberId = OpaqueNumber<'number'>;

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<TestGuidId, TestNumberId, void>): Promise<void> {
    return req.sendOk([]);
  }
}
