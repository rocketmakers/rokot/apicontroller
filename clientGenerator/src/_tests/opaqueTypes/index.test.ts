// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
import * as path from 'path';
import { testClientGeneratorScenario } from '..';
import { InterpreterData } from '../../interpreters';
import { IOpaqueTypeDefinitionData } from '../../types/metadata';

describe('Opaque Types', () => {
  testClientGeneratorScenario(
    'When a known opaque type is known, then they are extracted correctly',
    path.join(__dirname, 'opaqueTypes.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(10);

      const expectedOpaqueString: Partial<IOpaqueTypeDefinitionData> = {
        kind: 'opaque-type-def',
        name: 'OpaqueString',
        type: {
          kind: 'literal',
          name: 'string'
        }
      };
      chai.expect(data[1], 'Expected opaque string to be present').to.deep.include(expectedOpaqueString);

      const expectedOpaqueNumber: Partial<IOpaqueTypeDefinitionData> = {
        kind: 'opaque-type-def',
        name: 'OpaqueNumber',
        type: {
          kind: 'literal',
          name: 'number'
        }
      };
      chai.expect(data[5], 'Expected opaque number to be present').to.deep.include(expectedOpaqueNumber);
    });
});
