// eslint-disable-next-line import/no-extraneous-dependencies
import * as TypeMoq from 'typemoq';
import { Logger } from '@rocketmakers/log';
import { ApiReflector } from '../apiReflector';
import { IWriter, IClientMetadata, BaseClientInterpreter, InterpreterData } from '../interpreters';
import { ClientGenerator } from '../clientGenerator';
import { getDefaultCompilerOptions } from '../utils/typescript';

export class MockedInterpreter extends BaseClientInterpreter {
  public writtenData: InterpreterData[] = [];

  constructor(writer: IWriter, metadata: IClientMetadata) {
    super(writer, metadata);
  }

  public write(data: InterpreterData): Promise<void> {
    this.writtenData.push(data);
    return Promise.resolve();
  }

  public finalise(): Promise<void> {
    return Promise.resolve();
  }
}

export function testClientGeneratorScenario(
  scenario: string,
  inputPath: string,
  assertOutput: (data: InterpreterData[]) => void,
  outputTrace: boolean = false) {

  it(scenario, async () => {
    // Arrange
    const mockedLogger = TypeMoq.Mock.ofType<Logger>();
    if (outputTrace) {
      // eslint-disable-next-line no-console
      mockedLogger.setup(x => x.trace(TypeMoq.It.isAny())).callback(msg => console.log(msg));
    }

    const reflector = new ApiReflector(mockedLogger.object, [inputPath], getDefaultCompilerOptions());
    const reflectorResult = reflector.reflect();

    const mockedWriter = TypeMoq.Mock.ofType<IWriter>();

    const metadata: IClientMetadata = {
      name: 'test',
      version: '1.0.0',
      servers: [],
    };

    const interpreter = new MockedInterpreter(mockedWriter.object, metadata);

    const builder = new ClientGenerator(mockedLogger.object, reflector.checker, [interpreter]);

    // Act
    await builder.process(reflectorResult);

    // Assert
    assertOutput(interpreter.writtenData);
  }).timeout(20000)
}