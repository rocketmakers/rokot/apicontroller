// eslint-disable-next-line import/no-extraneous-dependencies
import * as chai from 'chai';
import * as path from 'path';
import { testClientGeneratorScenario } from '..';
import { InterpreterData } from '../../interpreters';
import { IInterfaceData, IIntersectionData } from '../../types/metadata';

describe('Type Alias', () => {
  testClientGeneratorScenario(
    'When a type alias is generic, then the alias name contains the generic type names',
    path.join(__dirname, 'genericTypeAlias.ts'),
    (data: InterpreterData[]) => {
      chai.expect(data, 'Unexpected number of data records extracted').is.lengthOf(14);

      const expectedIntersection: Partial<IIntersectionData> = {
        kind: 'intersection',
        name: 'DtoOf_ICoreOneData_string',
      };
      chai.expect(data[7], 'Expected intersection to be present').to.deep.include(expectedIntersection);

      const expectedInterface: Partial<IInterfaceData> = {
        kind: 'interface',
        name: 'IEntityOne'
      };

      const actualInterface = data[8] as IInterfaceData;
      chai.expect(actualInterface, 'Expected class to be present').to.deep.include(expectedInterface);
      chai.expect(actualInterface.baseTypes.length, 'Unexpected number of base types for interface').to.equal(1);
      chai.expect(actualInterface.baseTypes[0], 'Expected interface to inherit from aliased intersection').to.deep.include(expectedIntersection);
      
    }
  );
});
