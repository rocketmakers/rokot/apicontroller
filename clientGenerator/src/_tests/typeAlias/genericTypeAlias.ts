/* tslint:disable */
/* eslint-disable */
/* prettier-ignore */
export interface IUpdatedOn {
  updatedOn: string;
}

export interface IUpdatable extends IUpdatedOn {
  updatedBy: string;
}

export interface ICreatedOn {
  createdOn: string;
}

export interface ICreatable extends ICreatedOn {
  createdBy: string;
}

export interface IIdentifiable<TId> {
  id: TId;
}

export type DtoOf<TCreate, TId> = TCreate & IUpdatable & ICreatable & IIdentifiable<TId>;

export interface ICoreOneData {
  hello: string;
}

export interface ICoreTwoData {
  world: string;
}

export interface IEntityOne extends DtoOf<ICoreOneData, string> {}

export interface IEntityTwo extends DtoOf<ICoreTwoData, string> {}

@api.controller('TestController', 'test')
export class TestController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function,no-empty-function
  constructor() {}

  @api.route('all')
  @api.verbs('get')
  public async getAll(req: IGetRequest<IEntityOne, IEntityTwo, void>): Promise<void> {
    return req.sendOk([]);
  }
}
