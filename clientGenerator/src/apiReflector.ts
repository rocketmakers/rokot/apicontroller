import * as ts from 'typescript';
import * as _ from 'underscore';
import { Logger } from '@rocketmakers/log';

export interface IRegister {
  types: ITypeReg[];
  variables: ts.Node[];
}
export interface ITypeReg {
  name: string;
  type: ts.Type;
  node: ts.Node;
  allNodes: ts.Node[];
  dragNodes: ts.Node[];
}

interface ITypeRegDictionary {
  [id: string]: ITypeReg;
}

export interface IReflectionResult {
  api: ts.ClassDeclaration[];
  dependencies: IRegister;
}

function allowSourcePath(sourcePath: string, excludePaths: string[]) {
  return _.every(excludePaths, exclude => sourcePath.indexOf(exclude) === -1);
}

export class ApiReflector {
  private program: ts.Program;

  public checker: ts.TypeChecker;

  constructor(
    private logger: Logger,
    sourcePaths: string[],
    options: ts.CompilerOptions = {},
    private excludeSourcePaths?: string[]
  ) {
    const host = ts.createCompilerHost(options);
    this.program = ts.createProgram(sourcePaths, options, host);
    this.checker = this.program.getTypeChecker();
    const diagnostics = ts.getPreEmitDiagnostics(this.program);
    if (diagnostics.length) {
      logger.warn('Diagnostic Errors:');
      diagnostics.forEach(d => {
        logger.warn(d.messageText + (d.file ? ` [${d.file.fileName} - ${d.start}]` : ''));
      });
    }
  }

  private getIdFromType(type: ts.Type) {
    // Ids exist, but they are internal so we need to work against typescript to get them
    return (type as any).id;
  }

  reflect(): IReflectionResult {
    const types: ITypeRegDictionary = {};
    const sourceFiles = this.program.getSourceFiles();
    sourceFiles.forEach(sourceFile => {
      if (allowSourcePath(sourceFile.fileName, this.excludeSourcePaths || [])) {
        this.logger.trace('READING SOURCE: ', sourceFile.fileName);
        this.collectTypes(sourceFile as any, types);
      } else {
        this.logger.trace('IGNORING SOURCE: ', sourceFile.fileName);
      }
    });
    const allTypes = _.values(types);
    allTypes.forEach(t => {
      if (!ts.isTypeAliasDeclaration(t.node)) {
        return;
      }
      if (!ts.isTypeReferenceNode(t.node.type)) {
        return;
      }
      const nodes = t.type.aliasSymbol && this.findTypeNodesForSymbol(t.type.aliasSymbol, types);
      if (!nodes) {
        return;
      }

      if (nodes) {
        nodes.forEach(n => {
          n.dragNodes.push(...t.allNodes);
        });
      }
    });
    const controllers = allTypes
      .map(t => t.node)
      .filter(n => {
        if (ts.isClassDeclaration(n)) {
          const items = ApiReflector.getApiAttr(n.decorators, 'controller');
          return items && items.length > 0;
        }

        return false;
      }) as ts.ClassDeclaration[];

    const foundRegistrations: IRegister = { types: [], variables: [] };
    const includeExtra = allTypes
      .map(t => t.node)
      .filter(
        n => ts.isInterfaceDeclaration(n) && n.name.text === 'IApiGenIncludePropertyTypes'
      ) as ts.InterfaceDeclaration[];
    includeExtra.forEach(i => {
      i.members.forEach(m => {
        if (ts.isPropertySignature(m)) {
          this.processTypeNode(m.type, foundRegistrations, types);
        }
      });
    });

    includeExtra.forEach(e => {
      this.logger.info(`Found 'include' interface ${e.name.text} in ${e.getSourceFile().fileName}`);
    });

    controllers.forEach(i => {
      const routes = i.members.filter(m => {
        const items = ApiReflector.getApiAttr(m.decorators, 'route');
        return items && items.length > 0;
      });

      routes.forEach(r => {
        const method = r as ts.MethodDeclaration;
        const param = method.parameters[0];
        if (param.type) {
          this.processTypeArguments(param.type, foundRegistrations, types);
        }
      });
    });

    return { api: controllers, dependencies: foundRegistrations };
  }

  private findSymbolNode(symbol: ts.Symbol, types: ITypeRegDictionary) {
    return _.find(_.values(types), t => t.type.symbol === symbol);
  }

  private findTypeNodesForSymbol(symbol: ts.Symbol, types: ITypeRegDictionary) {
    const decs = symbol && symbol.getDeclarations();
    if (decs) {
      const bases: ITypeReg[] = _.compact(
        _.flatten(
          decs.map(d => {
            const type1 = this.checker.getTypeAtLocation(d);
            if (type1) {
              return types[this.getIdFromType(type1)];
            }

            return undefined;
          })
        )
      );
      if (bases.length) {
        return bases;
      }
    }

    return undefined;
  }

  private findTypeNode(node: ts.Node, types: ITypeRegDictionary): ITypeReg[] | undefined {
    const type = this.checker.getTypeAtLocation(node);
    if (!type) {
      return undefined;
    }
    const found = types[this.getIdFromType(type)];
    if (!found) {
      const nodes = this.findTypeNodesForSymbol(type.symbol, types);
      if (nodes) {
        return nodes;
      }
      return undefined;
    }

    return [found];
  }

  private collectTypes(node: ts.TypeNode, types: ITypeRegDictionary) {
    if (this.isKeywordType(node.kind)) {
      return false;
    }

    this.nodeRecurser(node, n => {
      let targetNode = n;
      if (ts.isArrayTypeNode(targetNode)) {
        targetNode = targetNode.elementType;
      }

      const isType =
        targetNode.kind === ts.SyntaxKind.EnumDeclaration ||
        targetNode.kind === ts.SyntaxKind.TypeAliasDeclaration ||
        targetNode.kind === ts.SyntaxKind.ClassDeclaration ||
        targetNode.kind === ts.SyntaxKind.InterfaceDeclaration;
      if (isType) {
        const t = this.checker.getTypeAtLocation(targetNode);
        const id = this.getIdFromType(t);
        if (types[id]) {
          types[id].allNodes.push(targetNode);
          return false;
        }

        // eslint-disable-next-line no-param-reassign
        types[id] = {
          name: t.symbol ? t.symbol.name : targetNode.getText(), // "[ANON]"),
          type: t,
          node: targetNode,
          allNodes: [targetNode],
          dragNodes: [],
        };

        return false;
      }
      return true; // recurse this node
    });

    return false;
  }

  private nodeRecurser(node: ts.Node, shouldRecurseChildren: (n: ts.Node) => boolean) {
    if (shouldRecurseChildren(node)) {
      ts.forEachChild(node, n => this.nodeRecurser(n, shouldRecurseChildren));
    }
  }

  private visitedNodes: number[] = [];

  private processNodes(r: ITypeReg, foundRegistrations: IRegister, types: ITypeRegDictionary) {
    if (this.visitedNodes.indexOf(this.getIdFromType(r.type)) > -1) {
      return;
    }

    this.visitedNodes.push(this.getIdFromType(r.type));

    if (ts.isTypeAliasDeclaration(r.node)) {
      this.processTypeNode(r.node.type, foundRegistrations, types);
    }

    const inter = r.node as ts.InterfaceDeclaration;
    if (inter.typeParameters) {
      this.processTypeParameter(inter.typeParameters, foundRegistrations, types);
    }

    if (inter.heritageClauses) {
      inter.heritageClauses.forEach(hc => {
        hc.types.forEach(hcType => {
          if (hcType.typeArguments) {
            hcType.typeArguments.forEach(hcTypeArg => {
              this.processTypeNode(hcTypeArg, foundRegistrations, types);
            });
          }
        });
      });
    }

    if (inter.members) {
      inter.members.forEach(m => {
        if (ts.isEnumMember(m)) {
          return;
        }
        // ts.isIndexSignatureDeclaration
        if (ts.isPropertySignature(m) || ts.isIndexSignatureDeclaration(m) || ts.isPropertyDeclaration(m)) {
          this.processTypeNode(m.type, foundRegistrations, types);
          return;
        }

        if (ts.isMethodDeclaration(m) || ts.isConstructorDeclaration(m)) {
          this.processTypeNode(m.type, foundRegistrations, types);
          if (m.parameters) {
            m.parameters.forEach(tp => {
              this.processTypeNode(tp.type, foundRegistrations, types);
            });
          }

          if (m.typeParameters) {
            this.processTypeParameter(m.typeParameters, foundRegistrations, types);
          }

          return;
        }
        this.logger.warn(`Unknown member type: ${ts.SyntaxKind[m.kind]}`);
      });
    }

    this.processTypeAlias(r.type, foundRegistrations, types);

    if (!r.type.symbol) {
      if (ts.isTypeAliasDeclaration(r.node)) {
        if (!this.processTypeNode(r.node.type, foundRegistrations, types)) {
          this.logger.warn(`Unknown TypeAliasDeclaration type`);
        }
        return;
      }

      this.logger.warn(`Unknown type without symbol: ${ts.SyntaxKind[r.node.kind]}`);
      return;
    }

    const out = this.getBaseSymbols(r.type, foundRegistrations, types); // || "[NULL]"
    this.processTypeRegistrations(out, foundRegistrations, types);
  }

  private isKeywordType(kind: ts.SyntaxKind) {
    return ts.SyntaxKind[kind].indexOf('Keyword') > -1;
  }

  private processTypeParameter(
    typeParameters: ts.NodeArray<ts.TypeParameterDeclaration>,
    foundRegistrations: IRegister,
    types: ITypeRegDictionary
  ) {
    if (typeParameters) {
      typeParameters.forEach(tp => {
        if (tp.constraint) {
          this.processTypeNode(tp.constraint, foundRegistrations, types);
        }
      });
    }
  }

  private processTypeArguments(typeNode: ts.TypeNode, foundRegistrations: IRegister, types: ITypeRegDictionary) {
    const typeArgs = (typeNode as any).typeArguments;
    if (typeArgs) {
      typeArgs.forEach((t: any) => {
        this.processTypeNode(t, foundRegistrations, types);
      });
    }
  }

  private processTypeNode(
    typeNode: ts.TypeNode | undefined,
    foundRegistrations: IRegister,
    types: ITypeRegDictionary
  ): boolean {
    if (!typeNode) {
      return false;
    }

    if (ts.isIndexedAccessTypeNode(typeNode)) {
      return this.processTypeNode(typeNode.objectType, foundRegistrations, types);
    }

    if (ts.isTypeQueryNode(typeNode)) {
      const scoped = this.checker.getSymbolsInScope(typeNode, ts.SymbolFlags.Variable);
      const txt = (typeNode.exprName as any).text;
      const sc = scoped.filter(s => s.name === txt);
      if (sc.length !== 1) {
        this.logger.error(`Unable to find variable declaration ${txt}`);
        return true;
      }
      const node = sc[0].declarations[0].parent;
      if (foundRegistrations.variables.indexOf(node) === -1) {
        foundRegistrations.variables.push(node);
      }
      return true;
    }

    if (this.isKeywordType(typeNode.kind)) {
      return true;
    }

    this.processTypeArguments(typeNode, foundRegistrations, types);
    if (typeNode.kind === ts.SyntaxKind.LastTypeNode) {
      return true;
    }
    if (ts.isArrayTypeNode(typeNode)) {
      return this.processTypeNode(typeNode.elementType, foundRegistrations, types);
    }

    if (ts.isUnionTypeNode(typeNode) || ts.isIntersectionTypeNode(typeNode)) {
      typeNode.types.forEach(id => {
        this.processTypeNode(id, foundRegistrations, types);
      });
      return true;
    }
    if (ts.isParenthesizedTypeNode(typeNode)) {
      return this.processTypeNode(typeNode.type, foundRegistrations, types);
    }
    if (ts.isTypeOperatorNode(typeNode)) {
      return this.processTypeNode(typeNode.type, foundRegistrations, types);
    }
    const typeLiteralNode = typeNode as ts.TypeLiteralNode;
    if (typeLiteralNode.members) {
      typeLiteralNode.members.forEach(mem => {
        const propSig = mem as ts.PropertySignature;
        if (propSig.type) {
          this.processTypeNode(propSig.type, foundRegistrations, types);
        } else {
          this.logger.warn(
            `Unknown TypeLiteralNode member symbol: ${ts.SyntaxKind[mem.kind]} - ${typeLiteralNode.getFullText()}`
          );
        }
      });
    }
    if (ts.isTypeLiteralNode(typeNode)) {
      return true;
    }
    if (ts.isLiteralTypeNode(typeNode)) {
      return true;
    }

    const typedNodes = this.findTypeNode(typeNode, types);
    if (typedNodes) {
      this.processTypeRegistrations(typedNodes, foundRegistrations, types);
      return true;
    }
    const t = this.checker.getTypeFromTypeNode(typeNode);
    if (t.symbol && t.symbol.flags === ts.SymbolFlags.TypeParameter) {
      return true;
    }
    if (this.processTypeAlias(t, foundRegistrations, types)) {
      return true;
    }
    this.logger.warn(
      `Cannot find type registry entry for: ${ts.SyntaxKind[typeNode.kind]} - ${typeNode.getFullText()}`
    );
    return false;
  }

  private processTypeAlias(t: ts.Type, foundRegistrations: IRegister, types: ITypeRegDictionary) {
    if (t.aliasSymbol) {
      const aliasTypedNodes: ITypeReg[] = _.compact(
        _.flatten(t.aliasSymbol.declarations.map(d => this.findTypeNode(d, types)))
      );
      if (aliasTypedNodes) {
        this.processTypeRegistrations(aliasTypedNodes, foundRegistrations, types);
        return true;
      }
    }
    if (t.symbol) {
      const aliasTypedNodes: ITypeReg[] = _.compact(
        _.flatten(t.symbol.declarations.map(d => this.findTypeNode(d, types)))
      );
      if (aliasTypedNodes) {
        this.processTypeRegistrations(aliasTypedNodes, foundRegistrations, types);
        return true;
      }
    }

    return false;
  }

  private processTypeRegistrations(typedNodes: ITypeReg[], foundRegistrations: IRegister, types: ITypeRegDictionary) {
    typedNodes.forEach(n => {
      this.processNodes(n, foundRegistrations, types);
    });
    this.addUnique(typedNodes, foundRegistrations.types);
  }

  private addUnique<T>(add: T[], collect: T[]) {
    add.forEach(o => {
      if (collect.indexOf(o) === -1) {
        collect.push(o);
      }
    });
  }

  private getBaseSymbols(type: ts.Type, foundRegistrations: IRegister, types: ITypeRegDictionary): ITypeReg[] {
    const bt = type.getBaseTypes();
    if (bt) {
      bt.forEach(bbt => {
        this.processTypeAlias(bbt, foundRegistrations, types);
      });
    }

    const tt = bt
      ? bt
          .filter(t => t.symbol)
          .map(t => {
            return this.findSymbolNode(t.symbol as ts.Symbol, types);
          })
      : [];
    const dd: ITypeReg[] = _.flatten(tt.map(t => (t ? this.getBaseSymbols(t.type, foundRegistrations, types) : [])));
    return tt.concat(dd) as any;
  }

  static getApiAttr(decorators: ts.NodeArray<ts.Decorator> | undefined, name: string) {
    if (!decorators || !decorators.length) {
      return undefined;
    }
    return decorators.filter(d => this.isApiAttr(d, name));
  }

  private static isApiAttr(decorator: ts.Decorator, name: string) {
    try {
      return this.decoratorProperty(decorator).name.text === name;
    } catch (e) {
      return false;
    }
  }

  private static decoratorProperty(decorator: ts.Decorator) {
    return (decorator.expression as ts.CallExpression).expression as ts.PropertyAccessExpression;
  }
}
