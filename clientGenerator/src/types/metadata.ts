import { assertNever } from '../utils';

export interface IControllerRouteMethod {
  name: string;
  method: string;
  contentType: string;
  response?: TypeData;
  params?: TypeData;
  query?: TypeData;
  body?: TypeData;
}

export interface IControllerRoute {
  path: string;
  methods: IControllerRouteMethod[];
}

export interface IControllerData {
  kind: 'controller';
  name: string;
  root: string;
  routes: IControllerRoute[];
}

export interface IReferenceData {
  kind: 'reference';
  reference: {
    kind: 'class' | 'interface';
    name: string;
  };
}

export interface IPropertyData {
  name: string;
  type: ReferencableTypeData;
  isNullable: boolean;
}

export interface IInheritable {
  baseTypes: ReferencableTypeData[];
}

export interface IHasProperties {
  properties: IPropertyData[];
  supportsAdditionalProperties: boolean;
}

export interface IClassData extends IInheritable, IHasProperties {
  kind: 'class';
  name: string;
}

export interface IInterfaceData extends IInheritable, IHasProperties {
  kind: 'interface';
  name: string;
}

export interface IArrayData {
  kind: 'array';
  type: TypeData;
}

export interface IEnumData {
  kind: 'enum';
  name: string;
  values: { key: string; value: string }[];
}

export interface IUnionData {
  kind: 'union';
  name: string;
  options: TypeData[];
  commonProperties: IPropertyData[];
  isNullable: boolean;
}

export interface IIntersectionData {
  kind: 'intersection';
  name: string;
  options: TypeData[];
}

export interface ILiteralData {
  kind: 'literal';
  name: 'string' | 'number' | 'boolean' | 'void' | 'any' | 'undefined' | 'datetime' | 'date';
}

export interface ITypeParameterData {
  kind: 'type-parameter';
  name: string;
  type?: TypeData;
}

export interface IQualifiedTypeData {
  kind: 'qualified-type';
  name: string;
  type: IEnumData | ILiteralData;
  value: string;
}

// Because we use OpaqueTypes throught our system to give content to primitive types
// we want to preserve this as it might be useful for certain languages
export interface IOpaqueTypeDefinitionData {
  kind: 'opaque-type-def';
  name: string;
  type: ILiteralData;
}

export interface IOpaqueTypeImplementationData {
  kind: 'opaque-type-impl';
  name: string;
  key: IQualifiedTypeData;
  type: IOpaqueTypeDefinitionData;

  // This is disgusting, but we want to be able to apply certain restrictions on
  // certain opaque types (e.g. date time on DateTimeString)
  restriction?: ILiteralData;
}

type BaseTypeData =
  | IArrayData
  | ILiteralData
  | IEnumData
  | IUnionData
  | IQualifiedTypeData
  | IIntersectionData
  | IOpaqueTypeImplementationData;
export type ReferencableTypeData = IReferenceData | BaseTypeData | IOpaqueTypeDefinitionData;
export type TypeData = IClassData | IInterfaceData | BaseTypeData | IOpaqueTypeDefinitionData;
export type AllTypeData = ReferencableTypeData | TypeData;

export function getBaseTypeDataName(data: BaseTypeData): string {
  switch (data.kind) {
    case 'enum':
    case 'union':
    case 'intersection':
    case 'qualified-type':
    case 'literal':
    case 'opaque-type-impl':
      return data.name;
    case 'array':
      break;
    default:
      assertNever(data, 'Unexpected data kind');
      break;
  }

  return 'Unknown';
}
