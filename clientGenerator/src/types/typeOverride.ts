import { TypeData } from './metadata';

export interface ITypeOverride {
  name: string;
  type: TypeData;
}

export const defaultTypeOverrides: ITypeOverride[] = [];
