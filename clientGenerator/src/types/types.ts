import { ITypeParameterData, TypeData, IControllerData } from './metadata';

export interface IBuilderContext {
  parent: IBuilderContext | undefined;
  name: string;
  kind: (TypeData | IControllerData)['kind'] | 'unknown';
  typeParameters?: ITypeParameterData[];
}
