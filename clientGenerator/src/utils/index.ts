import { IBuilderContext } from '../types/types';

export function buildNameFromContext(context: IBuilderContext, options: { separator?: string }): string {
  let finalName = '';
  let currentContext: IBuilderContext | undefined = context;
  while (currentContext) {
    let { name } = currentContext;
    name = `${name.substring(0, 1).toUpperCase()}${name.substring(1)}`;

    finalName = `${name}${options.separator && finalName ? options.separator : ''}${finalName}`;

    // If we're looking at a class or interface, we've got enough for a name so that we try
    // and not go all the way up the chain and create a ridiculous name
    if (currentContext.kind === 'class' || currentContext.kind === 'interface') {
      break;
    }

    currentContext = currentContext.parent;
  }

  return finalName;
}

export function assertNever(_v: never, message: string) {
  throw new Error(message);
}
