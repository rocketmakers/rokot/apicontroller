import * as ts from 'typescript';

export function getDefaultCompilerOptions(): ts.CompilerOptions {
  return {
    lib: ['lib.es2016.d.ts'],
    target: ts.ScriptTarget.ES2015,
    module: ts.ModuleKind.CommonJS,
    isolatedModules: false,
    experimentalDecorators: true,
    emitDecoratorMetadata: true,
    moduleResolution: ts.ModuleResolutionKind.NodeJs,
    removeComments: false,
    sourceMap: true,
    strict: true,
    noImplicitReturns: true,
    noImplicitThis: true,
    noImplicitAny: true,
    noUnusedLocals: true,
    noUnusedParameters: true,
    forceConsistentCasingInFileNames: true,
    noLib: false,
    importHelpers: true,
    resolveJsonModule: true,
    skipLibCheck: true,
    composite: true,
  };
}
