parameters:
{{#each this.parameters}}
  - in: {{this.parameterType}}
    name: {{this.name}}
    schema:
      {{> openApiTypeReference type=this.type}}
    required: {{this.isRequired}}
    description: Input {{this.parameterType}}
{{/each}}