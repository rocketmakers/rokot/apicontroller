responses:
  200:
    description: "successful operation"
{{#if this.response}}
    content:
      {{this.contentType}}:
        schema:
          {{> openApiTypeReference type=this.response}}
{{/if}}
{{#compare this.responseSupportsNoContent "==" true}}
  204:
    description: No content available
{{/compare}}