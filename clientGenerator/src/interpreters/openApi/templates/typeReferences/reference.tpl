{{#compare this.isNullable "==" true }}
allOf:
  - $ref: "#/components/schemas/{{> openApiTypeName this.type }}"
nullable: true
{{/compare}}
{{#compare this.isNullable "!=" true }}
$ref: "#/components/schemas/{{> openApiTypeName this.type }}"
{{/compare}}