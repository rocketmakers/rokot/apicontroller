type: "array"
items:
  {{> openApiTypeReference type=this.type.type}}
{{#if this.isNullable }}nullable: true
{{/if}}