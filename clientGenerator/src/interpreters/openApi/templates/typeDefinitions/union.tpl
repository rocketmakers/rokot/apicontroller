type: "object"
oneOf:
{{#each options}}  - {{> openApiTypeReference type=this}}{{/each}}
{{#if this.discriminator}}
discriminator:
  propertyName: {{ this.discriminator.name }}
  mapping:
    {{#each mappings}}
    {{this.value}}: "#/components/schemas/{{> openApiTypeName this.type }}"
    {{/each}}
{{/if}}
xml:
  name: "{{this.name}}"