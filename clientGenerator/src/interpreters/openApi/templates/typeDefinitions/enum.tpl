type: "string"
enum:
{{#each this.values}}
  - {{this.value}}
{{/each}}
description: >
{{#each this.values}}
  * {{key}} - {{value}}
{{/each}}
xml:
  name: "{{> openApiTypeName this }}"