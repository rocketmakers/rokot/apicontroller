{{#compare this.hasBaseTypes "==" true }}allOf:
{{#each this.baseTypes}}  - {{> openApiTypeReference type=this}}{{/each}}  - type: "object"
{{#if properties}}
    properties:
      {{#each properties}}
      {{this.name}}:
        {{> openApiTypeReference type=this.type isNullable=this.isNullable}}
      {{/each}}
    {{#if requiredProperties}}
    required:
    {{#each requiredProperties}}
      - {{this.name}}
    {{/each}}
    {{/if}}
{{/if}}    xml:
      name: "{{> openApiTypeName this }}"{{/compare}}{{#compare this.hasBaseTypes "!=" true }}type: "object"
{{#if properties}}
properties:
  {{#each properties}}
  {{this.name}}:
    {{> openApiTypeReference type=this.type isNullable=this.isNullable}}
  {{/each}}
{{#if requiredProperties}}
required:
{{#each requiredProperties}}
  - {{this.name}}
{{/each}}
{{/if}}
{{/if}}xml:
  name: "{{> openApiTypeName this }}"{{/compare}}