type: "object"
allOf:
{{#each options}}  - {{> openApiTypeReference type=this}}{{/each}}
xml:
  name: "{{this.name}}"