import * as Handlebars from 'handlebars';
import { FileSystem } from '@rocketmakers/shell-commands/lib/fs';
import * as path from 'path';
import * as HandlebarsHelpers from 'handlebars-helpers';
import OpenApiSchemaValidator from 'openapi-schema-validator';
import * as yaml from 'js-yaml';
import { BaseClientInterpreter, IWriter, InterpreterData, IClientMetadata, getDiscriminator } from '..';
import {
  IControllerData,
  TypeData,
  AllTypeData,
  getBaseTypeDataName,
  IPropertyData,
  IControllerRouteMethod,
  IControllerRoute,
  ReferencableTypeData,
} from '../../types/metadata';
import { assertNever } from '../../utils';
import { getAllProperties } from '../../nodeProcessor/utils';

interface IParameterType {
  name: string;
  type: ReferencableTypeData;
  parameterType: 'path' | 'query';
  isRequired: boolean;
}

interface ITemplates {
  root: HandlebarsTemplateDelegate<any>;
  response: HandlebarsTemplateDelegate<any>;
  typeDefs: {
    enum: HandlebarsTemplateDelegate<any>;
    intersection: HandlebarsTemplateDelegate<any>;
    union: HandlebarsTemplateDelegate<any>;
    index: HandlebarsTemplateDelegate<any>;
  };
  typeReferences: {
    array: HandlebarsTemplateDelegate<any>;
    literals: {
      index: HandlebarsTemplateDelegate<any>;
      any: HandlebarsTemplateDelegate<any>;
      date: HandlebarsTemplateDelegate<any>;
      datetime: HandlebarsTemplateDelegate<any>;
    };
    qualifiedType: HandlebarsTemplateDelegate<any>;
    reference: HandlebarsTemplateDelegate<any>;
  };
  parameters: HandlebarsTemplateDelegate<any>;
}

export class OpenApiInterpreter extends BaseClientInterpreter {
  private controllers: IControllerData[] = [];

  private types: TypeData[] = [];

  constructor(writer: IWriter, metadata: IClientMetadata) {
    super(writer, metadata);

    HandlebarsHelpers({ handlebars: Handlebars });
  }

  public write(data: InterpreterData): Promise<void> {
    switch (data.kind) {
      case 'class':
      case 'enum':
      case 'union':
      case 'interface':
      case 'intersection':
        this.types.push(data);
        break;
      case 'opaque-type-def':
      case 'opaque-type-impl':
      case 'literal':
      case 'qualified-type':
        // Ignore on purpose
        break;
      case 'controller':
        // OpenApi doesn't support optional parameters, so you have to have two routes; one where the parameter is mandatory
        // and one where the parameter isn't present.
        const newRoutes: { route: IControllerRoute; originalIndex: number }[] = [];
        for (let i = 0; i < data.routes.length; i++) {
          const route = data.routes[i];

          const regex = /:([^/]+\?)(\/|$)/g;
          if (route.path.endsWith('?') || route.path.endsWith('?/')) {
            const matches = route.path.match(regex);
            if (!matches || matches.length !== 1) {
              throw new Error('Unexpected number of matches');
            }

            const newRoute: IControllerRoute = {
              ...route,
              // Remove our optional parameter for our new route
              path: route.path.replace(matches[0], ''),
            };

            // Update our existing route to have the parameter not optional
            route.path.replace(matches[0], matches[0].replace('?', ''));

            newRoutes.push({ route: newRoute, originalIndex: i });
          }
        }

        // Push our new routes next to their original routes
        for (let i = 0; i < newRoutes.length; i++) {
          const newRoute = newRoutes[i];
          data.routes.splice(newRoute.originalIndex + 1 + i, 0, newRoute.route);
        }

        this.controllers.push(data);
        break;
      default:
        assertNever(data, 'Unexpected data kind');
    }

    return Promise.resolve();
  }

  private async loadTemplate(filePath: string): Promise<HandlebarsTemplateDelegate<any>> {
    const rootTemplateContent: Buffer = await FileSystem.readFileAsync(filePath);
    return Handlebars.compile(rootTemplateContent.toString(), { preventIndent: false });
  }

  private resolveTypeReferenceTemplate(context: any, templates: ITemplates): string {
    const type = context.type as AllTypeData;
    switch (type.kind) {
      case 'array':
        return templates.typeReferences.array(context);
      case 'qualified-type':
        return templates.typeReferences.qualifiedType(context);
      case 'class':
      case 'enum':
      case 'interface':
      case 'union':
      case 'intersection':
        return templates.typeReferences.reference(context);
      case 'opaque-type-def':
        throw new Error(`'${type.kind}' not supported for type references`);
      case 'opaque-type-impl':
        // Should process as a literal
        if (type.restriction) {
          return this.resolveTypeReferenceTemplate(
            { type: type.restriction, isNullable: context.isNullable },
            templates
          );
        }

        return this.resolveTypeReferenceTemplate({ type: type.type.type, isNullable: context.isNullable }, templates);
      case 'reference':
        // Treat the type reference as our referenced type
        return templates.typeReferences.reference({ ...context, type: type.reference });
      case 'literal':
        const specificTemplate = templates.typeReferences.literals[context.type.name];
        if (specificTemplate) {
          return specificTemplate(context);
        }

        return templates.typeReferences.literals.index(context);
      default:
        assertNever(type, `Unexpected type: '${type}'`);
        return '';
    }
  }

  private async setup(): Promise<ITemplates> {
    const templates: ITemplates = {
      root: await this.loadTemplate(path.join(__dirname, './templates/root.tpl')),
      response: await this.loadTemplate(path.join(__dirname, './templates/response.tpl')),
      typeDefs: {
        enum: await this.loadTemplate(path.join(__dirname, './templates/typeDefinitions/enum.tpl')),
        intersection: await this.loadTemplate(path.join(__dirname, './templates/typeDefinitions/intersection.tpl')),
        union: await this.loadTemplate(path.join(__dirname, './templates/typeDefinitions/union.tpl')),
        index: await this.loadTemplate(path.join(__dirname, './templates/typeDefinitions/index.tpl')),
      },
      typeReferences: {
        array: await this.loadTemplate(path.join(__dirname, './templates/typeReferences/array.tpl')),
        literals: {
          index: await this.loadTemplate(path.join(__dirname, './templates/typeReferences/literals/index.tpl')),
          any: await this.loadTemplate(path.join(__dirname, './templates/typeReferences/literals/any.tpl')),
          date: await this.loadTemplate(path.join(__dirname, './templates/typeReferences/literals/date.tpl')),
          datetime: await this.loadTemplate(path.join(__dirname, './templates/typeReferences/literals/datetime.tpl')),
        },
        qualifiedType: await this.loadTemplate(path.join(__dirname, './templates/typeReferences/qualifiedType.tpl')),
        reference: await this.loadTemplate(path.join(__dirname, './templates/typeReferences/reference.tpl')),
      },
      parameters: await this.loadTemplate(path.join(__dirname, './templates/parameters.tpl')),
    };

    // Partial template to make rokot controller paths OpenAPI friendly
    Handlebars.registerPartial('openApiCleanPath', context => {
      const paths = context as { controllerPath: string; routePath: string };
      const regex = /:([^/]+)(\/|$)/g;

      let outputPath = `${paths.controllerPath}/${paths.routePath}`;
      outputPath = outputPath.replace('//', '/');
      if (outputPath.startsWith('/') === false) {
        outputPath = `/${outputPath}`;
      }

      const matches = outputPath.match(regex);
      if (matches) {
        for (const match of matches) {
          const endWithSlash = match.endsWith('/');
          outputPath = outputPath.replace(
            match,
            `{${match
              .replace(':', '')
              .replace('/', '')
              .replace('?', '')}}${endWithSlash ? '/' : ''}`
          );
        }
      }

      return outputPath;
    });

    // Partial template to take params/queries defined in a route and manipulate into a format
    // that OpenAPI wants.
    Handlebars.registerPartial('openApiParameters', context => {
      const { controllerPath, routePath } = context;
      const type = context as IControllerRouteMethod;

      const parameters: IParameterType[] = [];

      if (type.params) {
        switch (type.params.kind) {
          case 'class':
          case 'interface':
          case 'intersection':
          case 'union':
            // Ignore any properties that don't exist in the path
            const filteredProperties = getAllProperties(type.params, this.types).filter(p => {
              const targetName = `:${p.name}`;
              return (controllerPath || '').includes(targetName) || (routePath || '').includes(targetName);
            });

            parameters.push(
              ...filteredProperties.map<IParameterType>(x => {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const { isNullable, ...rest } = x;
                return {
                  ...rest,
                  parameterType: 'path',
                  // all path parameters are required
                  isRequired: true,
                };
              })
            );
            break;
          case 'opaque-type-def':
          case 'opaque-type-impl':
          case 'enum':
          case 'literal':
          case 'qualified-type':
          case 'array':
            throw new Error(`Type '${type.params.kind}' are not supported for params`);
          default:
            assertNever(type.params, `Unexpected type: '${type.params}'`);
            return '';
        }
      }

      if (type.query) {
        switch (type.query.kind) {
          case 'class':
          case 'interface':
          case 'intersection':
          case 'union':
            parameters.push(
              ...getAllProperties(type.query, this.types).map<IParameterType>(p => {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const { isNullable, ...rest } = p;
                return {
                  ...rest,
                  parameterType: 'query',
                  isRequired: p.isNullable !== true,
                };
              })
            );
            break;
          case 'opaque-type-def':
            throw new Error(`'${type.query.name}' of type '${type.query.kind}' are not supported for query`);
          case 'opaque-type-impl':
          case 'enum':
          case 'literal':
          case 'qualified-type':
          case 'array':
            throw new Error(
              `'${getBaseTypeDataName(type.query)}' of type '${type.query.kind}' are not supported for query`
            );
          default:
            assertNever(type.query, `Unexpected type: '${type.query}'`);
            return '';
        }
      }

      return parameters.length > 0 ? templates.parameters({ parameters }) : '';
    });

    // Partial template for indicating the definition of the type of a object
    Handlebars.registerPartial('openApiTypeReference', context => {
      return this.resolveTypeReferenceTemplate(context, templates);
    });

    // Partial template to define the schema of an object
    Handlebars.registerPartial('openApiTypeDefinition', context => {
      const type = context as AllTypeData;
      switch (type.kind) {
        case 'enum':
          return templates.typeDefs.enum(context);
        case 'union':
          const discriminator = getDiscriminator(type);
          return templates.typeDefs.union({
            ...context,
            ...discriminator,
          });

        case 'intersection':
          return templates.typeDefs.intersection(context);
        case 'interface':
        case 'class':
          const requiredProperties: IPropertyData[] = type.properties.filter(x => x.isNullable !== true);

          return templates.typeDefs.index({
            ...type,
            hasBaseTypes: (type.baseTypes || []).length > 0,
            requiredProperties: requiredProperties.length > 0 ? requiredProperties : undefined,
          });
        case 'array':
        case 'literal':
          return templates.typeDefs.index(context);
        case 'opaque-type-def':
        case 'opaque-type-impl':
        case 'qualified-type':
        case 'reference':
          throw new Error(`'${type.kind}' should not have definitions`);
        default:
          assertNever(type, `Unexpected type: '${type}'`);
          return '';
      }
    });

    // Partial template to make interfaces look like classes
    Handlebars.registerPartial('openApiTypeName', context => {
      const type = context as AllTypeData;
      switch (type.kind) {
        case 'interface':
          return type.name.startsWith('I') ? type.name.substring(1) : type.name;
        case 'enum':
        case 'union':
        case 'class':
        case 'literal':
        case 'qualified-type':
        case 'intersection':
        case 'opaque-type-def':
        case 'opaque-type-impl':
          return type.name;
        case 'reference':
          return type.reference.name;
        case 'array':
          throw new Error("typeName can't be used on array types");
        default:
          assertNever(type, `Unexpected type: '${type}'`);
          return '';
      }
    });

    // Partial template for repsonses
    Handlebars.registerPartial('openApiResponse', context => {
      const type = context as IControllerRouteMethod;
      if (type.response) {
        switch (type.response.kind) {
          case 'union':
            // If we have an union of two options, where one option is undefined, then we'll need two responses
            if (
              type.response.options.length === 2 &&
              !!type.response.options.find(x => x.kind === 'literal' && x.name === 'undefined')
            ) {
              const positiveOption = type.response.options.find(x => x.kind !== 'literal' || x.name !== 'undefined');

              return templates.response({
                ...context,
                response: positiveOption,
                responseSupportsNoContent: true,
              });
            }

            return templates.response(context);
          case 'interface':
          case 'enum':
          case 'class':
          case 'literal':
          case 'qualified-type':
          case 'intersection':
          case 'opaque-type-def':
          case 'opaque-type-impl':
          case 'array':
            return templates.response(context);
          default:
            assertNever(type.response, `Unexpected type: '${type.response}'`);
            return '';
        }
      } else {
        return templates.response(context);
      }
    });

    return templates;
  }

  public async finalise(): Promise<void> {
    const templates = await this.setup();

    const output = templates.root({ metadata: this.metadata, controllers: this.controllers, types: this.types });

    await this.writer.write(output);

    // Validate that what we've produced is actually valid
    const validator = new OpenApiSchemaValidator({ version: '3.0.0' });
    const result = validator.validate(yaml.safeLoad(output, {}) as any);
    if (result.errors && result.errors.length > 0) {
      throw new Error(`Generated OpenAPI schema is not valid: ${JSON.stringify(result.errors)}`);
    }

    return Promise.resolve();
  }
}
