import { FileSystem } from '@rocketmakers/shell-commands/lib/fs';
import * as fs from 'fs';
import * as path from 'path';
import {
  IControllerData,
  IClassData,
  IInterfaceData,
  ILiteralData,
  IEnumData,
  IUnionData,
  IQualifiedTypeData,
  IIntersectionData,
  IOpaqueTypeImplementationData,
  IOpaqueTypeDefinitionData,
  IPropertyData,
} from '../types/metadata';

export class FileWriter implements IWriter {
  // eslint-disable-next-line no-empty-function
  constructor(private filePath: string) {}

  public async write(content: string): Promise<void> {
    if (FileSystem.exists(this.filePath)) {
      await new Promise(res => fs.appendFile(this.filePath, content, () => res()));
      return;
    }

    const parentDir = path.dirname(this.filePath);
    if (FileSystem.exists(parentDir) === false) {
      await new Promise(res => fs.mkdir(parentDir, res));
    }

    await FileSystem.writeFileAsync(this.filePath, content);
  }
}

export interface IWriter {
  write(content: string): Promise<void>;
}

export type InterpreterData =
  | IControllerData
  | IClassData
  | IInterfaceData
  | ILiteralData
  | IEnumData
  | IUnionData
  | IQualifiedTypeData
  | IIntersectionData
  | IOpaqueTypeDefinitionData
  | IOpaqueTypeImplementationData;

export interface IClientMetadata {
  name: string;
  version: string;
  servers: string[];
}

export abstract class BaseClientInterpreter {
  constructor(
    protected writer: IWriter,
    protected metadata: IClientMetadata // eslint-disable-next-line no-empty-function
  ) {}

  public abstract write(data: InterpreterData): Promise<void>;

  public abstract finalise(): Promise<void>;
}

export interface IDiscriminatorResult {
  discriminator: IPropertyData;
  mappings: { value: string; type: IClassData | IInterfaceData }[];
}

export function getDiscriminator(union: IUnionData): IDiscriminatorResult | undefined {
  const discriminator = union.commonProperties.find(x => x.type.kind === 'qualified-type' && x.isNullable !== true);
  if (discriminator) {
    const result: IDiscriminatorResult = {
      discriminator,
      mappings: [],
    };

    for (const option of union.options) {
      if (option.kind === 'interface' || option.kind === 'class') {
        const discriminatorProperty = option.properties.find(x => x.name === discriminator.name);
        if (!discriminatorProperty) {
          throw new Error(`Failed to find discriminator property '${discriminator.name}' on '${option.name}'`);
        } else if (discriminatorProperty.type.kind !== 'qualified-type') {
          throw new Error(`Discriminator property was expected to be a '${discriminator.name}' qualified-type`);
        }

        result.mappings.push({ type: option, value: discriminatorProperty.type.value });
      } else {
        // If we have an option that isn't a class or interface, then abort as we can't have a discriminator on
        // anything else.
        return undefined;
      }
    }

    return result;
  }

  return undefined;
}
