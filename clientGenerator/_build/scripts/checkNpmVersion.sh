#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
NPM_CURRENT="$(npm -v)"
NPM_DESIRED="$(cat $SCRIPT_DIR/../../.npm-version)"

if [ "$NPM_CURRENT" != "$NPM_DESIRED" ]; then
  echo -e "\\033[0;31mWrong npm version $NPM_CURRENT, 'npm i -g npm@$NPM_DESIRED' to install $NPM_DESIRED\n" 1>&2
  exit 1
fi