#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
NPM_CURRENT="$(npm -v)"
NPM_DESIRED="$(cat $SCRIPT_DIR/../../.npm-version)"

if [ "$NPM_CURRENT" != "$NPM_DESIRED" ]; then
  echo -e "\\033[0;31mWARNING: the current library has been built against $NPM_DESIRED\n" 1>&2
fi