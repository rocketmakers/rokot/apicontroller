const { readdirSync, lstatSync, existsSync } = require('fs');
const { join } = require('path');

const off = 0;
const warn = 1;
const error = 2;

const projects = [];

// Push the tsconfig for our build
projects.push(
  join(__dirname, './src/tsconfig.json'),
),
  (module.exports = {
    parser: '@typescript-eslint/parser',
    plugins: [
      "no-only-tests"
    ],
    extends: [
      'plugin:@typescript-eslint/recommended',
      'airbnb-base',
      'prettier/@typescript-eslint',
      'plugin:prettier/recommended',
      'plugin:react/recommended',
    ],
    parserOptions: {
      ecmaVersion: 2018,
      sourceType: 'module',
      project: projects,
      ecmaFeatures: {
        jsx: true, // Allows for the parsing of JSX
      },
    },
    globals: {
      window: true,
      document: true,
    },
    rules: {
      ['@typescript-eslint/no-floating-promises']: error,

      ['@typescript-eslint/no-namespace']: off,
      ['@typescript-eslint/interface-name-prefix']: off,
      ['@typescript-eslint/explicit-function-return-type']: off,
      ['@typescript-eslint/no-explicit-any']: off,
      ['@typescript-eslint/no-parameter-properties']: off,
      ['@typescript-eslint/explicit-member-accessibility']: off,

      ['import/no-unresolved']: off,
      ['import/prefer-default-export']: off,

      ['react/prop-types']: off,
      ['react/no-unescaped-entities']: off,

      ['no-inner-declarations']: off,
      ['class-methods-use-this']: off,
      ['no-useless-constructor']: off,
      ['no-undef']: off,
      ['global-require']: off,

      ['no-restricted-syntax']: off,
      ['no-await-in-loop']: off,
      ['no-continue']: off,
      ['import/extensions']: off,
      ['max-classes-per-file']: off,
      ['no-plusplus']: off,
      ['no-case-declarations']: off,

      ['no-only-tests/no-only-tests']: 'error'
    },
    env: {
      mocha: true,
      node: true,
    },
    settings: {
      react: {
        version: '16',
      },
    },
  });
