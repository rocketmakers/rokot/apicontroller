const fs = require('fs');
const path = require('path');
const npmVersionFilePath = path.join(__dirname, '.npm-version');
const expectedNpmVersion = fs.readFileSync(npmVersionFilePath, 'utf8');
const args = process.argv.slice(2);

function execShellCommand(cmd) {
  const exec = require('child_process').exec;
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        console.warn(error);
      }
      resolve(stdout ? stdout : stderr);
    });
  });
}

async function checkVersion() {
  const npmVersion = (await execShellCommand('npm -v')).trim();
  const versionMessage = `Current [${npmVersion}] Wanted [${expectedNpmVersion}]`;
  if (expectedNpmVersion !== npmVersion) {
    const message = ('\x1b[31m', 'INCORRECT NPM VERSION', '\x1b[0m', versionMessage);
    if (args[0] == '-e') {  
      console.error(message);
      console.error(`Install with 'npm i -g npm@${expectedNpmVersion}'`)
      process.exit(-1);    
    } else {
      console.warn(message);
      process.exit(0);     
    }
  }
  console.log('\x1b[32m', 'CORRECT NPM VERSION', '\x1b[0m', versionMessage);
  process.exit(0);
}

checkVersion();
