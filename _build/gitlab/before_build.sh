#/usr/bin/env bash
set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
NPM_DESIRED="$(cat $SCRIPT_DIR/../../.npm-version)"

# prepare environment
nodenv install -s
npm i -g npm@$NPM_DESIRED
npm i --unsafe-perm