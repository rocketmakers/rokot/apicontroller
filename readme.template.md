# rokot-apicontroller

Rokot - [Rocketmakers](http://www.rocketmakers.com/) TypeScript NodeJs Platform

## Introduction

A typescript decorators based solution to declaratively define routes for REST based api
This library creates metadata about the defined routes to allow auto route generation

## Getting Started

### Installation
Install via `npm`
```
npm i rokot-apicontroller
```

## Example

If you want to specify any additional custom Middleware, you can define them as below and annotate with the `middleware` decorator

```typescript
__LOADER__:./source/doco/middleware.ts|{"replace":[["../index","rokot-apicontroller"]]}
```

You can optionally register the middleware directly via the `registerMiddlewareFunction` method

```typescript
__LOADER__:./source/doco/middlewareSimple.ts|{"replace":[["../index","rokot-apicontroller"]]}
```

You can optionally create your own request to shape the request handler object:

```typescript
__LOADER__:./source/doco/customRequest.ts|{"replace":[["../index","rokot-apicontroller"]]}
```


You can then specify controllers and their routes:

```typescript
__LOADER__:./source/doco/controllerWithMiddleware.ts|{"replace":[["../index","rokot-apicontroller"]]}
```

To build your routes (and bootstrap your api) you can

```typescript
__LOADER__:./source/doco/bootstrap.ts|{"replace":[["../index","rokot-apicontroller"]]}
```

### Validation

`rokot-apicontroller` is agnostic of which validation framework you want to use.
You can specify a validation `spec` for the body, queryString or route params (typed as `any`) using:

```
@api.bodyValidationSpec({/* your body spec */})
@api.paramsValidationSpec({/* your route params spec */})
@api.queryValidationSpec({/* your query spec */})
```

Here is an example using the `rokot-validate` package

```typescript
__LOADER__:./source/doco/controllerWithValidation.ts|{"replace":[["../index","rokot-apicontroller"]]}
```

You then need to modify your bootstrap to add a validation function (that will validate the payload via its spec and return a validated copy of the payload) to the RouteBuilder constructor

```typescript
__LOADER__:./source/doco/bootstrapWithValidation.ts|{"replace":[["../index","rokot-apicontroller"]]}
```

If you want to expose these validation `spec`'s to your client, you can add a controller like this!

```typescript
__LOADER__:./source/doco/validationSpecController.ts|{"replace":[["../index","rokot-apicontroller"]]}
```

### Notes
The route methods should be instance members, and have a single param `req` of type `IApiRequest<TBody,TResponse,TParams,TQuery,TNative>`
It strongly types all aspects of the request to make consuming them simpler within the route


There is a corresponding `IExpressApiRequest<TBody, TResponse, TParams, TQuery>` that supplies the `TNative` with `{ request: express.Request, response: express.Response, next: express.NextFunction }`

The `@api.controller` decorator allows you to specify a controller name, route path prefix, and optionally the middleware keys to apply to all the controller contained routes.

NOTE: Its strongly recommended to supply the name of the class as the first parameter of `@api.controller`

The `@api.route` decorator must be supplied on all controller routes, it specifies the route path of the operation.

The optional `@api.middleware("three")` decorator on the route method allows you to specify addition middleware to implement within the route (you can apply this decorator multiple times per route to add additional middleware's).

The controllers middleware will run (in specified order) before the routes middleware is run (also in specified order)

The route verb (`get`,`put`,`post`,`delete` etc) is determined by the following rules:

1. If the route method is named exactly as a verb - that verb is used.
2. If you specify the optional `@api.verbs(...)` decorator - that verb (or those verbs) will be used.
3. if all else fails, `get`.

The route path is determined by combining the (optional) `routePrefix` from `@api.controller` with the `@api.route` decorator values

The optional `@api.contentType` decorator can be applied once on any controller routes, it specifies the content type of the request body (the default is `"application/json"`).

## Consumed Libraries

### [rokot-test](https://github.com/Rocketmakers/rokot-test)
The testing framework used within the Rokot Platform!
